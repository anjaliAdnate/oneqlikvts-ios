import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCustomerModal } from './add-customer-modal';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';

@NgModule({
  declarations: [
    AddCustomerModal
  ],
  imports: [
    IonicPageModule.forChild(AddCustomerModal)
  ],
  providers: [
    Camera,
    File,
    FilePath,
    Transfer,
    TransferObject,
    FileTransfer,
    FileTransferObject,
  ]
})
export class AddCustomerModalModule { }