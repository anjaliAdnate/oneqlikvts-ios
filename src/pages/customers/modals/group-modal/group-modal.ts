import { Component, Renderer } from "@angular/core";
import { ViewController, AlertController, ToastController, IonicPage } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";

@IonicPage()
@Component({
    selector: 'page-group-modal',
    templateUrl: 'group-modal.html'
})

export class GroupModalPage {
    GroupStatus: any = {};
    groupForm: FormGroup;
    groupstaus: any;
    GroupType: { vehicle: string; name: string; }[];
    TypeOf_Device: any;
    submitAttempt: boolean;
    islogin: any;
    devicesadd: any;
    constructor(
        public renderer: Renderer,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        public apiCall: ApiServiceProvider,
        public alerCtrl: AlertController,
        public toastCtrl: ToastController) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        this.GroupStatus = [
            {
                name: "Active",
                value: true
            },
            {
                name: "InActive",
                value: false
            }];

        this.GroupType = [
            {

                vehicle: "assets/imgs/car2.png",
                name: "car"
            },

            {

                vehicle: "assets/imgs/bike1.png",
                name: "bike"
            },

            {

                vehicle: "assets/imgs/truck2.png",
                name: "truck"

            }
        ];

        this.groupForm = formBuilder.group({
            group_name: ['', Validators.required],
            status: [''],
            grouptype: [''],
            address: [''],
            emailid: ['', Validators.email],
            mobno: ['', Validators]
        });
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }

    clickedGroupDiv(group, index) {
        this.TypeOf_Device = group.name,
            console.log(this.TypeOf_Device);
        console.log(index);
        var selected = [false, false, false];
        for (var i = 0; i < selected.length; i++)
            document.getElementById("" + i + "").className = "group daysDeselected";
        console.log(document.getElementById("" + i + "").className)
        document.getElementById(index).className = "group daysSelected";
        selected[index] = true;
        console.log(selected);
    }

    GroupStatusdata(status) {
        console.log("group status=> " + status);
        this.groupstaus = status;
        console.log("group status id=> " + this.groupstaus.value);
    }

    addGroup() {
        let that = this;
        that.submitAttempt = true;
        if (that.groupForm.valid) {

            // var devicedetails = {
            //     "name": this.groupForm.value.group_name,
            //     "status": this.groupstaus.name,
            //     "logopath": this.TypeOf_Device,
            //     "uid": this.islogin._id
            // }

            var devicedetails = {
                "name": that.groupForm.value.group_name,
                "status": that.groupstaus.value,
                "address": that.groupForm.value.address,
                "email": that.groupForm.value.email,
                "mobileNo": that.groupForm.value.mobno,
                "uid": that.islogin._id,
                "logopath": "car"
            }

            console.log(devicedetails)

            that.apiCall.startLoading().present();
            that.apiCall.addGroupCall(devicedetails)
                .subscribe(data => {
                    that.apiCall.stopLoading();
                    that.devicesadd = data;
                    console.log("response from device=> " + that.devicesadd);
                    let toast = that.toastCtrl.create({
                        message: 'Group was added successfully',
                        position: 'top',
                        duration: 2000
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        that.viewCtrl.dismiss();
                    });

                    toast.present();
                },
                    err => {
                        that.apiCall.stopLoading();
                        var body = err._body;
                        var msg = JSON.parse(body);
                        let alert = that.alerCtrl.create({
                            title: 'Oops!',
                            message: msg.message,
                            buttons: ['OK']
                        });
                        alert.present();
                    })
        }
    }
}