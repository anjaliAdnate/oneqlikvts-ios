import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowTripPage } from './show-trip';

@NgModule({
  declarations: [
    ShowTripPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowTripPage),
  ],
})
export class ShowTripPageModule {}
