import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-sos-report',
  templateUrl: 'sos-report.html',
})
export class SosReportPage {
  sos_id: any;
  islogin: any;
  datetimeStart: any;
  datetimeEnd: any;
  sosData: any;
  devices1243: any[];
  portstemp: any;
  devices: any;
  isdevice: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicall: ApiServiceProvider,
    public alertCtrl: AlertController
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
    this.getdevices();
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SosReportPage');
    // this.getSOSReportAPI();
  }

  getsosdevice(from, to, selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.sos_id = selectedVehicle.Device_Name;
    // this.getIgnitiondeviceReport(from, to);
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
        baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
        if (this.islogin.isDealer == true) {
            baseURLp += '&dealer=' + this.islogin._id;
        }
    }
    this.apicall.startLoading().present();
    // this.apicall.livedatacall(this.islogin._id, this.islogin.email)
    this.apicall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicall.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.isdevice = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
        this.getSOSReport();
      },
        err => {
          this.apicall.stopLoading();
          console.log(err);
        });
  }

  getSOS(from, to, selectedVehicle) {
    // debugger;
    console.log("selectedVehicle=> ", selectedVehicle)
    this.sos_id = selectedVehicle.Device_ID;
    // this.getIgnitiondeviceReport(from, to);
  }



  getSOSReport() {
    var _url;

    if (this.sos_id == undefined) {
      this.sos_id = "";
      _url = 'https://www.oneqlik.in/notifs/SOSReport?from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&_u=' + this.islogin._id;
    } else {
      _url = 'https://www.oneqlik.in/notifs/SOSReport?from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&dev_id=' + this.sos_id + '&_u=' + this.islogin._id;
    }


    this.apicall.startLoading().present();

    this.apicall.getSOSReport(_url)
      // this.apicall.getSOSReport(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.sos_id, this.islogin._id)
      .subscribe(data => {
        this.apicall.stopLoading();
        this.sosData = data;
        console.log(this.sosData);

        if (this.sosData.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }

      }, error => {
        this.apicall.stopLoading();
        console.log(error);
        // let alert = this.alertCtrl.create({
        //   message: 'No data found!',
        //   buttons: ['OK']
        // });
        // alert.present();
      })
  }

  // getSOSReportAPI() {

  //   this.apicall.getSOSReport(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.sos_id, this.islogin._id)
  //     .subscribe(data => {
  //       console.log(data)
  //     },
  //       err => {
  //         console.log("getting error => ", err)
  //       })
  // }


}
