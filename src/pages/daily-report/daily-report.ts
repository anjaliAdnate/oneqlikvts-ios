import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-daily-report',
  templateUrl: 'daily-report.html',
})
export class DailyReportPage implements OnInit {

  deviceReport: any[] = [];
  deviceReportSearch: any = [];

  to: string;
  from: string;
  islogin: any;
  todaydate: Date;
  todaytime: string;
  datetime: number;

  page: number = 0;
  limit: number = 10;
  fromDate: Date;
  vehicleData: any;
  dataTablesParameters: any;
  portstemp: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalldaily: ApiServiceProvider
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.from = moment({ hours: 0 }).format();
    console.log('start date', this.from)
    this.to = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.to);

    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }
  }

  ngOnInit() {
    this.getdevices();
    this.getDailyReportData();
  }

  getItems(ev: any) {
    const val = ev.target.value.trim();
    this.deviceReportSearch = this.deviceReport.filter((item) => {
      return (item.device.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
  }

  getDailyReportData() {
    console.log("entered");
    this.page = 0;
    var baseUrl;
    baseUrl = "https://www.oneqlik.in/devices/daily_report";
    let that = this;
    var currDay = new Date().getDay();
    var currMonth = new Date().getMonth();
    var currYear = new Date().getFullYear();
    var selectedDay = new Date(that.to).getDay();
    var selectedMonth = new Date(that.to).getMonth();
    var selectedYear = new Date(that.to).getFullYear();
    if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
      var devname = "Device_Name";
      var devid = "Device_ID";
      var today_odo = "today_odo";
      var today_running = "today_running";
      var today_stopped = "today_stopped";
      var today_trips = "today_trips";
      var maxSpeed = "maxSpeed"
    } else {
      console.log("else block called");
      var devid = "imei";
      var devname = "ID.Device_Name";
      var today_odo = "today_odo";
      var today_running = "today_running";
      var today_stopped = "today_stopped";
      var today_trips = "today_trips";
      var maxSpeed = "ID.maxSpeed"
    }
    var payload = {};
    if (this.vehicleData == undefined) {
      payload = {
        "draw": 2,
        "columns": [
          {
            "data": devname
          },
          {
            "data": devid
          },
          {
            "data": today_odo
          },
          {
            "data": today_running
          },
          {
            "data": today_stopped
          },
          {
            "data": today_trips
          },
          {
            "data": maxSpeed
          },
          {
            "data": null,
            "defaultContent": ""
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": 0,
        "length": this.limit,
        "search": {
          "value": "",
          "regex": false
        },
        "op": {},
        "select": [],
        "find": {
          "user_id": this.islogin._id,
          "date": new Date(this.to).toISOString()
        }
      }
    } else {
      payload = {
        "draw": 2,
        "columns": [
          {
            "data": devname
          },
          {
            "data": devid
          },
          {
            "data": today_odo
          },
          {
            "data": today_running
          },
          {
            "data": today_stopped
          },
          {
            "data": today_trips
          },
          {
            "data": maxSpeed
          },
          {
            "data": null,
            "defaultContent": ""
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": 0,
        "length": this.limit,
        "search": {
          "value": "",
          "regex": false
        },
        "op": {},
        "select": [],
        "find": {
          "user_id": this.islogin._id,
          "devId": this.vehicleData.Device_ID,
          "date": new Date(this.to).toISOString()
        }
      }
    }
    this.deviceReport = [];
    this.apicalldaily.startLoading().present();
    this.apicalldaily.getDailyReport1(baseUrl, payload)
      .subscribe(data => {
        this.apicalldaily.stopLoading();
        console.log("daily report data: ", data)
        for (var i = 0; i < data.data.length; i++) {
          // debugger
          // var odo = (data.data[i].today_odo).toString();
          // var odo1 = odo.split('.');
          this.deviceReport.push({
            _id: data.data[i]._id,
            Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
            Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
            maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
            today_odo: data.data[i].today_odo,
            today_running: this.millisToMinutesAndSeconds(data.data[i].today_running),
            today_stopped: this.millisToMinutesAndSeconds(data.data[i].today_stopped),
            today_trips: data.data[i].today_trips,
            // avgSpeed: this.calcAvgSpeed(odo1[0], data.data[i].today_running)
          })
        }

      }, error => {
        this.apicalldaily.stopLoading();
        console.log("error in service=> " + error);
      })
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    // this.apicalldaily.startLoading().present();
    // this.apicalldaily.livedatacall(this.islogin._id, this.islogin.email)
    this.apicalldaily.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        // this.apicalldaily.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          // this.apicalldaily.stopLoading();
          console.log(err);
        });
  }

  getSelectedId(pdata) {
    console.log(pdata)
    this.vehicleData = pdata;
    this.getDailyReportData();
  }

  millisToMinutesAndSeconds(millis) {
    var ms = millis;
    ms = 1000 * Math.round(ms / 1000); // round to nearest second
    var d = new Date(ms);
    return d.getUTCHours() + ':' + d.getUTCMinutes();
  }

  calcAvgSpeed(distance: any, time: any) {
    distance = distance / 1000;      //1000 = km
    time = time / 3600000;           // 1000 = sec, 60000 = min, 3600000 = hrs
    return distance * 3600000 / time;
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 10;
    setTimeout(() => {
      var baseUrl, payload = {};
      baseUrl = "https://www.oneqlik.in/devices/daily_report";
      let that = this;
      var currDay = new Date().getDay();
      var currMonth = new Date().getMonth();
      var currYear = new Date().getFullYear();
      var selectedDay = new Date(that.to).getDay();
      var selectedMonth = new Date(that.to).getMonth();
      var selectedYear = new Date(that.to).getFullYear();
      if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
        var devname = "Device_Name";
        var devid = "Device_ID";
        var today_odo = "today_odo";
        var today_running = "today_running";
        var today_stopped = "today_stopped";
        var today_trips = "today_trips";
        var maxSpeed = "maxSpeed"
      } else {
        console.log("else block called");
        var devid = "imei";
        var devname = "ID.Device_Name";
        var today_odo = "today_odo";
        var today_running = "today_running";
        var today_stopped = "today_stopped";
        var today_trips = "today_trips";
        var maxSpeed = "ID.maxSpeed"
      }
      var payload = {};
      if (that.vehicleData == undefined) {
        payload = {
          "draw": 2,
          "columns": [
            {
              "data": devname
            },
            {
              "data": devid
            },
            {
              "data": today_odo
            },
            {
              "data": today_running
            },
            {
              "data": today_stopped
            },
            {
              "data": today_trips
            },
            {
              "data": maxSpeed
            },
            {
              "data": null,
              "defaultContent": ""
            }
          ],
          "order": [
            {
              "column": 0,
              "dir": "asc"
            }
          ],
          "start": that.page,
          "length": 10,
          "search": {
            "value": "",
            "regex": false
          },
          "op": {},
          "select": [],
          "find": {
            "user_id": this.islogin._id,
            "date": new Date(this.to).toISOString()
          }
        }
      } else {
        payload = {
          "draw": 2,
          "columns": [
            {
              "data": devname
            },
            {
              "data": devid
            },
            {
              "data": today_odo
            },
            {
              "data": today_running
            },
            {
              "data": today_stopped
            },
            {
              "data": today_trips
            },
            {
              "data": maxSpeed
            },
            {
              "data": null,
              "defaultContent": ""
            }
          ],
          "order": [
            {
              "column": 0,
              "dir": "asc"
            }
          ],
          "start": that.page,
          "length": 10,
          "search": {
            "value": "",
            "regex": false
          },
          "op": {},
          "select": [],
          "find": {
            "user_id": this.islogin._id,
            "devId": this.vehicleData.Device_ID,
            "date": new Date(this.to).toISOString()
          }
        }
      }
      // that.deviceReport = [];
      // this.apicalldaily.startLoading().present();
      this.apicalldaily.getDailyReport1(baseUrl, payload)
        .subscribe(data => {
          // this.apicalldaily.stopLoading();
          console.log("daily report data: ", data)
          for (var i = 0; i < data.data.length; i++) {
            that.deviceReport.push({
              _id: data.data[i]._id,
              Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
              Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
              maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
              today_odo: data.data[i].today_odo,
              today_running: this.millisToMinutesAndSeconds(data.data[i].today_running),
              today_stopped: this.millisToMinutesAndSeconds(data.data[i].today_stopped),
              today_trips: data.data[i].today_trips,
              avgSpeed: this.calcAvgSpeed(data.data[i].today_odo, data.data[i].today_running)
            })
          }
          console.log('Async operation has ended');
          infiniteScroll.complete();
        })


    }, 200);
  }
}
