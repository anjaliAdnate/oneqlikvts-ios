import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-update-cust',
  templateUrl: 'edit-dealer.html',
})
export class EditDealerPage {
  islogin: any;
  _dealerData: any;
  editDealerForm: FormGroup;
  submitAttempt: boolean;

  constructor(
    public apiCall: ApiServiceProvider,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    navPar: NavParams,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this._dealerData = navPar.get("param");
    console.log("dealer details=> " + JSON.stringify(this._dealerData))

    this.editDealerForm = formBuilder.group({
      userid: [this._dealerData.userid, Validators.required],
      first_name: [this._dealerData.dealer_firstname, Validators.required],
      last_name: [this._dealerData.dealer_lastname, Validators.required],
      email: [this._dealerData.email, Validators.required],
      phone: [this._dealerData.phone, Validators.required],
      address: [this._dealerData.address, Validators.required],
      // creationdate: [moment(this._dealerData.date, 'DD/MM/YYYY').format('YYYY-MM-DD')],
      // expirationdate: [this.yearLater],
      // dealer_firstname: [this._dealerData.dealer_firstname],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditDealerPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  _submit() {
    this.submitAttempt = true;
    if (this.editDealerForm.valid) {
      var payload = {
        address: this.editDealerForm.value.address,
        contactid:this._dealerData.dealer_id,
        expire_date: null,
        first_name: this.editDealerForm.value.first_name,
        last_name: this.editDealerForm.value.last_name,
        status: this._dealerData.status,
        user_id: this.editDealerForm.value.userid
      }
      this.apiCall.startLoading().present();
      this.apiCall.editUserDetailsCall(payload)
        .subscribe(data => {
          this.apiCall.stopLoading();
          console.log("dealer updated data=> ", data)

        },
          err => {
            this.apiCall.stopLoading();
            console.log("error occured=> ", err)
          });
    }
  }

}
