import { Component } from '@angular/core';
import { NavParams, ViewController, IonicPage, NavController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-device-settings',
  templateUrl: 'device-settings.html',
})
export class DeviceSettingsPage {
  speedlimit: any;
  ingnitionStat: string;
  vname: string;
  tot_odo: any;
  dData: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
  ) {
    this.dData = navParams.get("param");
    this.vname = this.dData.Device_Name;
    this.tot_odo = this.fixDecimals(this.dData.total_odo);  // for two decimals
    this.speedlimit = this.dData.SpeedLimit;
    this.ingnitionStat = this.dData.ignitionSource;
  }

  fixDecimals(value : string){
    value = "" + value;
    value = value.trim();
    value = parseFloat(value).toFixed(2);
    return value;
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad SettingsPage');
  // }

  radioChecked(key) {
    console.log("ignition key=> ", key)
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  submitSettings() {
    // debugger;
    if (this.speedlimit == undefined || this.ingnitionStat == undefined || this.vname == undefined) {
      this.viewCtrl.dismiss();
    } else {
      // console.log("submit here")
      // console.log("vehicle name=> ", this.vname)
      // console.log("speedlimit=> ", this.speedlimit)
      // console.log("ingnitionStat=> ", this.ingnitionStat)
      var editData = {
        _id: this.dData._id,
        deviceid: this.dData.Device_ID,
        devicename: this.vname,
        speed: this.speedlimit,
        ignitionSource: this.ingnitionStat,
        total_odo: this.tot_odo
      }
      this.apiCall.startLoading().present();
      this.apiCall.deviceupdateCall(editData)
        .subscribe(data => {
          this.apiCall.stopLoading();
          // console.log("resp data=> " + data.message)
          const toast = this.toastCtrl.create({
            message: data.message + " successfully!",
            duration: 1500,
            position: "bottom"
          });
          toast.onDidDismiss(() => {
            this.viewCtrl.dismiss();
          })
          toast.present();
        },
          err => {
            console.log(err);
            this.apiCall.stopLoading();
          })
    }
  }

}
