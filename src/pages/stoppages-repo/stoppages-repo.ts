import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-stoppages-repo',
  templateUrl: 'stoppages-repo.html',
})
export class StoppagesRepoPage implements OnInit {

  stoppagesReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any = [];
  arrivalTime: string;
  departureTime: string;

  Stoppagesdata: any;
  datetime: number;
  selectedVehicle: any;
  vehicleData: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallStoppages: ApiServiceProvider,
    public alertCtrl: AlertController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);

    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }
  }

  ngOnInit() {
    if(this.vehicleData == undefined) {
      this.getdevices();
    } else {
      this.Ignitiondevice_id = this.vehicleData._id;
      this.getStoppageReport(this.datetimeStart, this.datetimeEnd);
    }
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
        baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
        if (this.islogin.isDealer == true) {
            baseURLp += '&dealer=' + this.islogin._id;
        }
    }
    this.apicallStoppages.startLoading().present();
    // this.apicallStoppages.livedatacall(this.islogin._id, this.islogin.email)
    this.apicallStoppages.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicallStoppages.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.devices = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicallStoppages.stopLoading();
          console.log(err);
        });
  }


  getStoppagesdevice(from, to, data) {
    console.log("selectedVehicle=> ", data)
    this.Ignitiondevice_id = data._id;
    // this.getIgnitiondeviceReport(from, to);
  }


  getStoppageReport(starttime, endtime) {
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    this.apicallStoppages.startLoading().present();
    this.apicallStoppages.getStoppageApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicallStoppages.stopLoading();
        this.stoppagesReport = data;
        this.Stoppagesdata = [];

        if (this.stoppagesReport.length != 0) {
          for (var i = 0; i < this.stoppagesReport.length; i++) {

            this.arrivalTime = new Date(this.stoppagesReport[i].arrival_time).toLocaleString();
            this.departureTime = new Date(this.stoppagesReport[i].departure_time).toLocaleString();

            var fd = new Date(this.arrivalTime).getTime();
            var td = new Date(this.departureTime).getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            // var Durations = rhours + 'hrs' + ' ' + rminutes;
            var Durations = rhours + ':' + rminutes;

            this.Stoppagesdata.push({ 'arrival_time': this.stoppagesReport[i].arrival_time, 'departure_time': this.stoppagesReport[i].departure_time, 'Durations': Durations, 'address': this.stoppagesReport[i].address, 'device': this.stoppagesReport[i].device.Device_Name });
            // console.log(this.Stoppagesdata);
          }

        } else {
          let alert = this.alertCtrl.create({
            message: 'No data found!',
            buttons: [{
              text: 'OK',
              handler: () => {
                this.selectedVehicle = undefined;
              }
            }]
          })
          alert.present();
        }


      }, error => {
        this.apicallStoppages.stopLoading();
        console.log(error);
      })
  }

}
