import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, Platform, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as io from 'socket.io-client';
import * as _ from "lodash";
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId, LatLngBounds, ILatLng, Polygon } from '@ionic-native/google-maps';
declare var google;

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live.html',
})
export class LivePage implements OnInit, OnDestroy {

  drawerHidden = true;
  drawerHidden1 = true;
  shouldBounce = true;
  dockedHeight = 150;
  bounceThreshold = 500;
  distanceTop = 56;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};

  data: any = {};
  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  resToken: string;
  liveDataShare: any;
  isEnabled: boolean = false;
  showMenuBtn: boolean = false;
  latlngCenter: any;
  mapHideTraffic: boolean = false;
  mapData: any = [];
  last_ping_on: any;
  geodata: any = [];
  geoShape: any = [];
  generalPolygon: any;
  locations: any = [];
  showFooter: boolean;
  tttime: any;
  condition2: string = 'light';
  condition1: string = 'light';
  condition: string = 'primary';
  acModel: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController
  ) {

    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));

    // platform.ready().then(() => {

    //   if (platform.is('cordova')) {

    //     //Subscribe on pause
    //     this.platform.pause.subscribe(() => {
    //       //Hello pause
    //       // alert("window pause")
    //     });

    //     //Subscribe on resume
    //     this.platform.resume.subscribe(() => {
    //       window['paused'] = 0;
    //       // alert("window resume");
    //       this.ngOnInit();
    //     });
    //   }
    // });
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
  bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z"
  truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z'

  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };
  busIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };
  bikeIcon = {
    path: this.bike,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  truckIcon = {
    path: this.truck,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  userIcon = {
    path: this.truck,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  }

  icons = {
    "car": this.carIcon,
    "bike": this.bikeIcon,
    "truck": this.truckIcon,
    "bus": this.busIcon,
    "user": this.userIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon
  }

  newMap() {
    let mapOptions = {
      controls: {
        zoom: true
      },
      mapTypeControlOptions: {
        mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
      },
      gestures: {
        rotate: false,
        tilt: false
      }
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    // map.animateCamera({
    //   target: { lat: 20.5937, lng: 78.9629 },
    //   // zoom: 15,
    //   duration: 2000,
    //   padding: 0,  // default = 20px
    // })
    return map;
  }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE') {
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
    } else {
      if (maptype == 'TERRAIN') {
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        }
      }
    }
  }

  onSelectMapOption(type) {
    let that = this;
    if (type == 'locateme') {
      that.allData.map.setCameraTarget(that.latlngCenter);
    } else {
      if (type == 'mapHideTraffic') {
        that.mapHideTraffic = !that.mapHideTraffic;
        if (that.mapHideTraffic) {
          that.allData.map.setTrafficEnabled(true);
        } else {
          that.allData.map.setTrafficEnabled(false);
        }
      } else {
        if (type == 'showGeofence') {
          console.log("Show Geofence")
          if (that.generalPolygon != undefined) {
            that.generalPolygon.remove();
            that.generalPolygon = undefined;
          } else {
            that.callGeofence();
          }
        }
      }
    }
  }

  callGeofence() {
    this.geoShape = [];
    this.apiCall.startLoading().present();
    this.apiCall.getGeofenceCall(this.userdetails._id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("geofence data=> " + data.length)
        if (data.length > 0) {
          this.geoShape = data.map(function (d) {
            return d.geofence.coordinates[0];
          });
          for (var g = 0; g < this.geoShape.length; g++) {
            for (var v = 0; v < this.geoShape[g].length; v++) {
              this.geoShape[g][v] = this.geoShape[g][v].reverse()
            }
          }

          for (var t = 0; t < this.geoShape.length; t++) {
            this.drawPolygon(this.geoShape[t])
          }
        } else {
          let alert = this.alertCtrl.create({
            message: 'No gofence found..!!',
            buttons: ['OK']
          });
          alert.present();
        }
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  drawPolygon(polyData) {
    let that = this;

    // that.allData.map = that.newMap();
    that.geodata = [];
    that.geodata = polyData.map(function (d) {
      return { lat: d[0], lng: d[1] }
    });
    console.log("geodata=> ", that.geodata)

    // let bounds = new LatLngBounds(that.geodata);
    // that.allData.map.moveCamera({
    // target: bounds
    // });
    let GORYOKAKU_POINTS: ILatLng[] = that.geodata;
    console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS)
    that.allData.map.addPolygon({
      'points': GORYOKAKU_POINTS,
      'strokeColor': '#AA00FF',
      'fillColor': '#00FFAA',
      'strokeWidth': 2
    }).then((polygon: Polygon) => {
      // polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
      //   console.log("polygon data=> " + param)
      //   console.log("polygon data=> " + param[1])
      // })
      this.generalPolygon = polygon;
    });
  }

  ngOnInit() {

    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") === 'live') {
        this.showMenuBtn = true;
      }
    }

    this._io = io.connect('https://www.oneqlik.in/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });

    this.drawerHidden = true;
    this.onClickShow = false;

    this.showBtn = false;
    this.SelectVehicle = "Select Vehicle";
    this.selectedVehicle = undefined;
    this.userDevices();
  }


  ngOnDestroy() {
    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    this._io.on('disconnect', () => {
      this._io.open();
    })
  }
  shareLive() {
    let that = this;
    that.onClickShow = false;
    that.drawerHidden1 = false;
    that.showFooter = true;
  }

  sharedevices(param) {
    let that = this;
    if (param == '15mins') {
      that.condition = 'primary';
      that.condition1 = 'light';
      that.condition2 = 'light';
      that.tttime = 15;
      // that.tttime  = (15 * 60000); //for miliseconds
    } else {
      if (param == '1hour') {
        that.condition1 = 'primary';
        that.condition = 'light';
        that.condition2 = 'light';
        that.tttime = 60;
        // that.tttime  = (1 * 3600000); //for miliseconds
      } else {
        if (param == '8hours') {
          that.condition2 = 'primary';
          that.condition = 'light';
          that.condition1 = 'light';
          that.tttime = (8 * 60);
          // that.tttime  = (8 * 3600000);
        }
      }
    }
  }

  shareLivetemp() {
    // debugger
    let that = this;
    if (that.tttime == undefined) {
      that.tttime = 15;
    }
    var data = {
      id: that.liveDataShare._id,
      imei: that.liveDataShare.Device_ID,
      sh: this.userdetails._id,
      ttl: that.tttime   // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  liveShare() {
    let that = this;
    var link = "https://www.oneqlik.in/share/liveShare?t=" + that.resToken;
    that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    that.onClickShow = true;
    that.showFooter = false;
    that.tttime = undefined;
    that.navCtrl.setRoot("DashboardPage");
  }

  // shareLive() {
  //   var data = {
  //     id: this.liveDataShare._id,
  //     imei: this.liveDataShare.Device_ID,
  //     sh: this.userdetails._id,
  //     ttl: 60 // set to 1 hour by default
  //   };
  //   this.apiCall.startLoading().present();
  //   this.apiCall.shareLivetrackCall(data)
  //     .subscribe(data => {
  //       this.apiCall.stopLoading();
  //       this.resToken = data.t;
  //       this.liveShare();
  //     },
  //       err => {
  //         this.apiCall.stopLoading();
  //         console.log(err);
  //       });
  // }

  // liveShare() {
  //   let that = this;
  //   var link = "https://www.oneqlik.in/share/liveShare?t=" + that.resToken;
  //   that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
  // }

  zoomin() {
    let that = this;
    that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.moveCameraZoomOut();
    // that.allData.map.animateCameraZoomOut();
  }


  userDevices() {

    let that = this;
    if (that.allData.map != undefined) {
      that.allData.map.remove();
      that.allData.map = that.newMap();
    } else {
      that.allData.map = that.newMap();
    }

    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;
    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userdetails._id;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += '&dealer=' + this.userdetails._id;
      }
    }

    that.apiCall.startLoading().present();
    // that.apiCall.getdevicesApi(that.userdetails._id, that.userdetails.email)
    that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(resp => {
        that.apiCall.stopLoading();
        that.portstemp = resp.devices;
        that.mapData = [];
        that.mapData = that.portstemp.map(function (d) {
          if (d.last_location != undefined) {
            return { lat: d.last_location['lat'], lng: d.last_location['long'] };
          }
        });

        let bounds = new LatLngBounds(that.mapData);
        that.allData.map.moveCamera({
          target: bounds,
          zoom: 10
        })

        for (var i = 0; i < resp.devices.length; i++) {
          that.socketInit(resp.devices[i]);
        }
      },
        err => {
          that.apiCall.stopLoading();
          console.log(err);
        });
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    return h + ':' + m + ':' + s;
  }

  socketInit(pdata, center = false) {
    let that = this;

    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

      if (d4 != undefined)
        (function (data) {

          if (data == undefined) {
            return;
          }

          if (data._id != undefined && data.last_location != undefined) {
            var key = data._id;
            let ic = _.cloneDeep(that.icons[data.iconType]);
            if (!ic) {
              return;
            }
            ic.path = null;
            ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
            console.log("ic url=> " + ic.url)
            that.vehicle_speed = data.last_speed;
            that.todays_odo = data.today_odo;
            that.total_odo = data.total_odo;
            that.fuel = data.currentFuel;
            that.last_ping_on = data.last_ping_on;

            if (data.lastStoppedAt != null) {
              var fd = new Date(data.lastStoppedAt).getTime();
              var td = new Date().getTime();
              var time_difference = td - fd;
              var total_min = time_difference / 60000;
              var hours = total_min / 60
              var rhours = Math.floor(hours);
              var minutes = (hours - rhours) * 60;
              var rminutes = Math.round(minutes);
              that.lastStoppedAt = rhours + ':' + rminutes;
            } else {
              that.lastStoppedAt = '00' + ':' + '00';
            }

            that.distFromLastStop = data.distFromLastStop;
            if (!isNaN(data.timeAtLastStop)) {
              that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
            } else {
              that.timeAtLastStop = '00:00:00';
            }
            that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
            that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
            that.last_ACC = data.last_ACC;
            that.acModel = data.ac;
            that.currentFuel = data.currentFuel;
            that.power = data.power;
            that.gpsTracking = data.gpsTracking;

            if (that.allData[key]) {
              that.socketSwitch[key] = data;
              that.allData[key].mark.setIcon(ic);
              that.allData[key].mark.setPosition(that.allData[key].poly[1]);
              var temp = _.cloneDeep(that.allData[key].poly[1]);
              that.allData[key].poly[0] = _.cloneDeep(temp);
              that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

              var speed = data.status == "RUNNING" ? data.last_speed : 0;
              that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
            }
            else {
              that.allData[key] = {};
              that.socketSwitch[key] = data;
              that.allData[key].poly = [];
              // if (data.sec_last_location) {
              // that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
              // } else {
              // that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
              // }
              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

              if (data.last_location != undefined) {

                that.allData.map.addMarker({
                  title: data.Device_Name,
                  position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                  icon: ic,
                }).then((marker: Marker) => {

                  that.allData[key].mark = marker;
                  // if (that.selectedVehicle == undefined || localStorage.getItem("LiveDevice") == null) {
                  if (that.selectedVehicle == undefined) {
                    marker.showInfoWindow();
                  }
                  // if (that.selectedVehicle != undefined || localStorage.getItem("LiveDevice") != null) {
                  if (that.selectedVehicle != undefined) {
                    marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                      .subscribe(e => {
                        that.liveVehicleName = data.Device_Name;
                        that.drawerHidden = false;
                        that.onClickShow = true;
                      });
                  }
                  var speed = data.status == "RUNNING" ? data.last_speed : 0;
                  that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                })
              }
            }

            var geocoder = new google.maps.Geocoder;
            var latlng = new google.maps.LatLng(data.last_location['lat'], data.last_location['long']);

            var request = {
              "latLng": latlng
            };

            geocoder.geocode(request, function (resp, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                if (resp[0] != null) {
                  that.address = resp[0].formatted_address;
                } else {
                  console.log("No address available")
                }
              }
              else {
                that.address = 'N/A';
              }
            })
          }
        })(d4)
    })
  }

  addCluster() {

    let that = this;
    console.log("locations length=> " + that.locations.length)
    console.log("locations=> " + that.locations)
    that.allData.map.addMarkerCluster({
      markers: that.locations,
      icons: [
        { min: 50, max: 100, url: "www/assets/imgs/maps/m1.png", anchor: { x: 16, y: 16 } }
      ]
    })
      .then((markerCluster) => {
        // markerCluster.on(GoogleMapsEvent.CLUSTER_CLICK).subscribe((cluster: any) => {
        //   console.log('cluster was clicked.');
        // });
      });
  }
  liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {

    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);

    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        mark.setIcon(icons);
      }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(new LatLng(lat, lng));
          }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(dest);
          }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  temp(data) {
    // debugger
    if (data.status == 'Expired') {
      let profileModal = this.modalCtrl.create('ExpiredPage');
      profileModal.present();

      profileModal.onDidDismiss(() => {
        this.selectedVehicle = undefined;
      });

    } else {
      let that = this;
      that.liveDataShare = data;
      that.drawerHidden = true;
      that.onClickShow = false;

      if (that.allData.map != undefined) {
        // that.allData.map.clear();
        that.allData.map.remove();
      }

      console.log("on select change data=> " + JSON.stringify(data));
      for (var i = 0; i < that.socketChnl.length; i++)
        that._io.removeAllListeners(that.socketChnl[i]);
      that.allData = {};
      that.socketChnl = [];
      that.socketSwitch = {};
      if (data) {
        if (data.last_location) {

          let mapOptions = {
            backgroundColor: 'white',
            controls: {
              compass: true,
              zoom: true,
              mapToolbar: true
            },
            gestures: {
              rotate: false,
              tilt: false
            },
            camera: {
              zoom: 15
            }
          }
          let map = GoogleMaps.create('map_canvas', mapOptions);
          // map.animateCamera({
          //   target: { lat: 20.5937, lng: 78.9629 },
          //   zoom: 15,
          //   duration: 1000,
          //   padding: 0  // default = 20px
          // });

          map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });

          // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          that.allData.map = map;
          that.socketInit(data);
        } else {
          that.allData.map = that.newMap();
          that.socketInit(data);
        }

        if (that.selectedVehicle != undefined) {
          that.drawerHidden = false;
          that.onClickShow = true;
        }
      }
      that.showBtn = true;
    }
  }
  info(mark, cb) {
    mark.addListener('click', cb);
  }
}