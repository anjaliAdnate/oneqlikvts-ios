webpackJsonp([31],{

/***/ 1011:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofenceReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GeofenceReportPage = /** @class */ (function () {
    function GeofenceReportPage(navCtrl, navParams, apicallGeofenceReport, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallGeofenceReport = apicallGeofenceReport;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    GeofenceReportPage.prototype.ngOnInit = function () {
        this.getgeofence1();
    };
    GeofenceReportPage.prototype.getgeofence1 = function () {
        var _this = this;
        console.log("getgeofence shape");
        var baseURLp = 'https://www.oneqlik.in/geofencing/getallgeofence?uid=' + this.islogin._id;
        this.apicallGeofenceReport.startLoading().present();
        this.apicallGeofenceReport.getallgeofenceCall(baseURLp)
            .subscribe(function (data) {
            _this.apicallGeofenceReport.stopLoading();
            _this.devices1243 = [];
            _this.geofencelist = data;
            console.log("geofencelist=> ", _this.geofencelist);
        }, function (err) {
            _this.apicallGeofenceReport.stopLoading();
            console.log(err);
        });
    };
    GeofenceReportPage.prototype.getGeofencedata = function (from, to, geofence) {
        console.log("selectedVehicle=> ", geofence);
        this.Ignitiondevice_id = geofence._id;
        this.getGeofenceReport(from, to);
    };
    GeofenceReportPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    GeofenceReportPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    GeofenceReportPage.prototype.getGeofenceReport = function (starttime, endtime) {
        var _this = this;
        console.log("this.Ignitiondevice_id=> " + this.Ignitiondevice_id);
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        console.log(starttime);
        console.log(endtime);
        this.apicallGeofenceReport.startLoading().present();
        this.apicallGeofenceReport.getGeogenceReportApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallGeofenceReport.stopLoading();
            _this.geofenceRepoert = data;
            console.log(_this.geofenceRepoert);
            if (_this.geofenceRepoert.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicallGeofenceReport.stopLoading();
            console.log(error);
            // let alert = this.alertCtrl.create({
            //   message: "Please Select Geofencing",
            //   buttons: ['OK']
            // });
            // alert.present();
            // let alert = this.alertCtrl.create({
            //   message: 'No data found!',
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    GeofenceReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-geofence-report',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/geofence-report/geofence-report.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Geofence Report</ion-title>\n\n    </ion-navbar>\n\n    <ion-item style="background-color: #fafafa;">\n\n        <ion-label style="margin-top: 15px;">Select Geofence</ion-label>\n\n        <select-searchable item-content [(ngModel)]="selectedGeofence" [items]="geofencelist" itemValueField="geoname"\n\n            itemTextField="geoname" [canSearch]="true"\n\n            (onChange)="getGeofencedata(datetimeStart, datetimeEnd, selectedGeofence)">\n\n        </select-searchable>\n\n    </ion-item>\n\n\n\n\n\n    <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n        <ion-col width-20>\n\n\n\n            <ion-label>\n\n                <span style="font-size: 13px">From Date</span>\n\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n\n                    [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n                    style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n            </ion-label>\n\n        </ion-col>\n\n\n\n        <ion-col width-20>\n\n            <ion-label>\n\n                <span style="font-size: 13px">To Date</span>\n\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n\n                    [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n                    style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n            </ion-label>\n\n        </ion-col>\n\n\n\n        <ion-col width-20>\n\n            <div style="margin-top: 9px; float: right">\n\n                <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;"\n\n                    (click)="getGeofenceReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n            </div>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-list>\n\n        <ion-card *ngFor="let item of geofenceRepoert">\n\n\n\n            <ion-item style="border-bottom: 2px solid #dedede;">\n\n                <ion-avatar item-start>\n\n                    <!-- <img src="assets/imgs/car_red_icon.png" > -->\n\n                    <img src="assets/imgs/car_red_icon.png"\n\n                        *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                    <img src="assets/imgs/car_green_icon.png"\n\n                        *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                    <img src="assets/imgs/car_grey_icon.png"\n\n                        *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                    <!-- <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))"> -->\n\n\n\n                    <img src="assets/imgs/truck_icon_red.png"\n\n                        *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                    <img src="assets/imgs/truck_icon_green.png"\n\n                        *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                    <img src="assets/imgs/truck_icon_grey.png"\n\n                        *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                    <img src="assets/imgs/bike_red_icon.png"\n\n                        *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                    <img src="assets/imgs/bike_green_icon.png"\n\n                        *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                    <img src="assets/imgs/bike_grey_icon.png"\n\n                        *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                    <img src="assets/imgs/jcb_red.png"\n\n                        *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                    <img src="assets/imgs/jcb_green.png"\n\n                        *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                    <img src="assets/imgs/jcb_gray.png"\n\n                        *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                    <img src="assets/imgs/bus_red.png"\n\n                        *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                    <img src="assets/imgs/bus_green.png"\n\n                        *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                    <img src="assets/imgs/bus_gray.png"\n\n                        *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n                    <!--  </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))"> -->\n\n\n\n                </ion-avatar>\n\n                <ion-row>\n\n                    <ion-col col-8>\n\n                        <p style="color:gray;font-size:13px;">{{item.vehicleName}}</p>\n\n                    </ion-col>\n\n\n\n                    <ion-col col-4>\n\n\n\n                        <p *ngIf="item.direction==\'In\'">\n\n                            <span style="font-size:12px;">{{item.direction}}&nbsp;</span>\n\n                            <ion-icon ios="ios-arrow-round-down" md="md-arrow-round-down" style="color:#ee7272">\n\n                            </ion-icon>\n\n                            &nbsp;\n\n                        </p>\n\n                        <p *ngIf="item.direction==\'Out\'">\n\n                            <span style="font-size:12px;">{{item.direction}}&nbsp;</span>\n\n                            <ion-icon ios="ios-arrow-round-up" md="md-arrow-round-up" style="color:#5edb82"></ion-icon>\n\n                            &nbsp;\n\n                        </p>\n\n                    </ion-col>\n\n                </ion-row>\n\n\n\n                <ion-row style="padding-top:13px;">\n\n\n\n                    <p>\n\n                        <ion-icon ios="ios-time" md="md-time" style="font-size:11px;"></ion-icon>&nbsp;\n\n                        <span style="font-size:11px;">{{item.timestamp | date: \'medium\'}}</span>\n\n                    </p>\n\n\n\n                </ion-row>\n\n\n\n                <ion-row>\n\n\n\n                    <p>\n\n                        <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:13px;"></ion-icon>&nbsp;\n\n\n\n                        <span *ngIf="!item.address">&nbsp;N/A</span>\n\n                        <span *ngIf="item.address"\n\n                            style="color:gray;font-size:11px;font-weight: 400;">{{item.address}}</span>\n\n                    </p>\n\n\n\n                </ion-row>\n\n            </ion-item>\n\n\n\n        </ion-card>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/geofence-report/geofence-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], GeofenceReportPage);
    return GeofenceReportPage;
}());

//# sourceMappingURL=geofence-report.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofenceReportPageModule", function() { return GeofenceReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence_report__ = __webpack_require__(1011);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var GeofenceReportPageModule = /** @class */ (function () {
    function GeofenceReportPageModule() {
    }
    GeofenceReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence_report__["a" /* GeofenceReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence_report__["a" /* GeofenceReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], GeofenceReportPageModule);
    return GeofenceReportPageModule;
}());

//# sourceMappingURL=geofence-report.module.js.map

/***/ })

});
//# sourceMappingURL=31.js.map