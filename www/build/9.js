webpackJsonp([9],{

/***/ 1028:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SosReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SosReportPage = /** @class */ (function () {
    function SosReportPage(navCtrl, navParams, apicall, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicall = apicall;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
        this.getdevices();
    }
    SosReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SosReportPage');
        // this.getSOSReportAPI();
    };
    SosReportPage.prototype.getsosdevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.sos_id = selectedVehicle.Device_Name;
        // this.getIgnitiondeviceReport(from, to);
    };
    SosReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicall.startLoading().present();
        // this.apicall.livedatacall(this.islogin._id, this.islogin.email)
        this.apicall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.isdevice = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
            _this.getSOSReport();
        }, function (err) {
            _this.apicall.stopLoading();
            console.log(err);
        });
    };
    SosReportPage.prototype.getSOS = function (from, to, selectedVehicle) {
        // debugger;
        console.log("selectedVehicle=> ", selectedVehicle);
        this.sos_id = selectedVehicle.Device_ID;
        // this.getIgnitiondeviceReport(from, to);
    };
    SosReportPage.prototype.getSOSReport = function () {
        var _this = this;
        var _url;
        if (this.sos_id == undefined) {
            this.sos_id = "";
            _url = 'https://www.oneqlik.in/notifs/SOSReport?from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&_u=' + this.islogin._id;
        }
        else {
            _url = 'https://www.oneqlik.in/notifs/SOSReport?from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&dev_id=' + this.sos_id + '&_u=' + this.islogin._id;
        }
        this.apicall.startLoading().present();
        this.apicall.getSOSReport(_url)
            // this.apicall.getSOSReport(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.sos_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            _this.sosData = data;
            console.log(_this.sosData);
            if (_this.sosData.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicall.stopLoading();
            console.log(error);
            // let alert = this.alertCtrl.create({
            //   message: 'No data found!',
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    SosReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sos-report',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/sos-report/sos-report.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>SOS Report</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n      [canSearch]="true" (onChange)="getSOS(datetimeStart, datetimeEnd, selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n    <ion-col width-20>\n\n      <ion-label>\n        <span style="font-size: 13px">From Date</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">To Date</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getSOSReport();"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let s of sosData">\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n      <ion-avatar item-start>\n        <img src="assets/imgs/car_green_icon.png" *ngIf="(s.device.iconType == \'car\')">\n        <img src="assets/imgs/truck_icon_green.png" *ngIf="(s.device.iconType == \'truck\')">\n        <img src="assets/imgs/bus_green.png" *ngIf="(s.device.iconType == \'bus\')">\n        <img src="assets/imgs/bike_green_icon.png" *ngIf="(s.device.iconType == \'bike\')">\n      </ion-avatar>\n      <ion-row>\n        <ion-col col-12>\n          <p style="color:gray;font-size:13px;">{{s.vehicleName}}</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n          <p ion-text text-wrap style="color:gray;font-size:12px;">{{s.timestamp | date:\'full\'}}</p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/sos-report/sos-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], SosReportPage);
    return SosReportPage;
}());

//# sourceMappingURL=sos-report.js.map

/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SosReportPageModule", function() { return SosReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sos_report__ = __webpack_require__(1028);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SosReportPageModule = /** @class */ (function () {
    function SosReportPageModule() {
    }
    SosReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sos_report__["a" /* SosReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__sos_report__["a" /* SosReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], SosReportPageModule);
    return SosReportPageModule;
}());

//# sourceMappingURL=sos-report.module.js.map

/***/ })

});
//# sourceMappingURL=9.js.map