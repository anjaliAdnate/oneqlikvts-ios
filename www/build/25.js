webpackJsonp([25],{

/***/ 1015:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IgnitionReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IgnitionReportPage = /** @class */ (function () {
    function IgnitionReportPage(navCtrl, navParams, apicalligi, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalligi = apicalligi;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    IgnitionReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    IgnitionReportPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    IgnitionReportPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    IgnitionReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalligi.startLoading().present();
        // this.apicalligi.livedatacall(this.islogin._id, this.islogin.email)
        this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.isdevice = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicalligi.stopLoading();
            console.log(err);
        });
    };
    IgnitionReportPage.prototype.getIgnitiondevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle.Device_Name;
        // this.getIgnitiondeviceReport(from, to);
    };
    IgnitionReportPage.prototype.getIgnitiondeviceReport = function (starttime, endtime) {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        // console.log(starttime);
        // console.log(endtime);
        this.apicalligi.startLoading().present();
        this.apicalligi.getIgiApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.igiReport = data;
            console.log(_this.igiReport);
            if (_this.igiReport.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicalligi.stopLoading();
            console.log(error);
            // let alert = this.alertCtrl.create({
            //   message: 'No data found!',
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    IgnitionReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ignition-report',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/ignition-report/ignition-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Ignition Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true"\n\n      (onChange)="getIgnitiondevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD-MM-YYYY hh:mm a" [(ngModel)]="datetimeStart"\n\n          (ionChange)="change(datetimeStart)" style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD-MM-YYYY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          (ionChange)="change1(datetimeEnd)" style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;"\n\n          (click)="getIgnitiondeviceReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-list>\n\n    <ion-card *ngFor="let ignitationdata of igiReport">\n\n\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-avatar item-start>\n\n          <img src="assets/imgs/car_red_icon.png">\n\n        </ion-avatar>\n\n        <ion-row>\n\n          <ion-col col-8>\n\n            <p style="color:gray;font-size:13px;">{{ignitationdata.vehicleName}}</p>\n\n          </ion-col>\n\n\n\n          <ion-col col-4>\n\n            <p style="text-align:center;" *ngIf="ignitationdata.switch==\'OFF\'">\n\n              <span style="font-size:12px;">{{ignitationdata.switch}}&nbsp;</span>\n\n              <span>\n\n                <ion-icon ios="ios-switch" md="md-switch" style="color:#ee7272"></ion-icon>&nbsp;\n\n              </span>\n\n            </p>\n\n            <p style="text-align:center;" *ngIf="ignitationdata.switch==\'ON\'">\n\n              <span style="font-size:12px;">{{ignitationdata.switch}}&nbsp;</span>\n\n              <span>\n\n                <ion-icon ios="ios-switch" md="md-switch" style="color:#5edb82"></ion-icon>&nbsp;\n\n              </span>\n\n            </p>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row style="padding-top:13px;">\n\n          <p>\n\n            <ion-icon ios="ios-time" md="md-time" style="font-size:11px;"></ion-icon>&nbsp;\n\n            <span style="font-size:11px;">{{ignitationdata.timestamp | date: \'medium\'}}</span>\n\n          </p>\n\n        </ion-row>\n\n        <ion-row>\n\n          <p>\n\n            <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:13px;"></ion-icon>&nbsp;\n\n            <span *ngIf="!ignitationdata.address">N/A</span>\n\n            <span *ngIf="ignitationdata.address"\n\n              style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">{{ignitationdata.address}}</span>\n\n          </p>\n\n        </ion-row>\n\n      </ion-item>\n\n    </ion-card>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/ignition-report/ignition-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], IgnitionReportPage);
    return IgnitionReportPage;
}());

//# sourceMappingURL=ignition-report.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IgnitionReportPageModule", function() { return IgnitionReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ignition_report__ = __webpack_require__(1015);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var IgnitionReportPageModule = /** @class */ (function () {
    function IgnitionReportPageModule() {
    }
    IgnitionReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__ignition_report__["a" /* IgnitionReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__ignition_report__["a" /* IgnitionReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], IgnitionReportPageModule);
    return IgnitionReportPageModule;
}());

//# sourceMappingURL=ignition-report.module.js.map

/***/ })

});
//# sourceMappingURL=25.js.map