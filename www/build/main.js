webpackJsonp([55],{

/***/ 134:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 134;

/***/ }),

/***/ 175:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-devices/add-devices.module": [
		547,
		51
	],
	"../pages/add-devices/immobilize/immobilize.module": [
		495,
		50
	],
	"../pages/add-devices/payment-greeting/payment-greeting.module": [
		498,
		49
	],
	"../pages/add-devices/payment-secure/payment-secure.module": [
		497,
		5
	],
	"../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module": [
		496,
		48
	],
	"../pages/add-devices/update-device/update-device.module": [
		532,
		47
	],
	"../pages/add-devices/upload-doc/add-doc/add-doc.module": [
		499,
		46
	],
	"../pages/add-devices/upload-doc/upload-doc.module": [
		501,
		54
	],
	"../pages/add-devices/vehicle-details/vehicle-details.module": [
		543,
		3
	],
	"../pages/add-devices/wallet/wallet.module": [
		500,
		0
	],
	"../pages/all-notifications/all-notifications.module": [
		531,
		45
	],
	"../pages/contact-us/contact-us.module": [
		502,
		44
	],
	"../pages/customers/customers.module": [
		537,
		43
	],
	"../pages/customers/modals/add-customer-modal/add-customer-modal.module": [
		533,
		42
	],
	"../pages/customers/modals/add-device-modal.module": [
		536,
		41
	],
	"../pages/customers/modals/group-modal/group-modal.module": [
		503,
		40
	],
	"../pages/customers/modals/update-cust/update-cust.module": [
		504,
		39
	],
	"../pages/daily-report/daily-report.module": [
		534,
		38
	],
	"../pages/dashboard/dashboard.module": [
		535,
		1
	],
	"../pages/dealers/add-dealer/add-dealer.module": [
		505,
		37
	],
	"../pages/dealers/dealers.module": [
		538,
		36
	],
	"../pages/dealers/edit-dealer/edit-dealer.module": [
		506,
		35
	],
	"../pages/device-summary-repo/device-summary-repo.module": [
		507,
		34
	],
	"../pages/distance-report/distance-report.module": [
		508,
		33
	],
	"../pages/feedback/feedback.module": [
		509,
		4
	],
	"../pages/fuel-report/fuel-report.module": [
		510,
		32
	],
	"../pages/geofence-report/geofence-report.module": [
		511,
		31
	],
	"../pages/geofence/add-geofence/add-geofence.module": [
		544,
		30
	],
	"../pages/geofence/geofence-show/geofence-show.module": [
		512,
		29
	],
	"../pages/geofence/geofence.module": [
		513,
		28
	],
	"../pages/groups/groups.module": [
		514,
		27
	],
	"../pages/history-device/history-device.module": [
		545,
		26
	],
	"../pages/ignition-report/ignition-report.module": [
		515,
		25
	],
	"../pages/live-single-device/device-settings/device-settings.module": [
		516,
		24
	],
	"../pages/live-single-device/live-single-device.module": [
		549,
		53
	],
	"../pages/live/expired/expired.module": [
		517,
		23
	],
	"../pages/live/live.module": [
		548,
		22
	],
	"../pages/login/login.module": [
		539,
		21
	],
	"../pages/over-speed-repo/over-speed-repo.module": [
		518,
		20
	],
	"../pages/poi-list/add-poi/add-poi.module": [
		546,
		19
	],
	"../pages/poi-list/poi-list.module": [
		520,
		18
	],
	"../pages/poi-report/poi-report.module": [
		519,
		17
	],
	"../pages/profile/profile.module": [
		540,
		52
	],
	"../pages/profile/settings/settings.module": [
		521,
		16
	],
	"../pages/route-map-show/route-map-show.module": [
		522,
		15
	],
	"../pages/route-voilations/route-voilations.module": [
		524,
		14
	],
	"../pages/route/route.module": [
		523,
		13
	],
	"../pages/show-geofence/show-geofence.module": [
		525,
		12
	],
	"../pages/show-route/show-route.module": [
		526,
		11
	],
	"../pages/show-trip/show-trip.module": [
		527,
		10
	],
	"../pages/sos-report/sos-report.module": [
		528,
		9
	],
	"../pages/speed-repo/speed-repo.module": [
		529,
		2
	],
	"../pages/stoppages-repo/stoppages-repo.module": [
		530,
		8
	],
	"../pages/trip-report/trip-report.module": [
		541,
		7
	],
	"../pages/trip-report/trip-review/trip-review.module": [
		542,
		6
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 175;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewDoc; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ViewDoc = /** @class */ (function () {
    function ViewDoc(navparams, viewCtrl) {
        this.navparams = navparams;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("param1: ", this.navparams.get("param1"));
        this._instData = this.navparams.get("param1");
        var str = this._instData.imageURL;
        var str1 = str.split('public/');
        this.imgUrl = "https://www.oneqlik.in/" + str1[1];
        console.log("img url: ", this._instData);
    }
    ViewDoc.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ViewDoc = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'page-view-doc',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/add-devices/upload-doc/view-doc/view-doc.html"*/'<div>\n    <ion-row>\n        <ion-col col-11>\n\n        </ion-col>\n        <ion-col col-1>\n            <ion-icon style="font-size:1.5em" name="close-circle" (tap)="dismiss()"></ion-icon>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col style="text-align: center;">\n            <img src="{{imgUrl}}" width="300" height="450" />\n        </ion-col>\n    </ion-row>\n    <ion-row padding-left>\n        <ion-col col-3>\n            <b>Expiry Date:</b>\n        </ion-col>\n        <ion-col col-9>{{_instData.expDate | date: \'medium\'}}</ion-col>\n    </ion-row>\n</div>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/add-devices/upload-doc/view-doc/view-doc.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"]])
    ], ViewDoc);
    return ViewDoc;
}());

//# sourceMappingURL=view-doc.js.map

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, loadingCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        //https://www.oneqlik.in
        this.usersURL = "https://www.oneqlik.in/users/";
        this.devicesURL = "https://www.oneqlik.in/devices";
        this.gpsURL = "https://www.oneqlik.in/gps";
        this.geofencingURL = "https://www.oneqlik.in/geofencing";
        this.trackRouteURL = "https://www.oneqlik.in/trackRoute";
        this.groupURL = "https://www.oneqlik.in/groups/";
        this.notifsURL = "https://www.oneqlik.in/notifs";
        this.stoppageURL = "https://www.oneqlik.in/stoppage";
        this.summaryURL = "https://www.oneqlik.in/summary";
        this.shareURL = "https://www.oneqlik.in/share";
        this.appId = "OneQlikVTS";
        console.log('Hello ApiServiceProvider Provider');
    }
    ////////////////// LOADING SERVICE /////////////////
    ApiServiceProvider.prototype.startLoadingnew = function (key) {
        var str;
        if (key == 1) {
            str = 'unlocking';
        }
        else {
            str = 'locking';
        }
        return this.loading1 = this.loadingCtrl.create({
            content: "Please wait for some time, as we are " + str + " your vehicle...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoadingnw = function () {
        return this.loading1.dismiss();
    };
    ApiServiceProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ////////////////// END LOADING SERVICE /////////////
    ApiServiceProvider.prototype.urlpasseswithdata = function (url, data) {
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePOIAPI = function (pay) {
        return this.http.post("https://www.oneqlik.in/vehtra/poi/updatePOI", pay, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addPOIAPI = function (payload) {
        return this.http.post("https://www.oneqlik.in/poi/addpoi", payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deletePOIAPI = function (_id) {
        return this.http.get("https://www.oneqlik.in/poi/deletePoi?_id=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getpoireportAPI = function (_id, pageNo, fromT, toT, poiId, devid) {
        return this.http.get("https://www.oneqlik.in/poi/poiReport?user=" + _id + "&s=" + pageNo + "&l=9&from=" + fromT + "&to=" + toT + "&poi=" + poiId + "&device=" + devid, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getPoisAPI = function (id) {
        return this.http.get("https://www.oneqlik.in/poi/getPois?user=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateDL = function (updateDL) {
        return this.http.post('https://www.oneqlik.in/users' + "/zogoUserUpdate", updateDL, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getsingledevice = function (id) {
        return this.http.get(this.devicesURL + "/getDevicebyId?deviceId=" + id, { headers: this.headers })
            // return this.http.get("http://192.168.1.18:3000/devices/getDevicebyId?deviceId=" + id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSOSReport = function (url) {
        // return this.http.get("http://192.168.1.20:3000/notifs/SOSReport?from_date=" + starttime + '&to_date=' + endtime + '&dev_id=' + sos_id + '&_u=' + _id, { headers: this.headers })
        return this.http.get(url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.DealerSearchService = function (_id, pageno, limit, key) {
        return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteDealerCall = function (deletePayload) {
        return this.http.post(this.usersURL + 'deleteUser', deletePayload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDealersCall = function (_id, pageno, limit) {
        return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.releaseAmount = function (uid) {
        return this.http.post('https://www.oneqlik.in/paytm/releaseAmount', uid)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateTripStatus = function (uId) {
        return this.http.get('https://www.oneqlik.in/zogo/updateZogoPaymentStatus?user=' + uId)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.proceedSecurely = function (withdrawObj) {
        return this.http.post('https://www.oneqlik.in/paytm/Withdraw', withdrawObj)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getblockamt = function () {
        return this.http.get('https://www.oneqlik.in/zogo/getInnitialBlockAmount')
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmbalance = function (userCred) {
        return this.http.get('https://www.oneqlik.in/paytm/checkBalance?CUST_ID=' + userCred + "&app_id=" + this.appId)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addMoneyPaytm = function (amtObj) {
        return this.http.post('https://www.oneqlik.in/paytm/addMoney', amtObj)
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.paytmLoginWallet = function (walletcredentials) {
        return this.http.post('https://www.oneqlik.in/paytm/sendOTP', walletcredentials)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmOTPvalidation = function (otpObj) {
        return this.http.post('https://www.oneqlik.in/paytm/validateOTP', otpObj)
            .map(function (res) { return res.json(); });
    };
    // callSearchService(email, id, key) {
    //   return this.http.get(this.devicesURL + "/getDeviceByUser?email=" + email + "&id=" + id + "&skip=0&limit=10&search=" + key, { headers: this.headers })
    //     .map(resp => resp.json());
    // }
    ApiServiceProvider.prototype.callSearchService = function (baseURL) {
        return this.http.get(baseURL, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.callResponse = function (_id) {
        return this.http.get("https://www.oneqlik.in/trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
            // return this.http.get("http://192.168.1.13:3000/trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.serverLevelonoff = function (data) {
        return this.http.post(this.devicesURL + "/addCommandQueue", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePassword = function (data) {
        return this.http.post(this.usersURL + "updatePassword", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateprofile = function (data) {
        return this.http.post(this.usersURL + 'Account_Edit', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + '/getgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.get7daysData = function (a, t) {
        return this.http.get(this.gpsURL + '/getDashGraph?imei=' + a + '&t=' + t, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.dataRemoveFuncCall = function (_id, did) {
        return this.http.get(this.devicesURL + '/RemoveShareDevice?did=' + did + '&uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.tripReviewCall = function (device_id, stime, etime) {
        return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + stime + '&to=' + etime, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.sendTokenCall = function (payLoad) {
        return this.http.post(this.shareURL + "/propagate", payLoad, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.shareLivetrackCall = function (data) {
        return this.http.post(this.shareURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDriverList = function (_id) {
        return this.http.get("https://www.oneqlik.in/driver/getDrivers?userid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByDateCall = function (_id, skip, limit, dates) {
        // console.log("from date => "+ dates.fromDate.toISOString())
        // console.log("new date "+ new Date(dates.fromDate).toISOString())
        var from = new Date(dates.fromDate).toISOString();
        var to = new Date(dates.toDate).toISOString();
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByType = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFilteredcall = function (_id, skip, limit, key) {
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDataOnScroll = function (_id, skip, limit) {
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // getVehicleListCall(_id, email) {
    //   return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.getVehicleListCall = function (_url) {
        return this.http.get(_url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_detailCall = function (_id, starttime, endtime, did) {
        if (did == undefined) {
            return this.http.get('https://www.oneqlik.in/user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
        else {
            return this.http.get('https://www.oneqlik.in/user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + did, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
    };
    ApiServiceProvider.prototype.trackRouteDataCall = function (data) {
        return this.http.post(this.trackRouteURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gettrackRouteCall = function (_id, data) {
        return this.http.post(this.trackRouteURL + '/' + _id, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteCall = function (_id) {
        return this.http.delete(this.trackRouteURL + '/' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRoutesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getStoppageApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.stoppageURL + "/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getIgiApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getOverSpeedApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeogenceReportApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFuelApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceReportApi = function (starttime, endtime, _id, Ignitiondevice_id) {
        return this.http.get(this.summaryURL + "/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport1 = function (url, payload) {
        return this.http.post(url, payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.contactusApi = function (contactdata) {
        return this.http.post(this.usersURL + "contactous", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllNotificationCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addgeofenceCall = function (data) {
        return this.http.post(this.geofencingURL + '/addgeofence', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getdevicegeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofencestatusCall = function (_id, status, entering, exiting) {
        return this.http.get(this.geofencingURL + '/geofencestatus?gid=' + _id + '&status=' + status + '&entering=' + entering + '&exiting=' + exiting, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGeoCall = function (_id) {
        return this.http.get(this.geofencingURL + '/deletegeofence?id=' + _id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getallgeofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + '/getallgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.user_statusCall = function (data) {
        return this.http.post(this.usersURL + 'user_status', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.editUserDetailsCall = function (devicedetails) {
        return this.http.post(this.usersURL + 'editUserDetails', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerVehiclesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addGroupCall = function (devicedetails) {
        return this.http.post(this.groupURL + 'addGroup', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleTypesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllUsersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDeviceModelCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.groupsCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addDeviceCall = function (devicedetails) {
        return this.http.post(this.devicesURL + '/addDevice', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCustomersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + "/getgeofence?uid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassApi = function (mobno) {
        return this.http.get(this.usersURL + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassMobApi = function (Passwordset) {
        return this.http.get(this.usersURL + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.loginApi = function (userdata) {
        return this.http.post(this.usersURL + "LoginWithOtp", userdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.signupApi = function (usersignupdata) {
        return this.http.post(this.usersURL + "signUp", usersignupdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // dashboardcall(email, from, to, _id) {
    //   return this.http.get(this.gpsURL + '/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.dashboardcall = function (_baseUrl) {
        return this.http.get(_baseUrl, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppedDevices = function (_id, email, off_ids) {
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.livedatacall = function (_id, email) {
        return this.http.get(this.devicesURL + "/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesApi = function (_id, email) {
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesForAllVehiclesApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .timeout(500000000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.ignitionoffCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceupdateCall = function (devicedetail) {
        return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
            // return this.http.post("http://192.168.1.18:3000/devices" + "/deviceupdate", devicedetail, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceSpeedCall = function (device_id, from, to) {
        return this.http.get(this.gpsURL + '/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppage_detail = function (_id, from, to, device_id) {
        return this.http.get(this.stoppageURL + '/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gpsCall = function (device_id, from, to) {
        return this.http.get(this.gpsURL + '/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getcustToken = function (id) {
        return this.http.get(this.usersURL + "getCustumerDetail?uid=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSummaryReportApi = function (starttime, endtime, _id, device_id) {
        return this.http.get(this.summaryURL + "?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getallrouteCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSpeedReport = function (_id, time) {
        return this.http.get(this.gpsURL + '/getGpsSpeedReport?imei=' + _id + '&time=' + time, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // deviceupdateInCall(devicedetail) {
    //   return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.deleteDeviceCall = function (d_id) {
        return this.http.get(this.devicesURL + "/deleteDevice?did=" + d_id, { headers: this.headers })
            // .map(res => res.json());
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.deviceShareCall = function (data) {
        return this.http.get(this.devicesURL + "/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pushnotifyCall = function (pushdata) {
        return this.http.post(this.usersURL + "PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pullnotifyCall = function (pushdata) {
        return this.http.post(this.usersURL + "PullNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGroupCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGroupCall = function (d_id) {
        return this.http.get(this.groupURL + "deleteGroup?_id=" + d_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // addcustomerCall(devicedetails) {
    //   return this.http.post(this.usersURL + 'signUp', devicedetails, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.deleteCustomerCall = function (data) {
        return this.http.post(this.usersURL + 'deleteUser', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.route_details = function (_id, user_id) {
        return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + _id + '&user=' + user_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.callcustomerSearchService = function (uid, pageno, limit, seachKey) {
        return this.http.get(this.usersURL + 'getCust?uid=' + uid + '&pageNo=' + pageno + '&size=' + limit + '&search=' + seachKey)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatusEnum */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum[ConnectionStatusEnum["Online"] = 0] = "Online";
    ConnectionStatusEnum[ConnectionStatusEnum["Offline"] = 1] = "Offline";
})(ConnectionStatusEnum || (ConnectionStatusEnum = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(alertCtrl, network, eventCtrl) {
        this.alertCtrl = alertCtrl;
        this.network = network;
        this.eventCtrl = eventCtrl;
        this._status = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        console.log('Hello NetworkProvider Provider');
        this.previousStatus = ConnectionStatusEnum.Online;
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Online) {
                _this.setStatus(ConnectionStatusEnum.Offline);
                _this.eventCtrl.publish('network:offline');
            }
            // if (this.previousStatus === ConnectionStatusEnum.Online) {
            //   this.eventCtrl.publish('network:offline');
            // }
            // this.previousStatus = ConnectionStatusEnum.Offline;
        });
        this.network.onConnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Offline) {
                _this.setStatus(ConnectionStatusEnum.Online);
                _this.eventCtrl.publish('network:online');
            }
            // setTimeout(() => {
            //   if (this.previousStatus === ConnectionStatusEnum.Offline) {
            //     this.eventCtrl.publish('network:online');
            //   }
            //   this.previousStatus = ConnectionStatusEnum.Online;
            // }, 3000);
        });
    };
    NetworkProvider.prototype.getNetworkType = function () {
        return this.network.type;
    };
    NetworkProvider.prototype.getNetworkStatus = function () {
        return this._status.asObservable();
    };
    NetworkProvider.prototype.setStatus = function (status) {
        this.status = status;
        this._status.next(this.status);
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuProvider = /** @class */ (function () {
    function MenuProvider(http) {
        this.http = http;
    }
    MenuProvider.prototype.getSideMenus = function () {
        return [{
                title: 'Home', component: 'DashboardPage', icon: 'home'
            }, {
                title: 'Groups', component: 'GroupsPage', icon: 'people'
            }, {
                title: 'Customers', component: 'CustomersPage', icon: 'contacts'
            }, {
                title: 'Notifications', component: 'AllNotificationsPage', icon: 'notifications'
            },
            {
                title: 'Reports',
                subPages: [{
                        title: 'Daily Report',
                        component: 'DailyReportPage',
                    }, {
                        title: 'Speed Variation Report',
                        component: 'SpeedRepoPage',
                    }, {
                        title: 'Summary Report',
                        component: 'DeviceSummaryRepoPage',
                    }, {
                        title: 'Geofenceing Report',
                        component: 'GeofenceReportPage',
                    },
                    {
                        title: 'Overspeed Report',
                        component: 'OverSpeedRepoPage',
                    }, {
                        title: 'Ignition Report',
                        component: 'IgnitionReportPage',
                    },
                    {
                        title: 'Route Violation Report',
                        component: 'RouteVoilationsPage',
                    }, {
                        title: 'Stoppage Report',
                        component: 'StoppagesRepoPage',
                    },
                    {
                        title: 'Fuel Report',
                        component: 'FuelReportPage',
                    }, {
                        title: 'Distnace Report',
                        component: 'DistanceReportPage',
                    },
                    {
                        title: 'Trip Report',
                        component: 'TripReportPage',
                    }],
                icon: 'clipboard'
            },
            {
                title: 'Routes',
                subPages: [{
                        title: 'Routes',
                        component: 'RoutePage',
                    }, {
                        title: 'Route Maping',
                        component: 'RouteMapPage',
                    }],
                icon: 'map'
            }, {
                title: 'Feedback', component: 'FeedbackPage', icon: 'paper'
            }, {
                title: 'Contact Us', component: 'ContactUsPage', icon: 'mail'
            }, {
                title: 'Support', component: 'SupportPage', icon: 'call'
            }, {
                title: 'About Us', component: 'AboutUsPage', icon: 'alert'
            },];
    };
    MenuProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], MenuProvider);
    return MenuProvider;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__ = __webpack_require__(492);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular
 // tslint:disable-line
// Ionic


// This class is defined in this file because
// we don't want to make it exportable
var InnerMenuOptionModel = /** @class */ (function () {
    function InnerMenuOptionModel() {
    }
    InnerMenuOptionModel.fromMenuOptionModel = function (option, parent) {
        var innerMenuOptionModel = new InnerMenuOptionModel();
        innerMenuOptionModel.id = this.counter++;
        innerMenuOptionModel.iconName = option.iconName;
        innerMenuOptionModel.displayText = option.displayText;
        innerMenuOptionModel.badge = option.badge;
        innerMenuOptionModel.targetOption = option;
        innerMenuOptionModel.parent = parent || null;
        innerMenuOptionModel.selected = option.selected;
        if (option.suboptions) {
            innerMenuOptionModel.expanded = false;
            innerMenuOptionModel.suboptionsCount = option.suboptions.length;
            innerMenuOptionModel.subOptions = [];
            option.suboptions.forEach(function (subItem) {
                var innerSubItem = InnerMenuOptionModel.fromMenuOptionModel(subItem, innerMenuOptionModel);
                innerMenuOptionModel.subOptions.push(innerSubItem);
                // Select the parent if any
                // child option is selected
                if (subItem.selected) {
                    innerSubItem.parent.selected = true;
                    innerSubItem.parent.expanded = true;
                }
            });
        }
        return innerMenuOptionModel;
    };
    InnerMenuOptionModel.counter = 1;
    return InnerMenuOptionModel;
}());
var SideMenuContentComponent = /** @class */ (function () {
    function SideMenuContentComponent(platform, eventsCtrl, cdRef) {
        var _this = this;
        this.platform = platform;
        this.eventsCtrl = eventsCtrl;
        this.cdRef = cdRef;
        this.collapsableItems = [];
        // Outputs: return the selected option to the caller
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // Handle the redirect event
        this.eventsCtrl.subscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */], function (data) {
            _this.updateSelectedOption(data);
        });
    }
    Object.defineProperty(SideMenuContentComponent.prototype, "options", {
        set: function (value) {
            var _this = this;
            if (value) {
                // Keep a reference to the options
                // sent to this component
                this.menuOptions = value;
                this.collapsableItems = new Array();
                // Map the options to our internal models
                this.menuOptions.forEach(function (option) {
                    var innerMenuOption = InnerMenuOptionModel.fromMenuOptionModel(option);
                    _this.collapsableItems.push(innerMenuOption);
                    // Check if there's any option marked as selected
                    if (option.selected) {
                        _this.selectedOption = innerMenuOption;
                    }
                    else if (innerMenuOption.suboptionsCount) {
                        innerMenuOption.subOptions.forEach(function (subItem) {
                            if (subItem.selected) {
                                _this.selectedOption = subItem;
                            }
                        });
                    }
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this.menuSettings = value;
                this.mergeSettings();
            }
        },
        enumerable: true,
        configurable: true
    });
    SideMenuContentComponent.prototype.ngOnDestroy = function () {
        this.eventsCtrl.unsubscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */]);
    };
    // ---------------------------------------------------
    // PUBLIC methods
    // ---------------------------------------------------
    // Send the selected option to the caller component
    SideMenuContentComponent.prototype.select = function (option) {
        if (this.menuSettings.showSelectedOption) {
            this.setSelectedOption(option);
        }
        // Return the selected option (not our inner option)
        this.change.emit(option.targetOption);
    };
    // Toggle the sub options of the selected item
    SideMenuContentComponent.prototype.toggleItemOptions = function (targetOption) {
        if (!targetOption)
            return;
        // If the accordion mode is set to true, we need
        // to collapse all the other menu options
        if (this.menuSettings.accordionMode) {
            this.collapsableItems.forEach(function (option) {
                if (option.id !== targetOption.id) {
                    option.expanded = false;
                }
            });
        }
        // Toggle the selected option
        targetOption.expanded = !targetOption.expanded;
    };
    // Reset the entire menu
    SideMenuContentComponent.prototype.collapseAllOptions = function () {
        this.collapsableItems.forEach(function (option) {
            if (!option.selected) {
                option.expanded = false;
            }
            if (option.suboptionsCount) {
                option.subOptions.forEach(function (subItem) {
                    if (subItem.selected) {
                        // Expand the parent if any of
                        // its childs is selected
                        subItem.parent.expanded = true;
                    }
                });
            }
        });
        // Update the view since there wasn't
        // any user interaction with it
        this.cdRef.detectChanges();
    };
    Object.defineProperty(SideMenuContentComponent.prototype, "subOptionIndentation", {
        // Get the proper indentation of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.subOptionIndentation.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.subOptionIndentation.wp;
            return this.menuSettings.subOptionIndentation.md;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "optionHeight", {
        // Get the proper height of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.optionHeight.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.optionHeight.wp;
            return this.menuSettings.optionHeight.md;
        },
        enumerable: true,
        configurable: true
    });
    // ---------------------------------------------------
    // PRIVATE methods
    // ---------------------------------------------------
    // Method that set the selected option and its parent
    SideMenuContentComponent.prototype.setSelectedOption = function (option) {
        if (!option.targetOption.component)
            return;
        // Clean the current selected option if any
        if (this.selectedOption) {
            this.selectedOption.selected = false;
            this.selectedOption.targetOption.selected = false;
            if (this.selectedOption.parent) {
                this.selectedOption.parent.selected = false;
                this.selectedOption.parent.expanded = false;
            }
            this.selectedOption = null;
        }
        // Set this option to be the selected
        option.selected = true;
        option.targetOption.selected = true;
        if (option.parent) {
            option.parent.selected = true;
            option.parent.expanded = true;
        }
        // Keep a reference to the selected option
        this.selectedOption = option;
        // Update the view if needed since we may have
        // expanded or collapsed some options
        this.cdRef.detectChanges();
    };
    // Update the selected option
    SideMenuContentComponent.prototype.updateSelectedOption = function (data) {
        if (!data.displayText)
            return;
        var targetOption;
        this.collapsableItems.forEach(function (option) {
            if (option.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                targetOption = option;
            }
            else if (option.suboptionsCount) {
                option.subOptions.forEach(function (subOption) {
                    if (subOption.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                        targetOption = subOption;
                    }
                });
            }
        });
        if (targetOption) {
            this.setSelectedOption(targetOption);
        }
    };
    // Merge the settings received with the default settings
    SideMenuContentComponent.prototype.mergeSettings = function () {
        var defaultSettings = {
            accordionMode: false,
            optionHeight: {
                ios: 50,
                md: 50,
                wp: 50
            },
            arrowIcon: 'ios-arrow-down',
            showSelectedOption: false,
            selectedOptionClass: 'selected-option',
            indentSubOptionsWithoutIcons: false,
            subOptionIndentation: {
                ios: 16,
                md: 16,
                wp: 16
            }
        };
        if (!this.menuSettings) {
            // Use the default values
            this.menuSettings = defaultSettings;
            return;
        }
        if (!this.menuSettings.optionHeight) {
            this.menuSettings.optionHeight = defaultSettings.optionHeight;
        }
        else {
            this.menuSettings.optionHeight.ios = this.isDefinedAndPositive(this.menuSettings.optionHeight.ios) ? this.menuSettings.optionHeight.ios : defaultSettings.optionHeight.ios;
            this.menuSettings.optionHeight.md = this.isDefinedAndPositive(this.menuSettings.optionHeight.md) ? this.menuSettings.optionHeight.md : defaultSettings.optionHeight.md;
            this.menuSettings.optionHeight.wp = this.isDefinedAndPositive(this.menuSettings.optionHeight.wp) ? this.menuSettings.optionHeight.wp : defaultSettings.optionHeight.wp;
        }
        this.menuSettings.showSelectedOption = this.isDefined(this.menuSettings.showSelectedOption) ? this.menuSettings.showSelectedOption : defaultSettings.showSelectedOption;
        this.menuSettings.accordionMode = this.isDefined(this.menuSettings.accordionMode) ? this.menuSettings.accordionMode : defaultSettings.accordionMode;
        this.menuSettings.arrowIcon = this.isDefined(this.menuSettings.arrowIcon) ? this.menuSettings.arrowIcon : defaultSettings.arrowIcon;
        this.menuSettings.selectedOptionClass = this.isDefined(this.menuSettings.selectedOptionClass) ? this.menuSettings.selectedOptionClass : defaultSettings.selectedOptionClass;
        this.menuSettings.indentSubOptionsWithoutIcons = this.isDefined(this.menuSettings.indentSubOptionsWithoutIcons) ? this.menuSettings.indentSubOptionsWithoutIcons : defaultSettings.indentSubOptionsWithoutIcons;
        if (!this.menuSettings.subOptionIndentation) {
            this.menuSettings.subOptionIndentation = defaultSettings.subOptionIndentation;
        }
        else {
            this.menuSettings.subOptionIndentation.ios = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.ios) ? this.menuSettings.subOptionIndentation.ios : defaultSettings.subOptionIndentation.ios;
            this.menuSettings.subOptionIndentation.md = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.md) ? this.menuSettings.subOptionIndentation.md : defaultSettings.subOptionIndentation.md;
            this.menuSettings.subOptionIndentation.wp = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.wp) ? this.menuSettings.subOptionIndentation.wp : defaultSettings.subOptionIndentation.wp;
        }
    };
    SideMenuContentComponent.prototype.isDefined = function (property) {
        return property !== null && property !== undefined;
    };
    SideMenuContentComponent.prototype.isDefinedAndPositive = function (property) {
        return this.isDefined(property) && !isNaN(property) && property > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('options'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], SideMenuContentComponent.prototype, "options", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('settings'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideMenuContentComponent.prototype, "settings", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], SideMenuContentComponent.prototype, "change", void 0);
    SideMenuContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'side-menu-content',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/shared/side-menu-content/side-menu-content.component.html"*/'<ion-list no-margin no-lines>\n    <ng-template ngFor let-option [ngForOf]="collapsableItems" let-i="index">\n\n        <!-- It is a simple option -->\n        <ng-template [ngIf]="!option.suboptionsCount">\n            <ion-item class="option" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                (tap)="select(option)" tappable>\n                <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n                {{ option.displayText }}\n                <ion-badge item-right *ngIf="option.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n            </ion-item>\n        </ng-template>\n\n        <!-- It has nested options -->\n        <ng-template [ngIf]="option.suboptionsCount">\n\n            <ion-list no-margin class="accordion-menu">\n\n                <!-- Header -->\n                <ion-item class="header" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                    (tap)="toggleItemOptions(option)" tappable>\n                    <ion-icon [class.rotate]="option.expanded" class="header-icon" [name]="option.iconName || menuSettings.arrowIcon"\n                        item-left></ion-icon>\n                    <!-- <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon> -->\n                    {{ option.displayText }}\n                </ion-item>\n\n                <!-- Sub items -->\n                <div [style.height]="option.expanded ? ((optionHeight + 1) * option.suboptionsCount) + \'px\' : \'0px\'" class="options">\n                    <ng-template ngFor let-item [ngForOf]="option.subOptions">\n                        <ion-item class="sub-option" [style.padding-left]="subOptionIndentation + \'px\'" [class.no-icon]="menuSettings?.indentSubOptionsWithoutIcons && !item.iconName"\n                            [ngClass]="menuSettings?.showSelectedOption && item.selected ? menuSettings.selectedOptionClass : null"\n                            tappable (tap)="select(item)">\n                            <ion-icon *ngIf="item.iconName" [name]="item.iconName" item-left></ion-icon>\n                            {{ item.displayText }}\n                            <ion-badge item-right *ngIf="item.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n                        </ion-item>\n                    </ng-template>\n                </div>\n            </ion-list>\n\n        </ng-template>\n\n    </ng-template>\n</ion-list>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/shared/side-menu-content/side-menu-content.component.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SideMenuContentComponent);
    return SideMenuContentComponent;
}());

//# sourceMappingURL=side-menu-content.component.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UploadDocPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UploadDocPage = /** @class */ (function () {
    function UploadDocPage(navCtrl, navParams, viewCtrl, platform, toastCtrl, loadingCtrl, apiCall, modalCtrl, popoverCtrl, event) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.event = event;
        this._docTypeList = [];
        this.imgData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.vehData = this.navParams.get("vehData");
        this.event.subscribe("reloaddoclist", function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
    }
    UploadDocPage.prototype.ngOnInit = function () {
        this.checkDevice();
    };
    UploadDocPage.prototype._addDocument = function () {
        this.navCtrl.push("AddDocPage", { vehData: this.vehData });
    };
    UploadDocPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UploadDocPage');
    };
    UploadDocPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UploadDocPage.prototype.onSelectChange = function (selectedValue) {
        console.log('Selected', selectedValue);
        this.docType = selectedValue;
    };
    UploadDocPage.prototype.brows = function () {
        this.docImge = true;
    };
    UploadDocPage.prototype.showToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    };
    UploadDocPage.prototype.viewDocFunc = function (imgData) {
        // let that = this;
        console.log("imgData: ", imgData);
        if (imgData == undefined) {
            this.showToast('Data not found');
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__["a" /* ViewDoc */], {
                param1: imgData
            });
            modal.present();
        }
    };
    UploadDocPage.prototype.checkDevice = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getsingledevice(this.vehData.Device_ID)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data.imageDoc.length > 0) {
                // console.log("imagedoc data: " + JSON.stringify(data.imageDoc))
                for (var i = 0; i < data.imageDoc.length; i++) {
                    _this._docTypeList.push({
                        docname: data.imageDoc[i].doctype,
                        expDate: data.imageDoc[i].docdate ? data.imageDoc[i].docdate : 'N/A',
                        imageURL: data.imageDoc[i].image
                    });
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UploadDocPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var that = this;
        var popover = this.popoverCtrl.create(DocPopoverPage, {
            vehData: that.vehData,
            imgData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
        popover.present({
            ev: ev
        });
    };
    UploadDocPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-upload-doc',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/add-devices/upload-doc/upload-doc.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Documents</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="_addDocument()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-list *ngIf="_docTypeList.length > 0">\n    <ion-item *ngFor="let doc of _docTypeList" (click)="viewDocFunc(doc)">\n      <ion-avatar item-start>\n        <img *ngIf="doc.docname == \'RC\'" src="assets/imgs/rc.png">\n        <img *ngIf="doc.docname == \'PUC\'" src="assets/imgs/puc.png">\n        <img *ngIf="doc.docname == \'Insurance\'" src="assets/imgs/insurence.png">\n        <img *ngIf="doc.docname == \'Licence\'" src="assets/imgs/dl.png">\n        <img *ngIf="doc.docname == \'Fitment\'" src="assets/imgs/rc.png">\n        <img *ngIf="doc.docname == \'Permit\'" src="assets/imgs/rc.png">\n      </ion-avatar>\n      <h1>{{doc.docname}}</h1>\n      <p>Expire On &nbsp;\n        <span *ngIf="doc.expDate != \'N/A\'">{{doc.expDate | date:\'mediumDate\'}}</span>\n        <span *ngIf="doc.expDate == \'N/A\'">{{doc.expDate}}</span>\n      </p>\n      <button ion-button item-end small icon-only color="gpsc" (click)="presentPopover($event, doc); $event.stopPropagation()">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-item>\n  </ion-list>\n  <ion-card *ngIf="_docTypeList.length == 0">\n    <ion-card-content>\n      No doucments uploaded yet... Upload documents of your vehicle by pressing above <b>+</b> button.\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>\n<!-- <ion-header>\n  <ion-navbar>\n    <ion-title>Upload Document</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-row>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'RC\' && !rcPresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'RC\')" tappable>\n            <div style="border: 5px solid #f2f2f2;">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">RC</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!rcPresent" name="cloud-upload" color="gpsc" style="font-size: 4em"></ion-icon>\n                      <ion-icon *ngIf="rcPresent" name="cloud-done" color="secondary" style="font-size: 4em"></ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Insurance\' && !insurencePresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'Insurance\')" tappable>\n            <div style="border: 5px solid #f2f2f2;">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">Insurance</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!insurencePresent" name="cloud-upload" color="gpsc" style="font-size: 4em">\n                      </ion-icon>\n                      <ion-icon *ngIf="insurencePresent" name="cloud-done" color="secondary" style="font-size: 4em">\n                      </ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Licence\' && !licencePresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'Licence\')" tappable>\n            <div style="border: 5px solid #f2f2f2;">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">Licence</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!licencePresent" name="cloud-upload" color="gpsc" style="font-size: 4em">\n                      </ion-icon>\n                      <ion-icon *ngIf="licencePresent" name="cloud-done" color="secondary" style="font-size: 4em">\n                      </ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'PUC\' && !licencePresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'PUC\')" tappable>\n            <div style="border: 5px solid #f2f2f2;">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">PUC</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!pucPresent" name="cloud-upload" color="gpsc" style="font-size: 4em"></ion-icon>\n                      <ion-icon *ngIf="pucPresent" name="cloud-done" color="secondary" style="font-size: 4em">\n                      </ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Fitment\' && !fitmentPresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'Fitment\')" tappable>\n            <div style="border: 5px solid #f2f2f2;" id="step2">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">Fitment Certificate</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!fitmentPresent" name="cloud-upload" color="gpsc" style="font-size: 4em">\n                      </ion-icon>\n                      <ion-icon *ngIf="fitmentPresent" name="cloud-done" color="secondary" style="font-size: 4em">\n                      </ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Permitt\' && !permittPresent)}" style="padding: 5px;"\n            (click)="firstFunc(\'Permit\')" tappable>\n            <div style="border: 5px solid #f2f2f2;" id="step1">\n              <ion-row>\n                <ion-col>\n                  <ion-row>\n                    <ion-col class="docCol">Permit Certificate</ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="text-align: center;">\n                      <ion-icon *ngIf="!permittPresent" name="cloud-upload" color="gpsc" style="font-size: 4em"></ion-icon>\n                      <ion-icon *ngIf="permittPresent" name="cloud-done" color="secondary" style="font-size: 4em">\n                      </ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <form #docForm="ngForm" *ngIf="docString != undefined" padding>\n    <ion-item>\n      <ion-label floating>{{docString}} Number </ion-label>\n      <ion-input type="text" [(ngModel)]="docname" name="docname"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label floating>Expiry Date </ion-label>\n      <ion-input type="text" onfocus="(this.type=\'date\')" onfocusout="(this.type=\'text\')" [(ngModel)]="docexp_date"\n        name="docexp_date">\n      </ion-input>\n    </ion-item>\n    <button ion-button block color="gpsc" [hidden]="(docname == undefined) || (docexp_date == undefined) || (lastImage != null)"\n      (click)="presentActionSheet()">\n      Select Document\n    </button>\n    <ion-row *ngIf="lastImage !== null" padding-left padding-right>\n      <ion-col col-10>\n        Selected {{docString}} Doc <span style="color: blue;">{{lastImage}}</span>\n      </ion-col>\n      <ion-col col-2 (click)="removeImg()">\n          <ion-icon name="close-circle"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf="lastImage !== null">\n      <ion-col>\n        <button ion-button block (click)="uploadImage()">Upload Doc</button>\n      </ion-col>\n    </ion-row>\n  </form>\n\n</ion-content> -->'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/add-devices/upload-doc/upload-doc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], UploadDocPage);
    return UploadDocPage;
}());

var DocPopoverPage = /** @class */ (function () {
    function DocPopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl, socialSharing) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.socialSharing = socialSharing;
        this.vehData = {};
        this.imgData = {};
        this.vehData = navParams.get("vehData");
        this.imgData = navParams.get("imgData");
        console.log("popover data=> ", this.imgData);
        var str = this.imgData.imageURL;
        var str1 = str.split('public/');
        this.link = "https://www.oneqlik.in/" + str1[1];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    DocPopoverPage.prototype.ngOnInit = function () { };
    DocPopoverPage.prototype.editItem = function () {
        console.log("edit");
    };
    DocPopoverPage.prototype.deleteDoc = function () {
        var that = this;
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this Document?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    DocPopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        var that = this;
        var payload = {
            devId: d_id,
            doctype: that.imgData.docname
        };
        this.apiCall.startLoading().present();
        var baseUrl = "https://www.oneqlik.in/devices/deleteDocuments";
        this.apiCall.urlpasseswithdata(baseUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Document deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    DocPopoverPage.prototype.shareItem = function () {
        this.socialSharing.share(this.islogin.fn + " " + this.islogin.ln + " has shared the document with you.", "OneQlik- Vehicle Document", this.link, "");
    };
    DocPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteDoc()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n      \n    </ion-list>\n  "
        })
        // <ion-item id="step1" class="text-seravek" (click)="upload()">
        //        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;Docs
        //        </ion-item>
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], DocPopoverPage);
    return DocPopoverPage;
}());

//# sourceMappingURL=upload-doc.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { DatePicker } from '@ionic-native/date-picker';
// import * as moment from 'moment';
var FilterPage = /** @class */ (function () {
    function FilterPage(apiCall, viewCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.modal = {};
        this.filterList = [
            { filterID: 1, filterName: "Overspeed", filterValue: 'overspeed', checked: false },
            { filterID: 2, filterName: "Ignition", filterValue: 'IGN', checked: false },
            { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
            { filterID: 4, filterName: "Geofence", filterValue: 'Geo-Fence', checked: false },
            { filterID: 5, filterName: "Stoppage", filterValue: 'MAXSTOPPAGE', checked: false },
            { filterID: 6, filterName: "Fuel", filterValue: 'Fuel', checked: false },
            { filterID: 7, filterName: "AC", filterValue: 'AC', checked: false },
            { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
            { filterID: 9, filterName: "Power", filterValue: 'power', checked: false }
        ];
        this.selectedArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        if (localStorage.getItem("checkedList") != null) {
            this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
            console.log("checked data=> " + this.checkedData);
        }
    }
    FilterPage.prototype.ngOnInit = function () {
    };
    // dateOpen() {
    //     this.datePicker.show({
    //         date: new Date(),
    //         mode: 'date',
    //         androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    //     }).then(
    //         date => console.log('Got date: ', date),
    //         err => console.log('Error occurred while getting date: ', err)
    //     );
    // }
    // changeDTformat(fromDT) {
    //     let that = this;
    //     that.modal.fromDate = moment(new Date(fromDT), "YYYY-MM-DD").format('dd/mm/yyyy');
    //     console.log("moment=> ", that.modal.fromDate);
    // }
    FilterPage.prototype.applyFilter = function () {
        console.log(this.modal);
        console.log(this.selectedArray);
        localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));
        if (this.selectedArray.length == 0) {
            localStorage.setItem("dates", "dates");
            this.viewCtrl.dismiss(this.modal);
        }
        else {
            localStorage.setItem("types", "types");
            this.viewCtrl.dismiss(this.selectedArray);
        }
    };
    FilterPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage.prototype.selectMember = function (data) {
        console.log("selected=> " + JSON.stringify(data));
        if (data.checked == true) {
            this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.filterID !== data.filterID;
            });
            this.selectedArray = newArray;
        }
    };
    FilterPage.prototype.cancel = function () {
        console.log("do nothing");
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-filter',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/all-notifications/filter/filter.html"*/'<ion-list>\n\n    <!-- <button ion-item (click)="close()">Close</button> -->\n\n    <!-- <ion-list-header style="text-align: center;">\n\n        Filter By\n\n    </ion-list-header>\n\n    <hr> -->\n\n    <ion-item-group>\n\n        <ion-item-divider color="light">Filter Type</ion-item-divider>\n\n        <div *ngIf="filterList.length > 0">\n\n            <ion-item *ngFor="let member of filterList">\n\n                <ion-label>{{member?.filterName || \'N/A\'}}</ion-label>\n\n                <ion-checkbox color="danger" (click)="selectMember(member)" [(ngModel)]="member.checked"></ion-checkbox>\n\n            </ion-item>\n\n        </div>\n\n    </ion-item-group>\n\n    <!-- <ion-item-group>\n\n        <ion-item-divider color="light">Time</ion-item-divider>\n\n        <div class="input-w">\n\n            <label for="#your-input" class="leb2">From</label>\n\n            <input type="date" id="your-input" [(ngModel)] ="modal.fromDate"/>\n\n        </div>\n\n        <div class="input-w">\n\n            <label for="#your-input" class="leb1">To</label>\n\n            <input type="date" id="your-input" [(ngModel)] ="modal.toDate"/>\n\n        </div>\n\n    </ion-item-group> -->\n\n\n\n\n\n\n\n</ion-list>\n\n<ion-row style="background-color: darkgray">\n\n    <ion-col width-50 style="text-align: center; background-color: #f53d3d;">\n\n        <button ion-button clear small color="light" (click)="cancel()">Cancel</button>\n\n    </ion-col>\n\n    <ion-col width-50 style="text-align: center; background-color: #11b700;">\n\n        <button ion-button clear small color="light" (click)="applyFilter()">Apply Filter</button>\n\n    </ion-col>\n\n</ion-row>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/all-notifications/filter/filter.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ServiceProviderPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return UpdatePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { TextToSpeech } from '@ionic-native/text-to-speech';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(appVersion, apiCall, alertCtrl, navCtrl, navParams, events, formBuilder, storage) {
        var _this = this;
        this.appVersion = appVersion;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.formBuilder = formBuilder;
        this.storage = storage;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.appVersion.getVersionNumber().then(function (version) {
            _this.aVer = version;
            console.log("app version=> " + _this.aVer);
        });
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.footerState = __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        this.credentialsForm = this.formBuilder.group({
            fname: [this.islogin.fn],
            lname: [this.islogin.ln],
            email: [this.islogin.email],
            phonenum: [this.islogin.phn],
            org: [this.islogin._orgName],
        });
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    ProfilePage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    ProfilePage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    ProfilePage.prototype.settings = function () {
        this.navCtrl.push('SettingsPage');
    };
    ProfilePage.prototype.service = function () {
        this.navCtrl.push(ServiceProviderPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.password = function () {
        this.navCtrl.push(UpdatePasswordPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.onSignIn = function () {
        var _this = this;
        // this.logger.info('SignInPage: onSignIn()');
        console.log(this.credentialsForm.value);
        var data = {
            "fname": this.credentialsForm.value.fname,
            "lname": this.credentialsForm.value.lname,
            "org": this.credentialsForm.value.org,
            "noti": true,
            "uid": this.islogin._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.updateprofile(data)
            .subscribe(function (resdata) {
            _this.apiCall.stopLoading();
            console.log("response from server=> ", resdata);
            if (resdata.token) {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Profile updated succesfully!',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                var logindata = JSON.stringify(resdata);
                                var logindetails = JSON.parse(logindata);
                                var userDetails = window.atob(logindetails.token.split('.')[1]);
                                var details = JSON.parse(userDetails);
                                console.log(details.email);
                                localStorage.setItem("loginflag", "loginflag");
                                localStorage.setItem('details', JSON.stringify(details));
                                localStorage.setItem('condition_chk', details.isDealer);
                                _this.islogin = JSON.parse(localStorage.getItem('details')) || {};
                                _this.footerState = 1;
                                _this.toggleFooter();
                            }
                        }]
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error from service=> ", err);
        });
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        this.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {
            "uid": this.islogin._id,
            "token": this.token,
            "os": "ios"
        };
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            console.log("push notifications updated " + data.message);
                            localStorage.clear();
                            localStorage.setItem('count', null);
                            _this.storage.clear().then(function () {
                                console.log("ionic storage cleared!");
                            });
                            _this.navCtrl.setRoot('LoginPage');
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No'
                }]
        });
        alert.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Me</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (tap)="toggleFooter()">\n        <ion-icon color="light" name="create" *ngIf="footerState == 0"></ion-icon>\n        <ion-icon color="light" name="done-all" *ngIf="footerState == 1"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item>\n      <ion-avatar item-start>\n        <img src="assets/imgs/dummy-user-img.png">\n      </ion-avatar>\n      <h2>{{islogin.fn}}&nbsp;{{islogin.ln}}</h2>\n      <p>Account:{{islogin.account}}</p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <!-- <ion-item (tap)="service()" *ngIf="!isDealer"> -->\n    <ion-item (tap)="service()">\n      <ion-icon name="person" item-start></ion-icon>\n      Service Provider\n    </ion-item>\n    <ion-item (tap)="password()">\n      <ion-icon name="lock" item-start></ion-icon>\n      Password\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <!-- <ion-item>\n      <ion-icon *ngIf="isChecked" name="mic" item-start></ion-icon>\n      <ion-icon *ngIf="!isChecked" name="mic-off" item-start></ion-icon>\n      <ion-label>Voice Notification</ion-label>\n      <ion-checkbox color="dark" [(ngModel)]="notif" (click)="setNotif(notif)" item-right></ion-checkbox>\n    </ion-item> -->\n    <ion-item (tap)="settings()">\n      <ion-icon name="cog" item-start></ion-icon>\n      Settings\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group (tap)="logout()">\n    <ion-item ion-text text-center>\n      Logout\n    </ion-item>\n  </ion-item-group>\n</ion-content>\n<ion-footer padding-bottom padding-top>\n  <div style="text-align: center;">Version {{aVer}}</div>\n  <br/>\n  <div id="photo" style="text-align: center">\n    <span style="vertical-align:middle">Powered by </span>&nbsp;\n    <a href="http://www.oneqlik.in/telematics/">\n      <img style="vertical-align:middle" src="assets/imgs/sign.jpg" width="55" height="19">\n    </a>\n  </div>\n</ion-footer>\n\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n  <!-- <ion-pullup-tab [footer]="pullup" (tap)="toggleFooter()">\n    <ion-icon name="arrow-up" *ngIf="footerState == 0"></ion-icon><ion-icon name="arrow-down" *ngIf="footerState == 1"></ion-icon>\n  </ion-pullup-tab>\n  <ion-toolbar color="primary" (tap)="toggleFooter()">\n    <ion-title>Footer</ion-title>\n  </ion-toolbar> -->\n  <ion-content>\n    <ion-row padding>\n      <ion-col style="text-align: center; font-size: 15px">Update Profile</ion-col>\n    </ion-row>\n    <form [formGroup]="credentialsForm">\n      <ion-item>\n        <ion-label floating>First Name</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'fname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Last Name</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'lname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Email Id</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'email\']" type="email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Phone Number</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'phonenum\']" type="number"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Organisation</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'org\']" type="text"></ion-input>\n      </ion-item>\n      <!-- <ion-item>\n        <ion-label>Timezone</ion-label>\n        <ion-select interface="popover">\n          <ion-option value="f">Female</ion-option>\n          <ion-option value="m">Male</ion-option>\n        </ion-select>\n      </ion-item> -->\n      <!-- <ion-item no-lines>\n        <ion-label>Dealer</ion-label>\n        <ion-checkbox item-right color="green" checked="true" *ngIf="islogin.isDealer"></ion-checkbox>\n        <ion-checkbox item-right color="gpsc" checked="true" *ngIf="!islogin.isDealer"></ion-checkbox>\n      </ion-item> -->\n\n      <ion-row>\n        <ion-col text-center>\n          <button ion-button block color="gpsc" (click)="onSignIn()">\n            Update Account\n          </button>\n        </ion-col>\n      </ion-row>\n\n    </form>\n  </ion-content>\n</ion-pullup>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
    ], ProfilePage);
    return ProfilePage;
}());

var ServiceProviderPage = /** @class */ (function () {
    function ServiceProviderPage(navParam) {
        this.navParam = navParam;
        this.uData = {};
        this.sorted = [];
        this.uData = this.navParam.get("param");
        if (this.uData.Dealer_ID != undefined) {
            this.sorted = this.uData.Dealer_ID;
        }
        else {
            this.sorted.first_name = this.uData.fn;
            this.sorted.last_name = this.uData.fn;
            this.sorted.phone = this.uData.phn;
            this.sorted.email = this.uData.email;
        }
        console.log("udata=> ", this.uData);
        console.log("udata=> ", JSON.stringify(this.uData));
    }
    ServiceProviderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/profile/service-provider.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Provider Info</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <div class="div">\n\n        <img src="assets/imgs/dummy_user.jpg">\n\n    </div>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Name</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.first_name}}&nbsp;{{sorted.last_name}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Contacts</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Mobile</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>E-mail</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.email}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Address</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">N/A</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/profile/service-provider.html"*/,
            selector: 'page-profile'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ServiceProviderPage);
    return ServiceProviderPage;
}());

var UpdatePasswordPage = /** @class */ (function () {
    function UpdatePasswordPage(navParam, alertCtrl, toastCtrl, apiSrv, navCtrl) {
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiSrv = apiSrv;
        this.navCtrl = navCtrl;
        this.passData = this.navParam.get("param");
        console.log("passData=> " + JSON.stringify(this.passData));
    }
    UpdatePasswordPage.prototype.savePass = function () {
        var _this = this;
        if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
            var alert_2 = this.alertCtrl.create({
                message: 'Fields should not be empty!',
                buttons: ['OK']
            });
            alert_2.present();
        }
        else {
            if (this.newP != this.cnewP) {
                var alert_3 = this.alertCtrl.create({
                    message: 'Password Missmatched!!',
                    buttons: ['Try Again']
                });
                alert_3.present();
            }
            else {
                var data = {
                    "ID": this.passData._id,
                    "OLD_PASS": this.oldP,
                    "NEW_PASS": this.newP
                };
                this.apiSrv.startLoading().present();
                this.apiSrv.updatePassword(data)
                    .subscribe(function (respData) {
                    _this.apiSrv.stopLoading();
                    console.log("respData=> ", respData);
                    var toast = _this.toastCtrl.create({
                        message: 'Password Updated successfully',
                        position: "bottom",
                        duration: 2000
                    });
                    toast.onDidDismiss(function () {
                        _this.oldP = "";
                        _this.newP = "";
                        _this.cnewP = "";
                    });
                    toast.present();
                }, function (err) {
                    _this.apiSrv.stopLoading();
                    console.log("error in update password=> ", err);
                    // debugger
                    if (err.message == "Timeout has occurred") {
                        // alert("the server is taking much time to respond. Please try in some time.")
                        var alerttemp = _this.alertCtrl.create({
                            message: "the server is taking much time to respond. Please try in some time.",
                            buttons: [{
                                    text: 'Okay',
                                    handler: function () {
                                        _this.navCtrl.setRoot("DashboardPage");
                                    }
                                }]
                        });
                        alerttemp.present();
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: err._body.message,
                            position: "bottom",
                            duration: 2000
                        });
                        toast.onDidDismiss(function () {
                            _this.oldP = "";
                            _this.newP = "";
                            _this.cnewP = "";
                            _this.navCtrl.setRoot("DashboardPage");
                        });
                        toast.present();
                    }
                });
            }
        }
    };
    UpdatePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/profile/update-password.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Change Password</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="savePass()">\n\n                Save\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="Old Password" [(ngModel)]="oldP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="New Password" [(ngModel)]="newP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="Confirm New Password" [(ngModel)]="cnewP"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/profile/update-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], UpdatePasswordPage);
    return UpdatePasswordPage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDevicesPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sms__ = __webpack_require__(409);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddDevicesPage = /** @class */ (function () {
    function AddDevicesPage(navCtrl, navParams, apiCall, alertCtrl, toastCtrl, sms, modalCtrl, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.sms = sms;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.allDevices = [];
        this.allDevicesSearch = [];
        this.option_switch = false;
        this.searchCountryString = ''; // initialize your searchCountryString string empty
        this.page = 0;
        this.limit = 5;
        this.searchQuery = '';
        this.clicked = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.isSuperAdmin = this.islogin.isSuperAdmin;
        this.islogindealer = localStorage.getItem('isDealervalue');
        console.log("islogindealer=> " + this.islogindealer);
        if (navParams.get("label") && navParams.get("value")) {
            this.stausdevice = localStorage.getItem('status');
        }
        else {
            // localStorage.removeItem("status");
            this.stausdevice = undefined;
        }
    }
    AddDevicesPage.prototype.fonctionTest = function (d) {
        var _this = this;
        console.log("clicked: " + JSON.stringify(d));
        var theft;
        theft = !(d.theftAlert);
        if (theft) {
            var alert_1 = this.alertCtrl.create({
                title: "Confirm",
                message: "Are you sure you want to activate anti theft alarm? On activating this alert you will get receive notification if vehicle moves.",
                buttons: [
                    {
                        text: 'YES PROCEED',
                        handler: function () {
                            var payload = {
                                "_id": d._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: "Anti theft alarm Activated!",
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                _this.getdevices();
                            });
                        }
                    },
                    {
                        text: 'BACK'
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: "Confirm",
                message: "Are you sure you want to deactivate anti theft alarm?",
                buttons: [
                    {
                        text: 'YES PROCEED',
                        handler: function () {
                            var payload = {
                                "_id": d._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: "Anti theft alarm Deactivated!",
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                _this.getdevices();
                            });
                        }
                    },
                    {
                        text: 'BACK'
                    }
                ]
            });
            alert_2.present();
        }
    };
    AddDevicesPage.prototype.ngOnInit = function () {
        this.now = new Date().toISOString();
    };
    AddDevicesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
        this.getdevices();
        console.log(this.isDealer);
    };
    AddDevicesPage.prototype.activateVehicle = function (data) {
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": data
        });
    };
    // ngAfterViewInit(): void {
    //   this.intro();
    // }
    // ngAfterViewInit() {
    //   if (localStorage.getItem("INTRO") != null)
    //     this.startIntroJs(); // Maybe already present
    //   this.steps.changes.subscribe((list: QueryList<ElementRef>) => {
    //     if (localStorage.getItem("INTRO") != null)
    //       this.startIntroJs(); // Created later
    //   });
    // }
    // private startIntroJs(): void {
    //   if (this.steps.length > 4) {
    //     const intro = introJs.introJs();
    //     intro.setOptions({
    //       steps: [
    //         {
    //           // element: this.steps.toArray()[0].nativeElement,
    //           element: document.querySelector('#anjali0'),
    //           intro: 'Now you can upload and view the documents related to vehicle.'
    //         }
    //       ]
    //     });
    //     intro.start();
    //     localStorage.removeItem("INTRO");
    //   }
    // }
    // intro() {
    //   let intro = introJs.introJs();
    //   intro.setOptions({
    //     steps: [
    //       // {
    //       //   intro: "Welcome to Oneqlik VTS!"
    //       // },
    //       {
    //         element: document.querySelector('#step1'),
    //         intro: "Now you can upload and view the documents related to vehicle.",
    //         position: 'bottom'
    //       }
    //     ]
    //   });
    //   intro.start();
    // }
    AddDevicesPage.prototype.timeoutAlert = function () {
        var _this = this;
        var alerttemp = this.alertCtrl.create({
            message: "the server is taking much time to respond. Please try in some time.",
            buttons: [{
                    text: 'Okay',
                    handler: function () {
                        _this.navCtrl.setRoot("DashboardPage");
                    }
                }]
        });
        alerttemp.present();
    };
    AddDevicesPage.prototype.showVehicleDetails = function (vdata) {
        this.navCtrl.push('VehicleDetailsPage', {
            param: vdata
        });
    };
    AddDevicesPage.prototype.doRefresh = function (refresher) {
        var that = this;
        that.page = 0;
        that.limit = 5;
        console.log('Begin async operation', refresher);
        this.getdevices();
        refresher.complete();
    };
    AddDevicesPage.prototype.shareVehicle = function (d_data) {
        var that = this;
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            inputs: [
                {
                    name: 'device_name',
                    value: d_data.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: 'Enter Email Id/Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        that.sharedevices(data, d_data);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.sharedevices = function (data, d_data) {
        var _this = this;
        var that = this;
        var devicedetails = {
            "did": d_data._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    AddDevicesPage.prototype.showDeleteBtn = function (b) {
        // debugger;
        var that = this;
        if (localStorage.getItem('isDealervalue') == 'true') {
            return false;
        }
        else {
            if (b) {
                var u = b.split(",");
                for (var p = 0; p < u.length; p++) {
                    if (that.islogin._id == u[p]) {
                        return true;
                    }
                }
            }
            else {
                return false;
            }
        }
    };
    AddDevicesPage.prototype.sharedVehicleDelete = function (device) {
        var that = this;
        that.deivceId = device;
        var alert = that.alertCtrl.create({
            message: 'Do you want to delete this share vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        that.removeDevice(that.deivceId._id);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    AddDevicesPage.prototype.removeDevice = function (did) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
            .subscribe(function (data) {
            console.log(data);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: 'Shared Device was deleted successfully!',
                duration: 1500
            });
            toast.onDidDismiss(function () {
                _this.getdevices();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AddDevicesPage.prototype.showSharedBtn = function (a, b) {
        // debugger
        if (b) {
            return !(b.split(",").indexOf(a) + 1);
        }
        else {
            return true;
        }
    };
    AddDevicesPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var popover = this.popoverCtrl.create(PopoverPage, {
            vehData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this.getdevices();
        });
        popover.present({
            ev: ev
        });
    };
    AddDevicesPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            var baseURLp;
            if (that.stausdevice) {
                baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
            }
            else {
                baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
            }
            if (_this.islogin.isSuperAdmin == true) {
                baseURLp += '&supAdmin=' + _this.islogin._id;
            }
            else {
                if (_this.islogin.isDealer == true) {
                    baseURLp += '&dealer=' + _this.islogin._id;
                }
            }
            that.ndata = [];
            that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(function (res) {
                that.ndata = res.devices;
                for (var i = 0; i < that.ndata.length; i++) {
                    that.allDevices.push(that.ndata[i]);
                }
                that.allDevicesSearch = that.allDevices;
            }, function (error) {
                console.log(error);
            });
            infiniteScroll.complete();
        }, 500);
    };
    AddDevicesPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp;
        if (this.stausdevice) {
            baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.ndata = data.devices;
            _this.allDevices = _this.ndata;
            _this.allDevicesSearch = _this.ndata;
            var that = _this;
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
            // if (localStorage.getItem("INTRO") != null)
            // this.intro();
            ///////////////////////////////
        }, function (err) {
            console.log("error=> ", err);
            _this.apiCall.stopLoading();
            // if (err.message == "Timeout has occurred") {
            //   this.timeoutAlert();
            // }
        });
    };
    AddDevicesPage.prototype.livetrack = function (device) {
        localStorage.setItem("LiveDevice", "LiveDevice");
        this.navCtrl.push('LiveSingleDevice', {
            device: device
        });
    };
    AddDevicesPage.prototype.showHistoryDetail = function (device) {
        this.navCtrl.push('HistoryDevicePage', {
            device: device
        });
    };
    AddDevicesPage.prototype.device_address = function (device, index) {
        var that = this;
        that.allDevices[index].address = "N/A";
        if (!device.last_location) {
            that.allDevices[index].address = "N/A";
        }
        else if (device.last_location) {
            var lattitude = device.last_location.lat;
            var longitude = device.last_location.long;
            var geocoder = new google.maps.Geocoder;
            var latlng = new google.maps.LatLng(lattitude, longitude);
            var request = {
                "latLng": latlng
            };
            geocoder.geocode(request, function (resp, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (resp[0] != null) {
                        that.allDevices[index].address = resp[0].formatted_address;
                    }
                    else {
                        console.log("No address available");
                    }
                }
                else {
                    that.allDevices[index].address = 'N/A';
                }
            });
        }
    };
    // $('#chk1').change(function() {
    //   alert($(this).prop('checked'))
    // })
    AddDevicesPage.prototype.callSearch = function (ev) {
        var _this = this;
        console.log(ev.target.value);
        var searchKey = ev.target.value;
        var _baseURL;
        if (this.islogin.isDealer == true) {
            _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        }
        else {
            if (this.islogin.isSuperAdmin == true) {
                _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
            }
            else {
                _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
            }
        }
        // this.apiCall.startLoading().present();
        // this.apiCall.callSearchService(this.islogin.email, this.islogin._id, searchKey)
        this.apiCall.callSearchService(_baseURL)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            console.log("search result=> " + JSON.stringify(data));
            _this.allDevicesSearch = data.devices;
            _this.allDevices = data.devices;
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.IgnitionOnOff = function (d) {
        var _this = this;
        var that = this;
        if (d.last_ACC != null || d.last_ACC != undefined) {
            if (that.clicked) {
                var alert_3 = this.alertCtrl.create({
                    message: 'Process ongoing..',
                    buttons: ['Okay']
                });
                alert_3.present();
            }
            else {
                this.messages = undefined;
                this.dataEngine = d;
                var baseURLp = 'https://www.oneqlik.in/deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
                this.apiCall.startLoading().present();
                this.apiCall.ignitionoffCall(baseURLp)
                    .subscribe(function (data) {
                    _this.apiCall.stopLoading();
                    _this.DeviceConfigStatus = data;
                    _this.immobType = data[0].imobliser_type;
                    if (_this.dataEngine.ignitionLock == '1') {
                        _this.messages = 'Do you want to unlock the engine?';
                    }
                    else {
                        if (_this.dataEngine.ignitionLock == '0') {
                            _this.messages = 'Do you want to lock the engine?';
                        }
                    }
                    var alert = _this.alertCtrl.create({
                        message: _this.messages,
                        buttons: [{
                                text: 'YES',
                                handler: function () {
                                    if (_this.immobType == 0 || _this.immobType == undefined) {
                                        that.clicked = true;
                                        var devicedetail = {
                                            "_id": _this.dataEngine._id,
                                            "engine_status": !_this.dataEngine.engine_status
                                        };
                                        // this.apiCall.startLoading().present();
                                        _this.apiCall.deviceupdateCall(devicedetail)
                                            .subscribe(function (response) {
                                            // this.apiCall.stopLoading();
                                            _this.editdata = response;
                                            var toast = _this.toastCtrl.create({
                                                message: response.message,
                                                duration: 2000,
                                                position: 'top'
                                            });
                                            toast.present();
                                            _this.responseMessage = "Edit successfully";
                                            _this.getdevices();
                                            var msg;
                                            if (!_this.dataEngine.engine_status) {
                                                msg = _this.DeviceConfigStatus[0].resume_command;
                                            }
                                            else {
                                                msg = _this.DeviceConfigStatus[0].immoblizer_command;
                                            }
                                            _this.sms.send(d.sim_number, msg);
                                            var toast1 = _this.toastCtrl.create({
                                                message: 'SMS sent successfully',
                                                duration: 2000,
                                                position: 'bottom'
                                            });
                                            toast1.present();
                                            that.clicked = false;
                                        }, function (error) {
                                            that.clicked = false;
                                            // this.apiCall.stopLoading();
                                            console.log(error);
                                        });
                                    }
                                    else {
                                        console.log("Call server code here!!");
                                        that.clicked = true;
                                        var data = {
                                            "imei": d.Device_ID,
                                            "_id": _this.dataEngine._id,
                                            "engine_status": d.ignitionLock,
                                            "protocol_type": d.device_model.device_type
                                        };
                                        // this.apiCall.startLoading().present();
                                        _this.apiCall.serverLevelonoff(data)
                                            .subscribe(function (resp) {
                                            // this.apiCall.stopLoading();
                                            console.log("ignition on off=> ", resp);
                                            _this.respMsg = resp;
                                            // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
                                            _this.intervalID = setInterval(function () {
                                                _this.apiCall.callResponse(_this.respMsg._id)
                                                    .subscribe(function (data) {
                                                    console.log("interval=> " + data);
                                                    _this.commandStatus = data.status;
                                                    if (_this.commandStatus == 'SUCCESS') {
                                                        clearInterval(_this.intervalID);
                                                        that.clicked = false;
                                                        // this.apiCall.stopLoadingnw();
                                                        var toast1 = _this.toastCtrl.create({
                                                            message: 'process has completed successfully!',
                                                            duration: 1500,
                                                            position: 'bottom'
                                                        });
                                                        toast1.present();
                                                        _this.getdevices();
                                                    }
                                                });
                                            }, 5000);
                                        }, function (err) {
                                            _this.apiCall.stopLoading();
                                            console.log("error in onoff=>", err);
                                            that.clicked = false;
                                        });
                                    }
                                }
                            },
                            {
                                text: 'No'
                            }]
                    });
                    alert.present();
                }, function (error) {
                    _this.apiCall.stopLoading();
                    console.log(error);
                });
            }
        }
    };
    ;
    AddDevicesPage.prototype.dialNumber = function (number) {
        window.open('tel:' + number, '_system');
    };
    AddDevicesPage.prototype.getItems = function (ev) {
        console.log(ev.target.value, this.allDevices);
        var val = ev.target.value.trim();
        this.allDevicesSearch = this.allDevices.filter(function (item) {
            return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.allDevicesSearch);
    };
    AddDevicesPage.prototype.onClear = function (ev) {
        // debugger;
        this.getdevices();
        ev.target.value = '';
    };
    AddDevicesPage.prototype.openAdddeviceModal = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create('AddDeviceModalPage');
        profileModal.onDidDismiss(function (data) {
            console.log(data);
            _this.getdevices();
        });
        profileModal.present();
    };
    AddDevicesPage.prototype.upload = function (vehData) {
        // let mod = this.modalCtrl.create('UploadDocPage',
        //   { vehData: vehData });
        // mod.present();
        // mod.onDidDismiss(() => {
        //   // this.viewCtrl.dismiss();
        // })
        this.navCtrl.push('UploadDocPage', { vehData: vehData });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])("step"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"])
    ], AddDevicesPage.prototype, "steps", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], AddDevicesPage.prototype, "navBar", void 0);
    AddDevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/add-devices/add-devices.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Vehicle List</ion-title>\n    <!-- <ion-buttons end *ngIf="isDealer || islogindealer"> -->\n    <ion-buttons end *ngIf="isDealer || isSuperAdmin">\n      <button ion-button icon-only (click)="openAdddeviceModal()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <!-- <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar> -->\n  <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-card *ngFor="let d of allDevicesSearch; let i=index;">\n    <div *ngIf="d.expiration_date < now " style="position: relative">\n      <div>\n        <ion-item style="background-color: rgba(0, 0, 0, 0.9)">\n          <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n              *ngIf="(d.vehicleType.iconType == \'car\')">\n            <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n              *ngIf="(d.vehicleType.iconType == \'truck\')">\n            <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n              *ngIf="(d.vehicleType.iconType == \'bike\')">\n            <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'jcb\')">\n            <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'bus\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'user\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}"\n              *ngIf="(d.vehicleType.iconType == \'tractor\')">\n          </ion-avatar>\n\n          <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n              *ngIf="(d.vehicleType.iconType == \'car\')">\n            <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n              *ngIf="(d.vehicleType.iconType == \'truck\')">\n            <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n              *ngIf="(d.vehicleType.iconType == \'bike\')">\n            <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'jcb\')">\n            <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'bus\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'user\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}"\n              *ngIf="(d.vehicleType.iconType == \'tractor\')">\n          </ion-avatar>\n\n          <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'car\')">\n            <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'truck\')">\n            <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'bike\')">\n            <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'jcb\')">\n            <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'bus\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'user\')">\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'tractor\')">\n          </ion-avatar>\n\n          <div>\n            <h2 style="color: gray">{{d.Device_Name}}</h2>\n            <p style="color:gray;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n              <span style="text-transform: uppercase; color: red;">{{d.status}} </span>\n              <span *ngIf="d.status_updated_at ">since&nbsp;{{d.status_updated_at | date:\'medium\'}} </span>\n            </p>\n          </div>\n\n          <button ion-button item-end clear *ngIf="!showDeleteBtn(d.SharedWith)">\n            <ion-icon ios="ios-more" md="md-more" color="grey" *ngIf="option_switch"></ion-icon>\n          </button>\n\n          <button ion-button item-end clear *ngIf="!option_switch">\n            <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id,d.SharedWith)"></ion-icon>\n          </button>\n\n          <button ion-button item-end clear *ngIf="showDeleteBtn(d.SharedWith)">\n            <ion-icon ios="ios-remove-circle" md="md-remove-circle" color="danger"></ion-icon>\n          </button>\n\n          <div item-end *ngIf="d.currentFuel">\n            <ion-avatar class="ava">\n              <img src="assets/imgs/fuel.png">\n            </ion-avatar>\n            <p style="color: gray; font-size: 10px;font-weight: 400;margin:0px;">{{d.currentFuel}}L</p>\n          </div>\n        </ion-item>\n\n        <ion-row style="background-color: rgba(0, 0, 0, 0.9)" padding-right>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="pin" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Live</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="clock" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">History</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="power" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Ignition</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="lock" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock==\'1\'"></ion-icon>\n            <ion-icon name="unlock" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock==\'0\'"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Immobilize</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="battery-charging" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Power</p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="call" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Driver</p>\n          </ion-col>\n        </ion-row>\n\n        <ion-item item-start class="itemStyle" style="background-color: rgba(0, 0, 0, 0.9)">\n          <ion-row>\n            <ion-col (onCreate)="device_address(d,i)" class="colSt2">\n              <div class="overme" style="color: gray">\n                {{d.address}}\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </div>\n      <div>\n        <button class="activeBtn" ion-button outline color="secondary" (tap)="activateVehicle(d)">Activate</button>\n      </div>\n    </div>\n\n    <div *ngIf="(d.expiration_date == null) || (d.expiration_date > now)">\n      <ion-item>\n        <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null"\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\' || d.vehicleType.iconType == \'Heavy Truck\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\' || d.vehicleType.iconType == \'Heavy Truck\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\' || d.vehicleType.iconType == \'Heavy Truck\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/tractor_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&( !d.last_ACC ))">\n        </ion-avatar>\n\n        <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null"\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/tractor_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&( !d.last_ACC ))">\n        </ion-avatar>\n\n        <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null"\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'car\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'car\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'car\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==null))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bus\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bus\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bus\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'user\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'user\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}" *ngIf="((d.iconType == \'user\')&&( !d.last_ACC ))">\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'tractor\')&&(d.last_ACC==\'0\'))">\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'tractor\')&&(d.last_ACC==\'1\'))">\n          <img src="assets/imgs/tractor_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'tractor\')&&( !d.last_ACC ))">\n        </ion-avatar>\n\n        <div>\n          <h2>{{d.Device_Name}} &nbsp;&nbsp;\n            <label class="switch">\n              <input type="checkbox" [(ngModel)]="d.theftAlert" (click)="fonctionTest(d)">\n              <span class="slider round"></span>\n            </label>\n          </h2>\n          <p style="color:#cf1c1c;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n            <span style="text-transform: uppercase;">{{d.status}} </span>\n            <span *ngIf="d.status_updated_at ">since&nbsp;{{d.status_updated_at | date:\'medium\'}} </span>\n          </p>\n        </div>\n        <button #step id="anjali{{i}}" ion-button item-end clear color="gpsc" (click)="upload(d)"\n          style="font-size: 1.2em; margin: 0px;">\n          <ion-icon name="cloud-upload"></ion-icon>\n        </button>\n\n        <button ion-button item-end clear color="gpsc" (click)="presentPopover($event, d);$event.stopPropagation()"\n          *ngIf="!showDeleteBtn(d.SharedWith)" style="font-size: 1.2em; margin: 0px;">\n          <ion-icon ios="ios-more" md="md-more" *ngIf="option_switch"></ion-icon>\n        </button>\n\n        <button ion-button item-end clear color="gpsc" (click)="shareVehicle(d)" *ngIf="!option_switch"\n          style="font-size: 1.2em; margin: 0px;">\n          <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id,d.SharedWith)"></ion-icon>\n        </button>\n\n        <button ion-button item-end clear color="danger" (click)="sharedVehicleDelete(d)"\n          *ngIf="showDeleteBtn(d.SharedWith)" style="font-size: 1.2em; margin: 0px;">\n          <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n        </button>\n\n        <div item-end *ngIf="d.currentFuel">\n          <ion-avatar class="ava">\n            <img src="assets/imgs/fuel.png">\n          </ion-avatar>\n          <p style="color: green; font-size: 10px;font-weight: 400;margin:0px;">{{d.currentFuel}}L</p>\n        </div>\n      </ion-item>\n\n      <ion-row style="background-color: #fafafa;" padding-right>\n        <ion-col col-2 style="text-align:center" (click)="livetrack(d)">\n          <ion-icon name="pin" style="color:#ffc900;font-size: 12px;"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Live</p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center" (click)="showHistoryDetail(d)">\n          <ion-icon name="clock" style="color:#d675ea;font-size: 12px;"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">History</p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center">\n          <ion-icon name="power" style="color:#ef473a;font-size: 12px;" *ngIf="d.last_ACC==\'0\'"></ion-icon>\n          <ion-icon name="power" style="color:#1de21d;font-size: 12px;" *ngIf="d.last_ACC==\'1\'"></ion-icon>\n          <ion-icon name="power" style="color:gray;font-size: 12px;" *ngIf="d.last_ACC==null"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Ignition</p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center" (click)="IgnitionOnOff(d)">\n          <ion-icon name="lock" style="color:#ef473a;font-size: 12px;" *ngIf="d.ignitionLock==\'1\'"></ion-icon>\n          <ion-icon name="unlock" style="color:#1de21d;font-size: 12px;" *ngIf="d.ignitionLock==\'0\'"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Immobilize</p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center">\n          <ion-icon name="battery-charging" style="color:#ef473a;font-size: 12px;" *ngIf="d.power!=\'1\'"></ion-icon>\n          <ion-icon name="battery-charging" style="color:#1de21d;font-size: 12px;" *ngIf="d.power==\'1\'"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Power</p>\n        </ion-col>\n        <!-- <ion-col col-2 style="text-align:center">\n          <img src="assets/imgs/f2bd6f65e39f0261b62df07fa42122c8.png"  *ngIf="d.power!=\'1\'">\n          <img src="assets/imgs/f2bd6f65e39f0261b62df07fa42122c8.png" *ngIf="d.power==\'1\'">\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Theft</p>\n        </ion-col> -->\n        <ion-col col-2 style="text-align:center" (click)="dialNumber(d.contact_number)">\n          <ion-icon name="call" style="color:#0000FF;font-size: 12px;"></ion-icon>\n          <p style="color:#131212;font-size: 10px;font-weight: 400;">Driver</p>\n        </ion-col>\n      </ion-row>\n      <ion-item item-start class="itemStyle" style="background:#696D74;">\n        <ion-row>\n          <ion-col (onCreate)="device_address(d,i)" class="colSt2">\n            <div class="overme">\n              {{d.address}}\n            </div>\n          </ion-col>\n          <ion-col class="colSt1">\n            <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">Today\'s distance</p>\n            <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">\n              {{d.today_odo | number : \'1.0-2\'}}Kms</p>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </div>\n  </ion-card>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/add-devices/add-devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], AddDevicesPage);
    return AddDevicesPage;
}());

var PopoverPage = /** @class */ (function () {
    function PopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.vehData = navParams.get("vehData");
        console.log("popover data=> ", this.vehData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    PopoverPage.prototype.ngOnInit = function () {
        // if (this.navParams.data) {
        //   this.contentEle = this.navParams.data.contentEle;
        //   this.textEle = this.navParams.data.textEle;
        // }
    };
    // ngAfterViewInit(): void {
    //   if (localStorage.getItem("INTRO") != null)
    //     this.intro();
    // }
    // intro() {
    //   let intro = introJs.introJs();
    //   intro.setOptions({
    //     steps: [
    //       {
    //         intro: "Welcome to Oneqlik VTS!"
    //       },
    //       {
    //         element: document.querySelector('#step1'),
    //         intro: "Now you can upload and view the documents related to vehicle.",
    //         position: 'bottom'
    //       }
    //     ]
    //   });
    //   intro.start();
    //   localStorage.removeItem("INTRO");
    // }
    PopoverPage.prototype.editItem = function () {
        console.log("edit");
        this.navCtrl.push('UpdateDevicePage', {
            vehData: this.vehData
        });
        this.viewCtrl.dismiss();
        // let modal = this.modalCtrl.create('UpdateDevicePage', {
        //   vehData: this.vehData
        // });
        // modal.onDidDismiss(() => {
        //   console.log("modal dismissed!")
        //   this.viewCtrl.dismiss();
        // })
        // modal.present();
    };
    PopoverPage.prototype.deleteItem = function () {
        var that = this;
        console.log("delete");
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    PopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.deleteDeviceCall(d_id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Vehicle deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                // this.navCtrl.push(AddDevicesPage);
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PopoverPage.prototype.shareItem = function () {
        var that = this;
        console.log("share");
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'device_name',
                    value: that.vehData.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: 'Enter Email Id/Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("clicked=> ", data);
                        that.sharedevices(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    PopoverPage.prototype.sharedevices = function (data) {
        var _this = this;
        var that = this;
        console.log(data.shareId);
        var devicedetails = {
            "did": that.vehData._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var editdata = data;
            console.log(editdata);
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            // toast.onDidDismiss(() => {
            //   console.log('Dismissed toast');
            // });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PopoverPage.prototype.upload = function () {
        var _this = this;
        var mod = this.modalCtrl.create('UploadDocPage', { vehData: this.vehData });
        mod.present();
        mod.onDidDismiss(function () {
            _this.viewCtrl.dismiss();
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteItem()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n      \n    </ion-list>\n  "
        })
        // <ion-item id="step1" class="text-seravek" (click)="upload()">
        //        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;Docs
        //        </ion-item>
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=add-devices.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiveSingleDevice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PoiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LiveSingleDevice = /** @class */ (function () {
    function LiveSingleDevice(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, platform, 
    // private geolocation: Geolocation,
    modalCtrl, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.drawerHidden = true;
        this.drawerHidden1 = false;
        this.shouldBounce = true;
        this.dockedHeight = 150;
        this.bounceThreshold = 500;
        this.distanceTop = 56;
        this.showaddpoibtn = false;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.selectedFlag = { _id: '', flag: false };
        this.allData = {};
        this.isEnabled = false;
        this.showShareBtn = false;
        this.mapHideTraffic = false;
        this.locateme = false;
        this.deviceDeatils = {};
        this.tempArray = [];
        this.mapTrail = false;
        this.condition2 = 'light';
        this.condition = 'gpsc';
        this.condition1 = 'light';
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z";
        this.truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [10, 20],
        };
        this.busIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.bikeIcon = {
            path: this.bike,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.truckIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [10, 20],
        };
        this.userIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.bikeIcon,
            "truck": this.truckIcon,
            "bus": this.busIcon,
            "user": this.userIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon
        };
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        if (navParams.get("device") != null) {
            this.deviceDeatils = navParams.get("device");
        }
        this.motionActivity = 'Activity';
        this.menuActive = false;
        setTimeout(function () {
            debugger;
            document.getElementById('button_wa').click();
        }, 500);
    }
    LiveSingleDevice.prototype.callme = function () {
        console.log("m here");
    };
    LiveSingleDevice.prototype.newMap = function () {
        // this.geolocation.getCurrentPosition().then((position) => {
        //   var pos = {
        //     lat: position.coords.latitude,
        //     lng: position.coords.longitude
        //   };
        //   map.setCameraTarget(pos);
        // }).catch((error) => {
        //   console.log('Error getting location', error);
        // });
        var mapOptions = {
            camera: { zoom: 10 },
            gestures: {
                rotate: false,
                tilt: false,
            }
        };
        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
        return map;
    };
    LiveSingleDevice.prototype.ngOnInit = function () {
        var that = this;
        that.tempArray = [];
    };
    LiveSingleDevice.prototype.ionViewDidLoad = function () {
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"]('https://www.oneqlik.in/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        this.showShareBtn = true;
        var paramData = this.navParams.get("device");
        this.titleText = paramData.Device_Name;
        this.temp(paramData);
        this.showActionSheet = true;
    };
    LiveSingleDevice.prototype.ngOnDestroy = function () {
        var that = this;
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that._io.on('disconnect', function () {
            that._io.open();
        });
    };
    LiveSingleDevice.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE') {
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].SATELLITE);
        }
        else {
            if (maptype == 'TERRAIN') {
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
            }
            else {
                if (maptype == 'NORMAL') {
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                }
            }
        }
    };
    LiveSingleDevice.prototype.trafficFunc = function () {
        var that = this;
        that.isEnabled = !that.isEnabled;
        if (that.isEnabled == true) {
            that.allData.map.setTrafficEnabled(true);
        }
        else {
            that.allData.map.setTrafficEnabled(false);
        }
    };
    LiveSingleDevice.prototype.shareLive = function () {
        var that = this;
        that.showActionSheet = false;
        that.drawerHidden = false;
        that.showFooter = true;
    };
    LiveSingleDevice.prototype.sharedevices = function (param) {
        var that = this;
        if (param == '15mins') {
            that.condition = 'gpsc';
            that.condition1 = 'light';
            that.condition2 = 'light';
            that.tttime = 15;
            // that.tttime  = (15 * 60000); //for miliseconds
        }
        else {
            if (param == '1hour') {
                that.condition1 = 'gpsc';
                that.condition = 'light';
                that.condition2 = 'light';
                that.tttime = 60;
                // that.tttime  = (1 * 3600000); //for miliseconds
            }
            else {
                if (param == '8hours') {
                    that.condition2 = 'gpsc';
                    that.condition = 'light';
                    that.condition1 = 'light';
                    that.tttime = (8 * 60);
                    // that.tttime  = (8 * 3600000);
                }
            }
        }
    };
    LiveSingleDevice.prototype.shareLivetemp = function () {
        var _this = this;
        var that = this;
        if (that.tttime == undefined) {
            that.tttime = 15;
        }
        var data = {
            id: that.liveDataShare._id,
            imei: that.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: that.tttime // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LiveSingleDevice.prototype.liveShare = function () {
        var that = this;
        var link = "https://www.oneqlik.in/share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
        that.showActionSheet = true;
        that.showFooter = false;
        that.tttime = undefined;
        // that.navCtrl.setRoot("AddDevicesPage");
    };
    LiveSingleDevice.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LiveSingleDevice.prototype.addPOI = function () {
        var _this = this;
        var that = this;
        console.log("check marker: ", that.impkey);
        console.log("adding poi part is comming soon");
        var modal = this.modalCtrl.create(PoiPage, {
            param1: that.allData.marker[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            var that = _this;
            that.showaddpoibtn = false;
            // this.getcustomer();
            // this.navCtrl.setRoot("AddPoiPage")
        });
        modal.present();
    };
    LiveSingleDevice.prototype.settings = function () {
        var _this = this;
        // var paramData = this.navParams.get("device");
        // this.temp(paramData);
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: this.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            for (var i = 0; i < that.socketChnl.length; i++)
                that._io.removeAllListeners(that.socketChnl[i]);
            var paramData = _this.navParams.get("device");
            _this.temp(paramData);
        });
    };
    LiveSingleDevice.prototype.socketInit = function (pdata, center) {
        if (center === void 0) { center = false; }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        var that = this;
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    if (data._id != undefined && data.last_location != undefined) {
                        var key = data._id;
                        that.impkey = data._id;
                        that.deviceDeatils = data;
                        var ic_1 = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.icons[data.iconType]);
                        if (!ic_1) {
                            return;
                        }
                        ic_1.path = null;
                        // ic.url = './assets/imgs/vehicles/runningjcb.png';
                        ic_1.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                        console.log("icon status=> ", ic_1.url);
                        that.vehicle_speed = data.last_speed;
                        that.todays_odo = data.today_odo;
                        that.total_odo = data.total_odo;
                        that.fuel = data.currentFuel;
                        that.last_ping_on = data.last_ping_on;
                        var tempvar = new Date(data.lastStoppedAt);
                        if (data.lastStoppedAt != null) {
                            var fd = tempvar.getTime();
                            var td = new Date().getTime();
                            var time_difference = td - fd;
                            var total_min = time_difference / 60000;
                            var hours = total_min / 60;
                            var rhours = Math.floor(hours);
                            var minutes = (hours - rhours) * 60;
                            var rminutes = Math.round(minutes);
                            that.lastStoppedAt = rhours + ':' + rminutes;
                        }
                        else {
                            that.lastStoppedAt = '00' + ':' + '00';
                        }
                        that.distFromLastStop = data.distFromLastStop;
                        if (!isNaN(data.timeAtLastStop)) {
                            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
                        }
                        else {
                            that.timeAtLastStop = '00:00:00';
                        }
                        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
                        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
                        that.last_ACC = data.last_ACC;
                        that.acModel = data.ac;
                        that.currentFuel = data.currentFuel;
                        that.power = data.power;
                        that.gpsTracking = data.gpsTracking;
                        if (that.allData[key]) {
                            that.socketSwitch[key] = data;
                            that.allData[key].mark.setIcon(ic_1);
                            that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                            var temp = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.allData[key].poly[1]);
                            that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](temp);
                            that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                        }
                        else {
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            if (data.sec_last_location) {
                                that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                            }
                            else {
                                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            }
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                that.allData.map.addMarker({
                                    title: data.Device_Name,
                                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                    icon: ic_1.url,
                                }).then(function (marker) {
                                    ///////////////////////////////
                                    // let htmlInfoWindow = new HtmlInfoWindow();
                                    // let frame: HTMLElement = document.createElement('div');
                                    // frame.innerHTML = [
                                    //     '<button onclick="myFunction()">Click me</button>'
                                    // ].join("");
                                    // htmlInfoWindow.setContent(frame, { width: "80px", height: "50px" });
                                    that.allData[key].mark = marker;
                                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                        .subscribe(function (e) {
                                        that.liveVehicleName = data.Device_Name;
                                        that.drawerHidden1 = false;
                                        that.showActionSheet = true;
                                        that.showaddpoibtn = true;
                                        // marker.showInfoWindow();
                                        // htmlInfoWindow.open(marker);
                                    });
                                    // function myFunction() {
                                    //     // htmlInfoWindow.setContent('<div style="background-color: green">' + htmlInfoWindow.getContent() + "</div>");
                                    //     console.log("m coming soon")
                                    // }
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                });
                            }
                        }
                        var geocoder = new google.maps.Geocoder;
                        var latlng = new google.maps.LatLng(data.last_location['lat'], data.last_location['long']);
                        var request = {
                            "latLng": latlng
                        };
                        geocoder.geocode(request, function (resp, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (resp[0] != null) {
                                    that.address = resp[0].formatted_address;
                                }
                                else {
                                    console.log("No address available");
                                }
                            }
                            else {
                                that.address = 'N/A';
                            }
                        });
                    }
                })(d4);
        });
    };
    LiveSingleDevice.prototype.liveTrack = function (map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["i" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                // mark.setIcon(icons);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["i" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(lat, lng, map);
                    }
                    else {
                        if (that.polylineID) {
                            that.tempArray = [];
                            that.polylineID.remove();
                        }
                    }
                    map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* LatLng */](lat, lng));
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["i" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(dest.lat, dest.lng, map);
                    }
                    else {
                        if (that.polylineID) {
                            that.tempArray = [];
                            that.polylineID.remove();
                        }
                    }
                    map.setCameraTarget(dest);
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    // socketInit(pdata, center = false) {
    //     let that = this;
    //     let key = pdata._id;
    //     that.impkey = pdata._id;
    //     if (that.allData.marker[key] == undefined) {
    //         if (pdata && pdata.last_location && pdata.last_location.lat && pdata.last_location.long) {
    //             let ddd: any = { lat: pdata.last_location.lat, lng: pdata.last_location.long };
    //             let icon = "";
    //             if (moment().diff(moment(pdata.last_ping_on), 'minutes') < 60)
    //                 icon = 'www/assets/imgs/vehicles/running' + pdata.iconType + '.png';
    //             else
    //                 icon = 'www/assets/imgs/vehicles/stopped' + pdata.iconType + '.png';
    //             that.allData.map.addMarker({
    //                 position: ddd,
    //                 title: pdata.Device_Name,
    //                 icon: icon
    //             }).then((marker: Marker) => {
    //                 that.allData.marker[pdata._id] = marker;
    //                 that.allData.marker[pdata._id].dicon = icon;
    //             })
    //         }
    //     }
    //     that._io.emit('acc', pdata.Device_ID);
    //     that.socketChnl.push(pdata.Device_ID + 'acc');
    //     that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
    //         if (d4 == null) return;
    //         that.impkey = d4._id;
    //         let dd1: any = { lat: d4.last_location.lat, lng: d4.last_location.long };
    //         let dd2: any = { lat: d4.sec_last_location.lat, lng: d4.sec_last_location.long };
    //         // debugger
    //         let ic = _.cloneDeep(that.icons[d4.iconType]);
    //         if (!ic) {
    //             return;
    //         }
    //         ic.path = null;
    //         ic.url = 'www/assets/imgs/vehicles/' + d4.status.toLowerCase() + d4.iconType + '.png';
    //         that.vehicle_speed = d4.last_speed;
    //         that.todays_odo = d4.today_odo;
    //         that.total_odo = d4.total_odo;
    //         that.fuel = d4.currentFuel;
    //         that.last_ping_on = d4.last_ping_on;
    //         var tempvar = new Date(d4.lastStoppedAt);
    //         if (d4.lastStoppedAt != null) {
    //             var fd = tempvar.getTime();
    //             var td = new Date().getTime();
    //             var time_difference = td - fd;
    //             var total_min = time_difference / 60000;
    //             var hours = total_min / 60
    //             var rhours = Math.floor(hours);
    //             var minutes = (hours - rhours) * 60;
    //             var rminutes = Math.round(minutes);
    //             that.lastStoppedAt = rhours + ':' + rminutes;
    //         } else {
    //             that.lastStoppedAt = '00' + ':' + '00';
    //         }
    //         that.distFromLastStop = d4.distFromLastStop;
    //         if (!isNaN(d4.timeAtLastStop)) {
    //             that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(d4.timeAtLastStop);
    //         } else {
    //             that.timeAtLastStop = '00:00:00';
    //         }
    //         that.today_stopped = that.parseMillisecondsIntoReadableTime(d4.today_stopped);
    //         that.today_running = that.parseMillisecondsIntoReadableTime(d4.today_running);
    //         that.last_ACC = d4.last_ACC;
    //         that.acModel = d4.ac;
    //         that.currentFuel = d4.currentFuel;
    //         that.power = d4.power;
    //         that.gpsTracking = d4.gpsTracking;
    //         that.allData.marker[d4._id].setIcon(that.allData.marker[d4._id].dicon);
    //         var geocoder = new google.maps.Geocoder;
    //         var latlng = new google.maps.LatLng(d4.last_location['lat'], d4.last_location['long']);
    //         var request = {
    //             "latLng": latlng
    //         };
    //         geocoder.geocode(request, function (resp, status) {
    //             if (status == google.maps.GeocoderStatus.OK) {
    //                 if (resp[0] != null) {
    //                     that.address = resp[0].formatted_address;
    //                 } else {
    //                     console.log("No address available")
    //                 }
    //             }
    //             else {
    //                 that.address = 'N/A';
    //             }
    //         })
    //         if (that.allData.marker[d4._id] == undefined) {
    //             that.allData.marker[key].remove();
    //             var icon1;
    //             icon1 = ic.url;
    //             that.allData.map.addMarker({
    //                 position: dd1,
    //                 title: d4.Device_Name,
    //                 icon: icon1
    //             }).then((marker: Marker) => {
    //                 debugger
    //                 that.allData.marker[d4._id] = marker;
    //                 that.allData.marker[d4._id].dicon = icon1;
    //             })
    //         } else {
    //             let icon = "";
    //             if (moment().diff(moment(d4.last_ping_on), 'minutes') < 60)
    //                 that.allData.marker[d4._id].icon = 'www/assets/imgs/vehicles/running' + d4.iconType + '.png';
    //             else
    //                 that.allData.marker[d4._id].icon = 'www/assets/imgs/vehicles/stopped' + d4.iconType + '.png';
    //             that.allData.marker[d4._id].addEventListener(GoogleMapsEvent.MARKER_CLICK)
    //                 .subscribe(e => {
    //                     that.liveVehicleName = d4.Device_Name;
    //                     that.drawerHidden1 = false;
    //                     that.showActionSheet = true;
    //                     that.showaddpoibtn = true;
    //                 });
    //             if (that.ongoingMoveMarker[d4._id])
    //                 clearTimeout(that.ongoingMoveMarker[d4._id]);
    //             if (that.ongoingGoToPoint[d4._id])
    //                 clearTimeout(that.ongoingGoToPoint[d4._id]);
    //             that.allData.marker[d4._id].setPosition(dd2);
    //             // debugger
    //             that.liveTrack(that.allData.map, that.allData.marker[d4._id], [dd1], 50, 10, d4._id)
    //         }
    //     })
    //     // this._io.emit('acc', pdata.Device_ID);
    //     // this.socketChnl.push(pdata.Device_ID + 'acc');
    //     // this._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
    //     //     if (d4 != undefined)
    //     //         (function (data) {
    //     //             if (data == undefined) {
    //     //                 return;
    //     //             }
    //     //             if (data._id != undefined && data.last_location != undefined) {
    //     //                 var key = data._id;
    //     //                 let ic = _.cloneDeep(that.icons[data.iconType]);
    //     //                 if (!ic) {
    //     //                     return;
    //     //                 }
    //     //                 ic.path = null;
    //     //                 ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
    //     //                 console.log("icon status=> ", ic.url)
    //     //                 that.vehicle_speed = data.last_speed;
    //     //                 that.todays_odo = data.today_odo;
    //     //                 that.total_odo = data.total_odo;
    //     //                 that.fuel = data.currentFuel;
    //     //                 that.last_ping_on = data.last_ping_on;
    //     //                 var tempvar = new Date(data.lastStoppedAt);
    //     //                 if (data.lastStoppedAt != null) {
    //     //                     var fd = tempvar.getTime();
    //     //                     var td = new Date().getTime();
    //     //                     var time_difference = td - fd;
    //     //                     var total_min = time_difference / 60000;
    //     //                     var hours = total_min / 60
    //     //                     var rhours = Math.floor(hours);
    //     //                     var minutes = (hours - rhours) * 60;
    //     //                     var rminutes = Math.round(minutes);
    //     //                     that.lastStoppedAt = rhours + ':' + rminutes;
    //     //                 } else {
    //     //                     that.lastStoppedAt = '00' + ':' + '00';
    //     //                 }
    //     //                 that.distFromLastStop = data.distFromLastStop;
    //     //                 if (!isNaN(data.timeAtLastStop)) {
    //     //                     that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
    //     //                 } else {
    //     //                     that.timeAtLastStop = '00:00:00';
    //     //                 }
    //     //                 that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
    //     //                 that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
    //     //                 that.last_ACC = data.last_ACC;
    //     //                 that.acModel = data.ac;
    //     //                 that.currentFuel = data.currentFuel;
    //     //                 that.power = data.power;
    //     //                 that.gpsTracking = data.gpsTracking;
    //     //                 if (that.allData[key]) {
    //     //                     // console.log("entered in if")
    //     //                     that.socketSwitch[key] = data;
    //     //                     that.allData[key].mark.setIcon(ic);
    //     //                     that.allData[key].mark.setPosition(that.allData[key].poly[1]);
    //     //                     var temp = _.cloneDeep(that.allData[key].poly[1]);
    //     //                     that.allData[key].poly[0] = _.cloneDeep(temp);
    //     //                     that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
    //     //                     var speed = data.status == "RUNNING" ? data.last_speed : 0;
    //     //                     that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
    //     //                 }
    //     //                 else {
    //     //                     that.allData[key] = {};
    //     //                     that.socketSwitch[key] = data;
    //     //                     that.allData[key].poly = [];
    //     //                     if (data.sec_last_location) {
    //     //                         that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
    //     //                     } else {
    //     //                         that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
    //     //                     }
    //     //                     that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
    //     //                     if (data.last_location != undefined) {
    //     //                         that.allData.map.addMarker({
    //     //                             title: data.Device_Name,
    //     //                             position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
    //     //                             icon: ic,
    //     //                         }).then((marker: Marker) => {
    //     //                             that.allData[key].mark = marker;
    //     //                             marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
    //     //                                 .subscribe(e => {
    //     //                                     that.liveVehicleName = data.Device_Name;
    //     //                                     that.drawerHidden1 = false;
    //     //                                     that.showActionSheet = true;
    //     //                                 });
    //     //                             var speed = data.status == "RUNNING" ? data.last_speed : 0;
    //     //                             that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
    //     //                         })
    //     //                     }
    //     //                 }
    //     //                 var geocoder = new google.maps.Geocoder;
    //     //                 var latlng = new google.maps.LatLng(data.last_location['lat'], data.last_location['long']);
    //     //                 var request = {
    //     //                     "latLng": latlng
    //     //                 };
    //     //                 geocoder.geocode(request, function (resp, status) {
    //     //                     if (status == google.maps.GeocoderStatus.OK) {
    //     //                         if (resp[0] != null) {
    //     //                             that.address = resp[0].formatted_address;
    //     //                         } else {
    //     //                             console.log("No address available")
    //     //                         }
    //     //                     }
    //     //                     else {
    //     //                         that.address = 'N/A';
    //     //                     }
    //     //                 })
    //     //             }
    //     //         })(d4)
    //     // })
    // }
    // liveTrack(map, mark, coords, speed, delay, id) {
    //     let that = this;
    //     var target = 0;
    //     function _goToPoint() {
    //         if (target > coords.length)
    //             return;
    //         var lat = mark.getPosition().lat;
    //         var lng = mark.getPosition().lng;
    //         var step = (speed * 1000 * delay) / 3600000; // in meters
    //         if (coords[target] == undefined)
    //             return;
    //         var dest = new LatLng(coords[target].lat, coords[target].lng);
    //         var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
    //         var numStep = distance / step;
    //         var i = 0;
    //         var deltaLat = (coords[target].lat - lat) / numStep;
    //         var deltaLng = (coords[target].lng - lng) / numStep;
    //         function changeMarker(mark, deg) {
    //             mark.setRotation(deg);
    //         }
    //         function _moveMarker() {
    //             lat += deltaLat;
    //             lng += deltaLng;
    //             i += step;
    //             var head;
    //             if (i < distance) {
    //                 head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
    //                 head = head < 0 ? (360 + head) : head;
    //                 if ((head != 0) || (head == NaN)) {
    //                     changeMarker(mark, head);
    //                 }
    //                 if (that.mapTrail) {
    //                     that.showTrail(lat, lng);
    //                 } else {
    //                     if (that.polylineID) {
    //                         that.tempArray = [];
    //                         that.polylineID.remove();
    //                     }
    //                 }
    //                 map.setCameraTarget(new LatLng(lat, lng));
    //                 that.latlngCenter = new LatLng(lat, lng);
    //                 mark.setPosition(new LatLng(lat, lng));
    //                 that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
    //             }
    //             else {
    //                 head = Spherical.computeHeading(mark.getPosition(), dest);
    //                 head = head < 0 ? (360 + head) : head;
    //                 if ((head != 0) || (head == NaN)) {
    //                     changeMarker(mark, head);
    //                 }
    //                 if (that.mapTrail) {
    //                     that.showTrail(dest.lat, dest.lng);
    //                 } else {
    //                     if (that.polylineID) {
    //                         that.tempArray = [];
    //                         that.polylineID.remove();
    //                     }
    //                 }
    //                 map.setCameraTarget(dest);
    //                 that.latlngCenter = dest;
    //                 mark.setPosition(dest);
    //                 target++;
    //                 that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
    //             }
    //         }
    //         _moveMarker();
    //     }
    //     _goToPoint();
    // }
    // liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
    //     var target = 0;
    //     clearTimeout(that.ongoingGoToPoint[id]);
    //     clearTimeout(that.ongoingMoveMarker[id]);
    //     if (center) {
    //         map.setCameraTarget(coords[0]);
    //     }
    //     function _goToPoint() {
    //         if (target > coords.length)
    //             return;
    //         var lat = mark.getPosition().lat;
    //         var lng = mark.getPosition().lng;
    //         var step = (speed * 1000 * delay) / 3600000; // in meters
    //         if (coords[target] == undefined)
    //             return;
    //         var dest = new LatLng(coords[target].lat, coords[target].lng);
    //         var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
    //         var numStep = distance / step;
    //         var i = 0;
    //         var deltaLat = (coords[target].lat - lat) / numStep;
    //         var deltaLng = (coords[target].lng - lng) / numStep;
    //         function changeMarker(mark, deg) {
    //             mark.setRotation(deg);
    //             // mark.setIcon(icons);
    //         }
    //         function _moveMarker() {
    //             lat += deltaLat;
    //             lng += deltaLng;
    //             i += step;
    //             var head;
    //             if (i < distance) {
    //                 head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
    //                 head = head < 0 ? (360 + head) : head;
    //                 if ((head != 0) || (head == NaN)) {
    //                     changeMarker(mark, head);
    //                 }
    //                 if (that.mapTrail) {
    //                     that.showTrail(lat, lng);
    //                 } else {
    //                     if (that.polylineID) {
    //                         that.tempArray = [];
    //                         that.polylineID.remove();
    //                     }
    //                 }
    //                 map.setCameraTarget(new LatLng(lat, lng));
    //                 that.latlngCenter = new LatLng(lat, lng);
    //                 mark.setPosition(new LatLng(lat, lng));
    //                 that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
    //             }
    //             else {
    //                 head = Spherical.computeHeading(mark.getPosition(), dest);
    //                 head = head < 0 ? (360 + head) : head;
    //                 if ((head != 0) || (head == NaN)) {
    //                     changeMarker(mark, head);
    //                 }
    //                 if (that.mapTrail) {
    //                     that.showTrail(dest.lat, dest.lng);
    //                 }
    //                 else {
    //                     if (that.polylineID) {
    //                         that.tempArray = [];
    //                         that.polylineID.remove();
    //                     }
    //                 }
    //                 map.setCameraTarget(dest);
    //                 that.latlngCenter = dest;
    //                 mark.setPosition(dest);
    //                 target++;
    //                 that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
    //             }
    //         }
    //         _moveMarker();
    //     }
    //     _goToPoint();
    // }
    LiveSingleDevice.prototype.zoomin = function () {
        var that = this;
        that.allData.map.moveCameraZoomIn();
    };
    LiveSingleDevice.prototype.zoomout = function () {
        var that = this;
        that.allData.map.moveCameraZoomOut();
        // that.allData.map.animateCameraZoomOut();
    };
    LiveSingleDevice.prototype.temp = function (data) {
        var that = this;
        that.showShareBtn = true;
        that.liveDataShare = data;
        that.onClickShow = false;
        if (that.allData.map != undefined) {
            that.allData.map.remove();
        }
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that.allData = {};
        that.socketChnl = [];
        that.socketSwitch = {};
        if (data) {
            if (data.last_location) {
                var mapOptions = {
                    backgroundColor: 'white',
                    controls: {
                        compass: true,
                        zoom: true,
                    },
                    gestures: {
                        rotate: false,
                        tilt: false,
                    },
                    camera: {
                        target: {
                            lat: data.last_location['lat'],
                            lng: data.last_location['long']
                        },
                        zoom: 15
                    }
                };
                var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                // map.animateCamera({
                //     target: { lat: 20.5937, lng: 78.9629 },
                //     zoom: 15,
                //     duration: 3000,
                //     padding: 0  // default = 20px
                // });
                // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                that.allData.map = map;
                that.socketInit(data);
            }
            else {
                that.allData.map = that.newMap();
                that.socketInit(data);
            }
        }
    };
    // temp(data) {
    //     let that = this;
    //     that.showShareBtn = true;
    //     that.liveDataShare = data;
    //     that.drawerHidden = true;
    //     that.onClickShow = false;
    //     if (that.allData.map != undefined) {
    //         that.allData.map.remove();
    //     }
    //     for (var i = 0; i < that.socketChnl.length; i++)
    //         that._io.removeAllListeners(that.socketChnl[i]);
    //     that.allData = {};
    //     that.socketChnl = [];
    //     that.socketSwitch = {};
    //     let styles = [
    //         {
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#ebe3cd"
    //                 }
    //             ]
    //         },
    //         {
    //             "elementType": "labels.text.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#523735"
    //                 }
    //             ]
    //         },
    //         {
    //             "elementType": "labels.text.stroke",
    //             "stylers": [
    //                 {
    //                     "color": "#f5f1e6"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "administrative",
    //             "elementType": "geometry.stroke",
    //             "stylers": [
    //                 {
    //                     "color": "#c9b2a6"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "administrative.land_parcel",
    //             "elementType": "geometry.stroke",
    //             "stylers": [
    //                 {
    //                     "color": "#dcd2be"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "administrative.land_parcel",
    //             "elementType": "labels.text.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#ae9e90"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "landscape.natural",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#dfd2ae"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "poi",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#dfd2ae"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "poi",
    //             "elementType": "labels.text.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#93817c"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "poi.park",
    //             "elementType": "geometry.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#a5b076"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "poi.park",
    //             "elementType": "labels.text.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#447530"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "poi.school",
    //             "elementType": "geometry.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#ffeb3b"
    //                 },
    //                 {
    //                     "weight": 1
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "poi.school",
    //             "elementType": "labels.icon",
    //             "stylers": [
    //                 {
    //                     "color": "#ff6841"
    //                 },
    //                 {
    //                     "weight": 8
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "poi.school",
    //             "elementType": "labels.text.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#ff6841"
    //                 },
    //                 {
    //                     "weight": 1
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "poi.school",
    //             "elementType": "labels.text.stroke",
    //             "stylers": [
    //                 {
    //                     "color": "#fff9fb"
    //                 },
    //                 {
    //                     "weight": 8
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "road",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#f5f1e6"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "road.arterial",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#fdfcf8"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "road.highway",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#f8c967"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "road.highway",
    //             "elementType": "geometry.stroke",
    //             "stylers": [
    //                 {
    //                     "color": "#e9bc62"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "road.highway.controlled_access",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#e98d58"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "road.highway.controlled_access",
    //             "elementType": "geometry.stroke",
    //             "stylers": [
    //                 {
    //                     "color": "#db8555"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "road.local",
    //             "elementType": "labels.text.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#806b63"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "transit.line",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#dfd2ae"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "transit.line",
    //             "elementType": "labels.text.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#8f7d77"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "transit.line",
    //             "elementType": "labels.text.stroke",
    //             "stylers": [
    //                 {
    //                     "color": "#ebe3cd"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "transit.station",
    //             "elementType": "geometry",
    //             "stylers": [
    //                 {
    //                     "color": "#dfd2ae"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "water",
    //             "elementType": "geometry.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#b9d3c2"
    //                 }
    //             ]
    //         },
    //         {
    //             "featureType": "water",
    //             "elementType": "labels.text.fill",
    //             "stylers": [
    //                 {
    //                     "color": "#92998d"
    //                 }
    //             ]
    //         }
    //     ];
    //     if (data) {
    //         if (data.last_location) {
    //             let mapOptions = {
    //                 backgroundColor: 'white',
    //                 controls: {
    //                     compass: true,
    //                     zoom: true,
    //                 },
    //                 gestures: {
    //                     rotate: false,
    //                     tilt: false,
    //                     // scroll: false
    //                 },
    //                 camera: {
    //                     target: { lat: data.last_location['lat'], lng: data.last_location['long'] },
    //                     zoom: 18
    //                 },
    //                 styles: styles
    //                 // zoom: 15
    //             }
    //             let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
    //             // map.animateCamera({
    //             //     target: { lat: 20.5937, lng: 78.9629 },
    //             //     zoom: 15,
    //             //     duration: 3000,
    //             //     padding: 0  // default = 20px
    //             // });
    //             // map.setCameraZoom(18);
    //             // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
    //             that.allData.marker = [];
    //             that.allData.map = map;
    //             that.socketInit(data);
    //         } else {
    //             that.allData.map = that.newMap();
    //             that.socketInit(data);
    //         }
    //     }
    // }
    LiveSingleDevice.prototype.showTrail = function (lat, lng) {
        var that = this;
        that.tempArray.push({ lat: lat, lng: lng });
        if (that.tempArray.length > 1) {
            // Polyline polyline = this.allData.map.addPolyline(new PolylineOptions().....);
            that.allData.map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            })
                .then(function (polyline) {
                that.polylineID = polyline;
            });
        }
    };
    LiveSingleDevice.prototype.info = function (mark, cb) {
        mark.addListener('click', cb);
    };
    LiveSingleDevice.prototype.show = function (data, key) {
        if (data != undefined) {
            if (key == 'last_ping_on') {
                return __WEBPACK_IMPORTED_MODULE_5_moment__(data[key]).format('DD/MM/YYYY, h:mm:ss a');
            }
            else {
                return data[key];
            }
        }
    };
    LiveSingleDevice.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LiveSingleDevice.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                this.mapHideTraffic = !this.mapHideTraffic;
                if (this.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                }
            }
            else {
                if (type == 'mapTrail') {
                    this.mapTrail = !this.mapTrail;
                }
            }
        }
    };
    LiveSingleDevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/live-single-device/live-single-device.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Live For {{titleText}}\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div id="map_canvas_single_device">\n\n    <button ion-button outline item-end id=\'button_wa\' style=\'margin: 0; padding: 0; border: 0; height: 0;\' (click)="callme()">\n\n      &nbsp;\n\n    </button>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-fab top left>\n\n          <button ion-fab color="light" mini (click)="onClickMainMenu()">\n\n            <ion-icon color="gpsc" name="map"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n\n              S\n\n            </button>\n\n            <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n\n              T\n\n            </button>\n\n            <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n\n              N\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n      <ion-col>\n\n        <p class="blink" style="text-align: center;font-size:24px;color:cornflowerblue;font-weight: 600;"\n\n          *ngIf="!vehicle_speed ">0\n\n          <span style="font-size:16px;">Km/h</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: green;font-weight: 600;"\n\n          *ngIf="vehicle_speed <= 60">{{vehicle_speed}}\n\n          <span style="font-size:16px;">Km/h</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: red;font-weight: 600;"\n\n          *ngIf="vehicle_speed > 60">{{vehicle_speed}}\n\n          <span style="font-size:16px;">Km/h</span>\n\n        </p>\n\n      </ion-col>\n\n      <ion-col>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top:-16%">\n\n      <button ion-fab mini *ngIf="showShareBtn" (click)="shareLive($event)" color="gpsc">\n\n        <ion-icon name="share" color="black"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top:-3%">\n\n      <button ion-fab mini (click)="onSelectMapOption(\'mapTrail\')" color="{{mapTrail ? \'light\' : \'gpsc\'}}">\n\n        <ion-icon name="eye" color="{{mapTrail ? \'dark-grey\' : \'black\'}}"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top:10%">\n\n      <button ion-fab mini (click)="onSelectMapOption(\'mapHideTraffic\')" color="{{mapHideTraffic ? \'light\' : \'gpsc\'}}">\n\n        <ion-icon name="walk" color="{{mapHideTraffic ? \'dark-grey\' : \'black\'}}"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top:23%">\n\n      <button ion-fab mini (click)="settings()" color="gpsc">\n\n        <ion-icon name="cog" color="black"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top:36%">\n\n      <button ion-fab mini (click)="zoomin()" color="gpsc">\n\n        <ion-icon name="add" color="black"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top:49%">\n\n      <button ion-fab mini (click)="zoomout()" color="gpsc">\n\n        <ion-icon name="remove" color="black"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top:62%">\n\n      <button ion-fab mini (click)="addPOI()" color="gpsc">\n\n        <ion-icon name="flag" color="black"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n  </div>\n\n</ion-content>\n\n<div *ngIf="showActionSheet" class="divPlan">\n\n  <ion-bottom-drawer [(hidden)]="drawerHidden1" [dockedHeight]="dockedHeight" [bounceThreshold]="bounceThreshold"\n\n    [shouldBounce]="shouldBounce" [distanceTop]="distanceTop">\n\n    <div class="drawer-content">\n\n      <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">Last Updated On &mdash;\n\n        {{last_ping_on | date:\'medium\'}}</p>\n\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n\n      <p padding-left style="font-size: 13px" *ngIf="address">{{address}}</p>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC==\'0\'" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC==\'1\'" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC==null" width="20" height="20">\n\n          <p>IGN</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel==null" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel==\'1\'" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel==\'0\'" width="20" height="20">\n\n          <p>AC</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20">\n\n          <p>FUEL</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power==null" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power==\'0\'" width="20" height="20">\n\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power==\'1\'" width="20" height="20">\n\n          <p>POWER</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking==null" width="30" height="20">\n\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking==\'0\'" width="30" height="20">\n\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking==\'1\'" width="30" height="20">\n\n          <p>GPS</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!total_odo">N/A</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="total_odo">{{total_odo | number : \'1.0-2\'}}</p>\n\n          <p style="font-size: 13px">Odometer</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!vehicle_speed">0 km/h</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="vehicle_speed">{{vehicle_speed}} km/h</p>\n\n          <p style="font-size: 13px">\n\n            Speed\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="fuel">{{fuel}}</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!fuel">N/A</p>\n\n          <p style="font-size: 13px">Fuel</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!distFromLastStop">0 Km</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="distFromLastStop">{{distFromLastStop | number : \'1.0-2\'}}\n\n            Km</p>\n\n          <p style="font-size: 13px">From Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%">\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!todays_odo">0 Km</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="todays_odo">{{todays_odo | number : \'1.0-2\'}} km</p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!timeAtLastStop">N/A</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="timeAtLastStop">{{timeAtLastStop}}</p>\n\n          <p style="font-size: 13px">At Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%">\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_stopped">N/A</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_stopped">{{today_stopped}}</p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr>\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!lastStoppedAt">N/A</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="lastStoppedAt">{{lastStoppedAt}}</p>\n\n          <p style="font-size: 13px">From Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%">\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_running">N/A</p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_running">{{today_running}}</p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>\n\n\n\n<div *ngIf="showFooter">\n\n  <ion-bottom-drawer [(hidden)]="drawerHidden" [dockedHeight]=200 [bounceThreshold]="bounceThreshold"\n\n    [shouldBounce]="shouldBounce" [distanceTop]="distanceTop">\n\n    <div class="drawer-content">\n\n      <p style="font-size:1.2em; color:black;">Share Live Vehicle</p>\n\n      <ion-row>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{condition}}" (click)="sharedevices(\'15mins\')">15\n\n            mins</button>\n\n        </ion-col>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{condition1}}" (click)="sharedevices(\'1hour\')">1\n\n            hour</button>\n\n        </ion-col>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{condition2}}" (click)="sharedevices(\'8hours\')">8\n\n            hours</button>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-4></ion-col>\n\n        <ion-col col-4></ion-col>\n\n        <ion-col col-4 style="text-align: right;">\n\n          <ion-fab style="right: calc(10px + env(safe-area-inset-right));">\n\n            <button ion-fab mini (click)="shareLivetemp()" color="gpsc">\n\n              <ion-icon name="send" color="black"></ion-icon>\n\n            </button>\n\n          </ion-fab>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/live-single-device/live-single-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], LiveSingleDevice);
    return LiveSingleDevice;
}());

var PoiPage = /** @class */ (function () {
    function PoiPage(apicall, toastCtrl, navparam, viewCtrl) {
        this.apicall = apicall;
        this.toastCtrl = toastCtrl;
        this.navparam = navparam;
        this.viewCtrl = viewCtrl;
        console.log(this.navparam.get("param1"));
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.islogin));
        this.params = this.navparam.get("param1");
        console.log("parameters: ", this.params);
        this.lat = this.params.getPosition().lat;
        this.lng = this.params.getPosition().lng;
        // this.lat = this.params.marker.getPosition().lat;
        // this.lng = this.params.marker.getPosition().lng;
    }
    PoiPage.prototype.ngOnInit = function () {
        // const LatLng = new Latlng(lat: this.lat, lng: this.lng)
        // let options: NativeGeocoderOptions = {
        //     useLocale: true,
        //     maxResults: 5
        // };
        var that = this;
        that.address = undefined;
        var geocoder = new google.maps.Geocoder;
        var latlng = new google.maps.LatLng(this.lat, this.lng);
        var request = {
            "latLng": latlng
        };
        geocoder.geocode(request, function (resp, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (resp[0] != null) {
                    that.address = resp[0].formatted_address;
                }
                else {
                    console.log("No address available");
                }
            }
            else {
                that.address = 'N/A';
            }
        });
        // this.nativeGeocoder.reverseGeocode(this.lat, this.lat, options)
        //     .then((result: NativeGeocoderReverseResult[]) => {
        //         console.log(JSON.stringify(result[0]))
        //         that.address = result[0].locality + ' ' + result[0].subLocality + ' ' + result[0].subAdministrativeArea + ' ' + result[0].countryName;;
        //     })
        //     .catch((error: any) => console.log(error));
    };
    PoiPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PoiPage.prototype.save = function () {
        var _this = this;
        if (this.poi_name == undefined || this.address == undefined) {
            var toast = this.toastCtrl.create({
                message: "POI name is required!",
                duration: 1500,
                position: "bottom"
            });
            toast.present();
        }
        else {
            var payload = {
                "poi": [{
                        "location": {
                            "type": "Point",
                            "coordinates": [
                                this.lng,
                                this.lat
                            ]
                        },
                        "poiname": this.poi_name,
                        "status": "Active",
                        "address": this.address,
                        "user": this.islogin._id
                    }]
            };
            this.apicall.startLoading().present();
            this.apicall.addPOIAPI(payload)
                .subscribe(function (data) {
                console.log("response adding poi: ", data);
                _this.apicall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: "POI added successfully!",
                    duration: 2000,
                    position: 'top'
                });
                toast.present();
                _this.viewCtrl.dismiss();
            }, function (err) {
                console.log("error adding poi: ", err);
                _this.apicall.stopLoading();
            });
        }
    };
    PoiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/live-single-device/poi.html"*/'<!-- <ion-content padding> -->\n<div>\n    <ion-row>\n        <ion-col col-12 style="text-align: center; font-size: 2rem;">\n            <b>Add POI</b>&nbsp;&nbsp;&nbsp;\n            <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n        </ion-col>\n        <!-- <ion-col col-2 style="text-align: right">\n                <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n            </ion-col> -->\n    </ion-row><br />\n    <!-- <p style="text-align: center; font-size: 2rem;">\n            <b>Add POI&nbsp;&nbsp;<ion-icon class="close-button" id="close-button" name="close-circle"\n                (tap)="dismiss()"></ion-icon></b>\n        </p><br /> -->\n    <ion-row>\n        <ion-col col-12><b>POI Name:</b></ion-col>\n        <ion-col col-12>\n            <ion-input type="text" [(ngModel)]="poi_name"></ion-input>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-12><b>POI Address:</b></ion-col>\n        <ion-col col-12>\n            <ion-input type="text" [(ngModel)]="address"></ion-input>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-6>\n            <button ion-button block (tap)="save()" color="gpsc">SAVE</button>\n        </ion-col>\n        <ion-col col-6>\n            <button ion-button block (tap)="dismiss()" color="gpsc">CANCEL</button>\n        </ion-col>\n    </ion-row>\n</div>\n<!-- </ion-content> -->'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/live-single-device/poi.html"*/,
            selector: 'page-device-settings'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PoiPage);
    return PoiPage;
}());

//# sourceMappingURL=live-single-device.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(417);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MyErrorHandler */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(491);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_network_network__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_menu_menu__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__node_modules_ion_bottom_drawer__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_app_version__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_pro__ = __webpack_require__(494);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_pro___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_25__ionic_pro__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_storage__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_add_devices_upload_doc_view_doc_view_doc__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_live_single_device_live_single_device__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_transfer_ngx__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_camera_ngx__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_file_path_ngx__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_native_geocoder__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_file_ngx__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__ = __webpack_require__(403);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















// Custom components


















__WEBPACK_IMPORTED_MODULE_25__ionic_pro__["Pro"].init('501b929f', {
    appVersion: '3.2'
});
var MyErrorHandler = /** @class */ (function () {
    function MyErrorHandler(injector) {
        try {
            this.ionicErrorHandler = injector.get(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"]);
        }
        catch (e) {
            // Unable to get the IonicErrorHandler provider, ensure
            // IonicErrorHandler has been added to the providers list below
        }
    }
    MyErrorHandler.prototype.handleError = function (err) {
        __WEBPACK_IMPORTED_MODULE_25__ionic_pro__["Pro"].monitoring.handleNewError(err);
        // Remove this if you want to disable Ionic's auto exception handling
        // in development mode.
        this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
    };
    MyErrorHandler = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injector"]])
    ], MyErrorHandler);
    return MyErrorHandler;
}());

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_19__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */],
                __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_28__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                    scrollPadding: false,
                    scrollAssist: false
                }, {
                    links: [
                        { loadChildren: '../pages/add-devices/immobilize/immobilize.module#ImmobilizePageModule', name: 'ImmobilizePage', segment: 'immobilize', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module#PaytmwalletloginPageModule', name: 'PaytmwalletloginPage', segment: 'paytmwalletlogin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/payment-secure/payment-secure.module#PaymentSecurePageModule', name: 'PaymentSecurePage', segment: 'payment-secure', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/payment-greeting/payment-greeting.module#PaymentGreetingPageModule', name: 'PaymentGreetingPage', segment: 'payment-greeting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/add-doc/add-doc.module#AddDocPageModule', name: 'AddDocPage', segment: 'add-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/wallet/wallet.module#WalletPageModule', name: 'WalletPage', segment: 'wallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/upload-doc.module#UploadDocPageModule', name: 'UploadDocPage', segment: 'upload-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-us/contact-us.module#ContactUsPageModule', name: 'ContactUsPage', segment: 'contact-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/group-modal/group-modal.module#GroupModalPageModule', name: 'GroupModalPage', segment: 'group-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/update-cust/update-cust.module#UpdateCustModalPageModule', name: 'UpdateCustModalPage', segment: 'update-cust', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/add-dealer/add-dealer.module#AddDealerPageModule', name: 'AddDealerPage', segment: 'add-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/edit-dealer/edit-dealer.module#EditDealerPageModule', name: 'EditDealerPage', segment: 'edit-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/device-summary-repo/device-summary-repo.module#DeviceSummaryRepoPageModule', name: 'DeviceSummaryRepoPage', segment: 'device-summary-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/distance-report/distance-report.module#DistanceReportPageModule', name: 'DistanceReportPage', segment: 'distance-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel-report/fuel-report.module#FuelReportPageModule', name: 'FuelReportPage', segment: 'fuel-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence-report/geofence-report.module#GeofenceReportPageModule', name: 'GeofenceReportPage', segment: 'geofence-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence-show/geofence-show.module#GeofenceShowPageModule', name: 'GeofenceShowPage', segment: 'geofence-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence.module#GeofencePageModule', name: 'GeofencePage', segment: 'geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ignition-report/ignition-report.module#IgnitionReportPageModule', name: 'IgnitionReportPage', segment: 'ignition-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/device-settings/device-settings.module#DeviceSettingsPageModule', name: 'DeviceSettingsPage', segment: 'device-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/expired/expired.module#ExpiredPageModule', name: 'ExpiredPage', segment: 'expired', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/over-speed-repo/over-speed-repo.module#OverSpeedRepoPageModule', name: 'OverSpeedRepoPage', segment: 'over-speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-report/poi-report.module#POIReportPageModule', name: 'POIReportPage', segment: 'poi-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/poi-list.module#PoiListPageModule', name: 'PoiListPage', segment: 'poi-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-map-show/route-map-show.module#RouteMapShowPageModule', name: 'RouteMapShowPage', segment: 'route-map-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route/route.module#RoutePageModule', name: 'RoutePage', segment: 'route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-voilations/route-voilations.module#RouteVoilationsPageModule', name: 'RouteVoilationsPage', segment: 'route-voilations', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-geofence/show-geofence.module#ShowGeofencePageModule', name: 'ShowGeofencePage', segment: 'show-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-route/show-route.module#ShowRoutePageModule', name: 'ShowRoutePage', segment: 'show-route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/show-trip/show-trip.module#ShowTripPageModule', name: 'ShowTripPage', segment: 'show-trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sos-report/sos-report.module#SosReportPageModule', name: 'SosReportPage', segment: 'sos-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speed-repo/speed-repo.module#SpeedRepoPageModule', name: 'SpeedRepoPage', segment: 'speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stoppages-repo/stoppages-repo.module#StoppagesRepoPageModule', name: 'StoppagesRepoPage', segment: 'stoppages-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/all-notifications.module#AllNotificationsPageModule', name: 'AllNotificationsPage', segment: 'all-notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/update-device/update-device.module#UpdateDevicePageModule', name: 'UpdateDevicePage', segment: 'update-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-customer-modal/add-customer-modal.module#AddCustomerModalModule', name: 'AddCustomerModal', segment: 'add-customer-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report/daily-report.module#DailyReportPageModule', name: 'DailyReportPage', segment: 'daily-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-device-modal.module#AddDeviceModalPageModule', name: 'AddDeviceModalPage', segment: 'add-device-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/dealers.module#DealerPageModule', name: 'DealerPage', segment: 'dealers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-report.module#TripReportPageModule', name: 'TripReportPage', segment: 'trip-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-review/trip-review.module#TripReviewPageModule', name: 'TripReviewPage', segment: 'trip-review', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/vehicle-details/vehicle-details.module#VehicleDetailsPageModule', name: 'VehicleDetailsPage', segment: 'vehicle-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/add-geofence/add-geofence.module#AddGeofencePageModule', name: 'AddGeofencePage', segment: 'add-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-device/history-device.module#HistoryDevicePageModule', name: 'HistoryDevicePage', segment: 'history-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/add-poi/add-poi.module#AddPoiPageModule', name: 'AddPoiPage', segment: 'add-poi', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/add-devices.module#AddDevicesPageModule', name: 'AddDevicesPage', segment: 'add-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/live.module#LivePageModule', name: 'LivePage', segment: 'live', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/live-single-device.module#LiveSingleDeviceModule', name: 'LiveSingleDevice', segment: 'live-single-device', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_26__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_15__node_modules_ion_bottom_drawer__["a" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_28__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"],
                [{ provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: MyErrorHandler }],
                // { provide: ErrorHandler, useClass: IonicErrorHandler },
                __WEBPACK_IMPORTED_MODULE_10__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_11__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_12__providers_menu_menu__["a" /* MenuProvider */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__["i" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_transfer_ngx__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_transfer_ngx__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_camera_ngx__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_file_path_ngx__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_file_ngx__["a" /* File */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 179,
	"./af.js": 179,
	"./ar": 180,
	"./ar-dz": 181,
	"./ar-dz.js": 181,
	"./ar-kw": 182,
	"./ar-kw.js": 182,
	"./ar-ly": 183,
	"./ar-ly.js": 183,
	"./ar-ma": 184,
	"./ar-ma.js": 184,
	"./ar-sa": 185,
	"./ar-sa.js": 185,
	"./ar-tn": 186,
	"./ar-tn.js": 186,
	"./ar.js": 180,
	"./az": 187,
	"./az.js": 187,
	"./be": 188,
	"./be.js": 188,
	"./bg": 189,
	"./bg.js": 189,
	"./bm": 190,
	"./bm.js": 190,
	"./bn": 191,
	"./bn.js": 191,
	"./bo": 192,
	"./bo.js": 192,
	"./br": 193,
	"./br.js": 193,
	"./bs": 194,
	"./bs.js": 194,
	"./ca": 195,
	"./ca.js": 195,
	"./cs": 196,
	"./cs.js": 196,
	"./cv": 197,
	"./cv.js": 197,
	"./cy": 198,
	"./cy.js": 198,
	"./da": 199,
	"./da.js": 199,
	"./de": 200,
	"./de-at": 201,
	"./de-at.js": 201,
	"./de-ch": 202,
	"./de-ch.js": 202,
	"./de.js": 200,
	"./dv": 203,
	"./dv.js": 203,
	"./el": 204,
	"./el.js": 204,
	"./en-au": 205,
	"./en-au.js": 205,
	"./en-ca": 206,
	"./en-ca.js": 206,
	"./en-gb": 207,
	"./en-gb.js": 207,
	"./en-ie": 208,
	"./en-ie.js": 208,
	"./en-il": 209,
	"./en-il.js": 209,
	"./en-nz": 210,
	"./en-nz.js": 210,
	"./eo": 211,
	"./eo.js": 211,
	"./es": 212,
	"./es-do": 213,
	"./es-do.js": 213,
	"./es-us": 214,
	"./es-us.js": 214,
	"./es.js": 212,
	"./et": 215,
	"./et.js": 215,
	"./eu": 216,
	"./eu.js": 216,
	"./fa": 217,
	"./fa.js": 217,
	"./fi": 218,
	"./fi.js": 218,
	"./fo": 219,
	"./fo.js": 219,
	"./fr": 220,
	"./fr-ca": 221,
	"./fr-ca.js": 221,
	"./fr-ch": 222,
	"./fr-ch.js": 222,
	"./fr.js": 220,
	"./fy": 223,
	"./fy.js": 223,
	"./gd": 224,
	"./gd.js": 224,
	"./gl": 225,
	"./gl.js": 225,
	"./gom-latn": 226,
	"./gom-latn.js": 226,
	"./gu": 227,
	"./gu.js": 227,
	"./he": 228,
	"./he.js": 228,
	"./hi": 229,
	"./hi.js": 229,
	"./hr": 230,
	"./hr.js": 230,
	"./hu": 231,
	"./hu.js": 231,
	"./hy-am": 232,
	"./hy-am.js": 232,
	"./id": 233,
	"./id.js": 233,
	"./is": 234,
	"./is.js": 234,
	"./it": 235,
	"./it.js": 235,
	"./ja": 236,
	"./ja.js": 236,
	"./jv": 237,
	"./jv.js": 237,
	"./ka": 238,
	"./ka.js": 238,
	"./kk": 239,
	"./kk.js": 239,
	"./km": 240,
	"./km.js": 240,
	"./kn": 241,
	"./kn.js": 241,
	"./ko": 242,
	"./ko.js": 242,
	"./ky": 243,
	"./ky.js": 243,
	"./lb": 244,
	"./lb.js": 244,
	"./lo": 245,
	"./lo.js": 245,
	"./lt": 246,
	"./lt.js": 246,
	"./lv": 247,
	"./lv.js": 247,
	"./me": 248,
	"./me.js": 248,
	"./mi": 249,
	"./mi.js": 249,
	"./mk": 250,
	"./mk.js": 250,
	"./ml": 251,
	"./ml.js": 251,
	"./mn": 252,
	"./mn.js": 252,
	"./mr": 253,
	"./mr.js": 253,
	"./ms": 254,
	"./ms-my": 255,
	"./ms-my.js": 255,
	"./ms.js": 254,
	"./mt": 256,
	"./mt.js": 256,
	"./my": 257,
	"./my.js": 257,
	"./nb": 258,
	"./nb.js": 258,
	"./ne": 259,
	"./ne.js": 259,
	"./nl": 260,
	"./nl-be": 261,
	"./nl-be.js": 261,
	"./nl.js": 260,
	"./nn": 262,
	"./nn.js": 262,
	"./pa-in": 263,
	"./pa-in.js": 263,
	"./pl": 264,
	"./pl.js": 264,
	"./pt": 265,
	"./pt-br": 266,
	"./pt-br.js": 266,
	"./pt.js": 265,
	"./ro": 267,
	"./ro.js": 267,
	"./ru": 268,
	"./ru.js": 268,
	"./sd": 269,
	"./sd.js": 269,
	"./se": 270,
	"./se.js": 270,
	"./si": 271,
	"./si.js": 271,
	"./sk": 272,
	"./sk.js": 272,
	"./sl": 273,
	"./sl.js": 273,
	"./sq": 274,
	"./sq.js": 274,
	"./sr": 275,
	"./sr-cyrl": 276,
	"./sr-cyrl.js": 276,
	"./sr.js": 275,
	"./ss": 277,
	"./ss.js": 277,
	"./sv": 278,
	"./sv.js": 278,
	"./sw": 279,
	"./sw.js": 279,
	"./ta": 280,
	"./ta.js": 280,
	"./te": 281,
	"./te.js": 281,
	"./tet": 282,
	"./tet.js": 282,
	"./tg": 283,
	"./tg.js": 283,
	"./th": 284,
	"./th.js": 284,
	"./tl-ph": 285,
	"./tl-ph.js": 285,
	"./tlh": 286,
	"./tlh.js": 286,
	"./tr": 287,
	"./tr.js": 287,
	"./tzl": 288,
	"./tzl.js": 288,
	"./tzm": 289,
	"./tzm-latn": 290,
	"./tzm-latn.js": 290,
	"./tzm.js": 289,
	"./ug-cn": 291,
	"./ug-cn.js": 291,
	"./uk": 292,
	"./uk.js": 292,
	"./ur": 293,
	"./ur.js": 293,
	"./uz": 294,
	"./uz-latn": 295,
	"./uz-latn.js": 295,
	"./uz.js": 294,
	"./vi": 296,
	"./vi.js": 296,
	"./x-pseudo": 297,
	"./x-pseudo.js": 297,
	"./yo": 298,
	"./yo.js": 298,
	"./zh-cn": 299,
	"./zh-cn.js": 299,
	"./zh-hk": 300,
	"./zh-hk.js": 300,
	"./zh-tw": 301,
	"./zh-tw.js": 301
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 447;

/***/ }),

/***/ 467:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 491:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_network_network__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_text_to_speech__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, 
    // private network: Network,
    events, networkProvider, menuProvider, menuCtrl, modalCtrl, push, alertCtrl, app, apiCall, toastCtrl, tts, storage) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.networkProvider = networkProvider;
        this.menuProvider = menuProvider;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.tts = tts;
        this.storage = storage;
        this.islogin = {};
        // Settings for the SideMenuContentComponent
        this.sideMenuSettings = {
            accordionMode: true,
            showSelectedOption: true,
            selectedOptionClass: 'active-side-menu-option'
        };
        this.unreadCountObservable = new __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__["ReplaySubject"](0);
        // this.backgroundMode.enable();
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
            _this.isDealer = udata.isDealer;
            console.log("islogin=> " + JSON.stringify(_this.islogin));
        });
        this.events.subscribe('notif:updated', function (notifData) {
            console.log("text to speech updated=> ", notifData);
            // this.notfiD = notifData;
            localStorage.setItem("notifValue", notifData);
        });
        platform.ready().then(function () {
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin=> " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
        console.log("DealerDetails=> " + this.DealerDetails._id);
        this.dealerStatus = this.islogin.isDealer;
        console.log("dealerStatus=> " + this.dealerStatus);
        this.getSideMenuData();
        this.initializeApp();
        if (localStorage.getItem("loginflag")) {
            // debugger;
            if (localStorage.getItem("SCREEN") != null) {
                if (localStorage.getItem("SCREEN") === 'live') {
                    this.rootPage = 'LivePage';
                }
                else {
                    if (localStorage.getItem("SCREEN") === 'dashboard') {
                        this.rootPage = 'DashboardPage';
                    }
                }
            }
            else {
                this.rootPage = 'DashboardPage';
            }
        }
        else {
            this.rootPage = 'LoginPage';
        }
    }
    MyApp.prototype.getSideMenuData = function () {
        this.pages = this.menuProvider.getSideMenus();
    };
    MyApp.prototype.pushNotify = function () {
        var that = this;
        that.push.hasPermission() // to check if we have permission
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
                that.pushSetup();
            }
            else {
                console.log('We do not have permission to send push notifications');
                that.pushSetup();
            }
        });
    };
    MyApp.prototype.backBtnHandler = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            var nav = _this.app.getActiveNavs()[0];
            var activeView = nav.getActive();
            if (activeView.name == "DashboardPage") {
                if (nav.canGoBack()) { //Can we go back?
                    nav.pop();
                }
                else {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'App termination',
                        message: 'Do you want to close the app?',
                        buttons: [{
                                text: 'Cancel',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Application exit prevented!');
                                }
                            }, {
                                text: 'Close App',
                                handler: function () {
                                    _this.platform.exitApp(); // Close this application
                                }
                            }]
                    });
                    alert_1.present();
                }
            }
        });
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '644983599736',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            // if (this.backgroundMode.isEnabled()) {
            if (localStorage.getItem("notifValue") != null) {
                if (localStorage.getItem("notifValue") == 'true') {
                    _this.tts.speak(notification.message)
                        .then(function () { return console.log('Success'); })
                        .catch(function (reason) { return console.log(reason); });
                }
            }
            // }
            if (notification.additionalData.foreground) {
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            // alert(registration.registrationId)
            console.log("device token => " + registration.registrationId);
            // console.log("reg type=> " + registration.registrationType);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
            // this.storage.set("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
            // alert('Error with Push plugin' + error)
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var that = this;
        that.platform.ready().then(function () {
            that.pushNotify();
            that.networkProvider.initializeNetworkEvents();
            // Offline event
            that.events.subscribe('network:offline', function () {
                // alert('network:offline ==> ' + this.networkProvider.getNetworkType());
                alert("Internet is not connected... please make sure the internet connection is working properly.");
            });
            // Online event
            that.events.subscribe('network:online', function () {
                alert('network:online ==> ' + _this.networkProvider.getNetworkType());
            });
            that.backBtnHandler();
        });
        // Initialize some options
        that.initializeOptions();
        // Change the value for the batch every 5 seconds
        setInterval(function () {
            _this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
        }, 5000);
    };
    MyApp.prototype.initializeOptions = function () {
        var _this = this;
        this.options = new Array();
        // debugger;
        // Load simple menu options analytics
        // ------------------------------------------
        // debugger;
        if ((this.islogin.role == undefined || this.islogin.role == 'supAdm' || this.islogin.role == 'adm') && this.islogin.isSuperAdmin == true && this.islogin.isDealer == false) {
            this.options.push({
                iconName: 'home',
                displayText: 'Home',
                component: 'DashboardPage',
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Groups',
                component: 'GroupsPage'
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Dealers',
                component: 'DealerPage'
            });
            this.options.push({
                iconName: 'contacts',
                displayText: 'Customers',
                component: 'CustomersPage'
            });
            this.options.push({
                iconName: 'notifications',
                displayText: 'Notifications',
                component: 'AllNotificationsPage'
            });
            this.options.push({
                iconName: 'list',
                displayText: 'POI List',
                component: 'PoiListPage'
            });
            // Load options with nested items (with icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Reports',
                iconName: 'clipboard',
                suboptions: [
                    {
                        iconName: 'clipboard',
                        displayText: 'Daily Report',
                        component: 'DailyReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Summary Report',
                        component: 'DeviceSummaryRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Geofenceing Report',
                        component: 'GeofenceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Overspeed Report',
                        component: 'OverSpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Ignition Report',
                        component: 'IgnitionReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Stoppage Report',
                        component: 'StoppagesRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Fuel Report',
                        component: 'FuelReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Distance Report',
                        component: 'DistanceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Trip Report',
                        component: 'TripReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Route Violation Report',
                        component: 'RouteVoilationsPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Speed Variation Report',
                        component: 'SpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'SOS Report',
                        component: 'SosReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'POI Report',
                        component: 'POIReportPage'
                    }
                ]
            });
            // Load options with nested items (without icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Routes',
                iconName: 'map',
                component: 'RoutePage'
            });
            // Load special options
            // -----------------------------------------------
            this.options.push({
                displayText: 'Customer Support',
                suboptions: [
                    {
                        iconName: 'star',
                        displayText: 'Rate Us',
                        component: 'FeedbackPage'
                    },
                    {
                        iconName: 'mail',
                        displayText: 'Contact Us',
                        component: 'ContactUsPage'
                    }
                ]
            });
            this.options.push({
                displayText: 'Profile',
                iconName: 'person',
                component: 'ProfilePage'
            });
        }
        else {
            if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == false) {
                this.options.push({
                    iconName: 'home',
                    displayText: 'Home',
                    component: 'DashboardPage',
                });
                this.options.push({
                    iconName: 'people',
                    displayText: 'Groups',
                    component: 'GroupsPage'
                });
                this.options.push({
                    iconName: 'notifications',
                    displayText: 'Notifications',
                    component: 'AllNotificationsPage'
                });
                this.options.push({
                    iconName: 'list',
                    displayText: 'POI List',
                    component: 'PoiListPage'
                });
                // Load options with nested items (with icons)
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Reports',
                    iconName: 'clipboard',
                    suboptions: [
                        {
                            iconName: 'clipboard',
                            displayText: 'Daily Report',
                            component: 'DailyReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Summary Report',
                            component: 'DeviceSummaryRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Geofenceing Report',
                            component: 'GeofenceReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Overspeed Report',
                            component: 'OverSpeedRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Ignition Report',
                            component: 'IgnitionReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Stoppage Report',
                            component: 'StoppagesRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Fuel Report',
                            component: 'FuelReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Distance Report',
                            component: 'DistanceReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Trip Report',
                            component: 'TripReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Route Violation Report',
                            component: 'RouteVoilationsPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Speed Variation Report',
                            component: 'SpeedRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'SOS Report',
                            component: 'SosReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'POI Report',
                            component: 'POIReportPage'
                        }
                    ]
                });
                // Load options with nested items (without icons)
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Routes',
                    iconName: 'map',
                    component: 'RoutePage'
                });
                // Load special options
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Customer Support',
                    suboptions: [
                        {
                            iconName: 'star',
                            displayText: 'Rate Us',
                            component: 'FeedbackPage'
                        },
                        {
                            iconName: 'mail',
                            displayText: 'Contact Us',
                            component: 'ContactUsPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: 'Profile',
                    iconName: 'person',
                    component: 'ProfilePage'
                });
            }
            else {
                if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == true) {
                    this.options.push({
                        iconName: 'home',
                        displayText: 'Home',
                        component: 'DashboardPage',
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: 'Groups',
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'contacts',
                        displayText: 'Customers',
                        component: 'CustomersPage'
                    });
                    this.options.push({
                        iconName: 'notifications',
                        displayText: 'Notifications',
                        component: 'AllNotificationsPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: 'POI List',
                        component: 'PoiListPage'
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Reports',
                        iconName: 'clipboard',
                        suboptions: [
                            {
                                iconName: 'clipboard',
                                displayText: 'Daily Report',
                                component: 'DailyReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Summary Report',
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Geofenceing Report',
                                component: 'GeofenceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Overspeed Report',
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Ignition Report',
                                component: 'IgnitionReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Stoppage Report',
                                component: 'StoppagesRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Fuel Report',
                                component: 'FuelReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Distance Report',
                                component: 'DistanceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Trip Report',
                                component: 'TripReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Route Violation Report',
                                component: 'RouteVoilationsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Speed Variation Report',
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'SOS Report',
                                component: 'SosReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'POI Report',
                                component: 'POIReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Routes',
                        iconName: 'map',
                        component: 'RoutePage'
                    });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Customer Support',
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: 'Rate Us',
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: 'Contact Us',
                                component: 'ContactUsPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: 'Profile',
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
                else {
                    this.options.push({
                        iconName: 'home',
                        displayText: 'Home',
                        component: 'DashboardPage',
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: 'Groups',
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'notifications',
                        displayText: 'Notifications',
                        component: 'AllNotificationsPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: 'POI List',
                        component: 'PoiListPage'
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Reports',
                        // iconName: 'clipboard',
                        suboptions: [
                            {
                                iconName: 'clipboard',
                                displayText: 'Daily Report',
                                component: 'DailyReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Summary Report',
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Geofenceing Report',
                                component: 'GeofenceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Overspeed Report',
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Ignition Report',
                                component: 'IgnitionReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Stoppage Report',
                                component: 'StoppagesRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Fuel Report',
                                component: 'FuelReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Distnace Report',
                                component: 'DistanceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Trip Report',
                                component: 'TripReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Route Violation Report',
                                component: 'RouteVoilationsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Speed Variation Report',
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'SOS Report',
                                component: 'SosReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'POI Report',
                                component: 'POIReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Routes',
                        iconName: 'map',
                        component: 'RoutePage'
                    });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Customer Support',
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: 'Rate Us',
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: 'Contact Us',
                                component: 'ContactUsPage'
                            },
                        ]
                    });
                    this.options.push({
                        displayText: 'Profile',
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
            }
        }
        console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"));
        // var checkCon = localStorage.getItem("isDealervalue");
        // var supAdminValue = localStorage.getItem("isSuperAdminValue");
        // if (checkCon != null) {
        //   if (checkCon == 'true') {
        //     console.log("console=> ", localStorage.getItem("isDealervalue"))
        //     this.options[2].displayText = 'Dealers';
        //     this.options[2].iconName = 'person';
        //     this.options[2].component = 'DashboardPage';
        //   } else if (checkCon == 'false') {
        //     this.options[2].displayText = 'Customers';
        //     this.options[2].iconName = 'contacts';
        //     this.options[2].component = 'CustomersPage';
        //   }
        // }
        var _DealerStat = localStorage.getItem("dealer_status");
        var _CustStat = localStorage.getItem("custumer_status");
        var onlyDeal = localStorage.getItem("OnlyDealer");
        if (_DealerStat != null || _CustStat != null) {
            if (_DealerStat == 'ON' && _CustStat == 'OFF') {
                this.options[2].displayText = 'Admin';
                this.options[2].iconName = 'person';
                this.options[2].component = 'DashboardPage';
                this.options[3].displayText = 'Customers';
                this.options[3].iconName = 'contacts';
                this.options[3].component = 'CustomersPage';
            }
            else {
                if (_DealerStat == 'OFF' && _CustStat == 'ON') {
                    this.options[2].displayText = 'Dealers';
                    this.options[2].iconName = 'person';
                    this.options[2].component = 'DashboardPage';
                }
                else {
                    if (_DealerStat == 'OFF' && _CustStat == 'OFF' && onlyDeal == null) {
                        this.options[2].displayText = 'Dealers';
                        this.options[2].iconName = 'person';
                        this.options[2].component = 'DealerPage';
                        this.options[3].displayText = 'Customers';
                        this.options[3].iconName = 'contacts';
                        this.options[3].component = 'CustomersPage';
                    }
                    else {
                        if (onlyDeal == 'true') {
                            this.options[2].displayText = 'Customers';
                            this.options[2].iconName = 'contacts';
                            this.options[2].component = 'CustomersPage';
                        }
                    }
                }
            }
        }
        this.events.subscribe("sidemenu:event", function (data) {
            console.log("sidemenu:event=> ", data);
            if (data) {
                _this.options[2].displayText = 'Dealers';
                _this.options[2].iconName = 'person';
                _this.options[2].component = 'DashboardPage';
            }
        });
    };
    // private initializeOptions(): void {
    //   this.options = new Array<SideMenuOption>();
    //   // debugger;
    //   // Load simple menu options analytics
    //   // ------------------------------------------
    //   // debugger;
    //   if ((this.islogin.role == undefined || this.islogin.role == 'supAdm' || this.islogin.role == 'adm') && this.islogin.isSuperAdmin == true && this.islogin.isDealer == false) {
    //     this.options.push({
    //       iconName: 'home',
    //       displayText: 'Home',
    //       component: 'DashboardPage',
    //     });
    //     this.options.push({
    //       iconName: 'people',
    //       displayText: 'Groups',
    //       component: 'GroupsPage'
    //     });
    //     this.options.push({
    //       iconName: 'people',
    //       displayText: 'Dealers',
    //       component: 'DealerPage'
    //     });
    //     this.options.push({
    //       iconName: 'contacts',
    //       displayText: 'Customers',
    //       component: 'CustomersPage'
    //     });
    //     this.options.push({
    //       iconName: 'notifications',
    //       displayText: 'Notifications',
    //       component: 'AllNotificationsPage'
    //     });
    //     // Load options with nested items (with icons)
    //     // -----------------------------------------------
    //     this.options.push({
    //       displayText: 'Reports',
    //       iconName: 'clipboard',
    //       suboptions: [
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Daily Report',
    //           component: 'DailyReportPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Summary Report',
    //           component: 'DeviceSummaryRepoPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Geofenceing Report',
    //           component: 'GeofenceReportPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Overspeed Report',
    //           component: 'OverSpeedRepoPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Ignition Report',
    //           component: 'IgnitionReportPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Stoppage Report',
    //           component: 'StoppagesRepoPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Fuel Report',
    //           component: 'FuelReportPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Distance Report',
    //           component: 'DistanceReportPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Trip Report',
    //           component: 'TripReportPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Route Violation Report',
    //           component: 'RouteVoilationsPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'Speed Variation Report',
    //           component: 'SpeedRepoPage'
    //         },
    //         {
    //           // iconName: 'clipboard',
    //           displayText: 'SOS Report',
    //           component: 'SosReportPage'
    //         }
    //       ]
    //     });
    //     // Load options with nested items (without icons)
    //     // -----------------------------------------------
    //     this.options.push({
    //       displayText: 'Routes',
    //       iconName: 'map',
    //       component: 'RoutePage'
    //     });
    //     // Load special options
    //     // -----------------------------------------------
    //     this.options.push({
    //       displayText: 'Customer Support',
    //       suboptions: [
    //         {
    //           iconName: 'star',
    //           displayText: 'Rate Us',
    //           component: 'FeedbackPage'
    //         },
    //         {
    //           iconName: 'mail',
    //           displayText: 'Contact Us',
    //           component: 'ContactUsPage'
    //         }
    //       ]
    //     });
    //     this.options.push({
    //       displayText: 'Profile',
    //       iconName: 'person',
    //       component: 'ProfilePage'
    //     });
    //   } else {
    //     if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == false) {
    //       this.options.push({
    //         iconName: 'home',
    //         displayText: 'Home',
    //         component: 'DashboardPage',
    //       });
    //       this.options.push({
    //         iconName: 'people',
    //         displayText: 'Groups',
    //         component: 'GroupsPage'
    //       });
    //       this.options.push({
    //         iconName: 'notifications',
    //         displayText: 'Notifications',
    //         component: 'AllNotificationsPage'
    //       });
    //       // Load options with nested items (with icons)
    //       // -----------------------------------------------
    //       this.options.push({
    //         displayText: 'Reports',
    //         iconName: 'clipboard',
    //         suboptions: [
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Daily Report',
    //             component: 'DailyReportPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Summary Report',
    //             component: 'DeviceSummaryRepoPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Geofenceing Report',
    //             component: 'GeofenceReportPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Overspeed Report',
    //             component: 'OverSpeedRepoPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Ignition Report',
    //             component: 'IgnitionReportPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Stoppage Report',
    //             component: 'StoppagesRepoPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Fuel Report',
    //             component: 'FuelReportPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Distance Report',
    //             component: 'DistanceReportPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Trip Report',
    //             component: 'TripReportPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Route Violation Report',
    //             component: 'RouteVoilationsPage'
    //           },
    //           {
    //             // iconName: 'clipboard',
    //             displayText: 'Speed Variation Report',
    //             component: 'SpeedRepoPage'
    //           },
    //           {
    //             iconName: 'clipboard',
    //             displayText: 'SOS Report',
    //             component: 'SosReportPage'
    //           }
    //         ]
    //       });
    //       // Load options with nested items (without icons)
    //       // -----------------------------------------------
    //       this.options.push({
    //         displayText: 'Routes',
    //         iconName: 'map',
    //         component: 'RoutePage'
    //       });
    //       // Load special options
    //       // -----------------------------------------------
    //       this.options.push({
    //         displayText: 'Customer Support',
    //         suboptions: [
    //           {
    //             iconName: 'star',
    //             displayText: 'Rate Us',
    //             component: 'FeedbackPage'
    //           },
    //           {
    //             iconName: 'mail',
    //             displayText: 'Contact Us',
    //             component: 'ContactUsPage'
    //           }
    //         ]
    //       });
    //       this.options.push({
    //         displayText: 'Profile',
    //         iconName: 'person',
    //         component: 'ProfilePage'
    //       });
    //     } else {
    //       if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == true) {
    //         this.options.push({
    //           iconName: 'home',
    //           displayText: 'Home',
    //           component: 'DashboardPage',
    //         });
    //         this.options.push({
    //           iconName: 'people',
    //           displayText: 'Groups',
    //           component: 'GroupsPage'
    //         });
    //         this.options.push({
    //           iconName: 'contacts',
    //           displayText: 'Customers',
    //           component: 'CustomersPage'
    //         });
    //         this.options.push({
    //           iconName: 'notifications',
    //           displayText: 'Notifications',
    //           component: 'AllNotificationsPage'
    //         });
    //         // Load options with nested items (with icons)
    //         // -----------------------------------------------
    //         this.options.push({
    //           displayText: 'Reports',
    //           iconName: 'clipboard',
    //           suboptions: [
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Daily Report',
    //               component: 'DailyReportPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Summary Report',
    //               component: 'DeviceSummaryRepoPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Geofenceing Report',
    //               component: 'GeofenceReportPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Overspeed Report',
    //               component: 'OverSpeedRepoPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Ignition Report',
    //               component: 'IgnitionReportPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Stoppage Report',
    //               component: 'StoppagesRepoPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Fuel Report',
    //               component: 'FuelReportPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Distance Report',
    //               component: 'DistanceReportPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Trip Report',
    //               component: 'TripReportPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Route Violation Report',
    //               component: 'RouteVoilationsPage'
    //             },
    //             {
    //               // iconName: 'clipboard',
    //               displayText: 'Speed Variation Report',
    //               component: 'SpeedRepoPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'SOS Report',
    //               component: 'SosReportPage'
    //             }
    //           ]
    //         });
    //         // Load options with nested items (without icons)
    //         // -----------------------------------------------
    //         this.options.push({
    //           displayText: 'Routes',
    //           iconName: 'map',
    //           component: 'RoutePage'
    //         });
    //         // Load special options
    //         // -----------------------------------------------
    //         this.options.push({
    //           displayText: 'Customer Support',
    //           suboptions: [
    //             {
    //               iconName: 'star',
    //               displayText: 'Rate Us',
    //               component: 'FeedbackPage'
    //             },
    //             {
    //               iconName: 'mail',
    //               displayText: 'Contact Us',
    //               component: 'ContactUsPage'
    //             }
    //           ]
    //         });
    //         this.options.push({
    //           displayText: 'Profile',
    //           iconName: 'person',
    //           component: 'ProfilePage'
    //         });
    //       } else {
    //         this.options.push({
    //           iconName: 'home',
    //           displayText: 'Home',
    //           component: 'DashboardPage',
    //         });
    //         this.options.push({
    //           iconName: 'people',
    //           displayText: 'Groups',
    //           component: 'GroupsPage'
    //         });
    //         this.options.push({
    //           iconName: 'notifications',
    //           displayText: 'Notifications',
    //           component: 'AllNotificationsPage'
    //         });
    //         // Load options with nested items (with icons)
    //         // -----------------------------------------------
    //         this.options.push({
    //           displayText: 'Reports',
    //           // iconName: 'clipboard',
    //           suboptions: [
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Daily Report',
    //               component: 'DailyReportPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Summary Report',
    //               component: 'DeviceSummaryRepoPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Geofenceing Report',
    //               component: 'GeofenceReportPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Overspeed Report',
    //               component: 'OverSpeedRepoPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Ignition Report',
    //               component: 'IgnitionReportPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Stoppage Report',
    //               component: 'StoppagesRepoPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Fuel Report',
    //               component: 'FuelReportPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Distnace Report',
    //               component: 'DistanceReportPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Trip Report',
    //               component: 'TripReportPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Route Violation Report',
    //               component: 'RouteVoilationsPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'Speed Variation Report',
    //               component: 'SpeedRepoPage'
    //             },
    //             {
    //               iconName: 'clipboard',
    //               displayText: 'SOS Report',
    //               component: 'SosReportPage'
    //             }
    //           ]
    //         });
    //         // Load options with nested items (without icons)
    //         // -----------------------------------------------
    //         this.options.push({
    //           displayText: 'Routes',
    //           iconName: 'map',
    //           component: 'RoutePage'
    //         });
    //         // Load special options
    //         // -----------------------------------------------
    //         this.options.push({
    //           displayText: 'Customer Support',
    //           suboptions: [
    //             {
    //               iconName: 'star',
    //               displayText: 'Rate Us',
    //               component: 'FeedbackPage'
    //             },
    //             {
    //               iconName: 'mail',
    //               displayText: 'Contact Us',
    //               component: 'ContactUsPage'
    //             },
    //             // {
    //             //   iconName: 'alert',
    //             //   displayText: 'About Us',
    //             //   component: 'AboutUsPage'
    //             // }
    //           ]
    //         });
    //         this.options.push({
    //           displayText: 'Profile',
    //           iconName: 'person',
    //           component: 'ProfilePage'
    //         });
    //       }
    //     }
    //   }
    //   console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"))
    //   // var checkCon = localStorage.getItem("isDealervalue");
    //   // var supAdminValue = localStorage.getItem("isSuperAdminValue");
    //   // if (checkCon != null) {
    //   //   if (checkCon == 'true') {
    //   //     console.log("console=> ", localStorage.getItem("isDealervalue"))
    //   //     this.options[2].displayText = 'Dealers';
    //   //     this.options[2].iconName = 'person';
    //   //     this.options[2].component = 'DashboardPage';
    //   //   } else if (checkCon == 'false') {
    //   //     this.options[2].displayText = 'Customers';
    //   //     this.options[2].iconName = 'contacts';
    //   //     this.options[2].component = 'CustomersPage';
    //   //   }
    //   // }
    //   var _DealerStat = localStorage.getItem("dealer_status");
    //   var _CustStat = localStorage.getItem("custumer_status");
    //   var onlyDeal = localStorage.getItem("OnlyDealer");
    //   if (_DealerStat != null || _CustStat != null) {
    //     if (_DealerStat == 'ON' && _CustStat == 'OFF') {
    //       this.options[2].displayText = 'Admin';
    //       this.options[2].iconName = 'person';
    //       this.options[2].component = 'DashboardPage';
    //       this.options[3].displayText = 'Customers';
    //       this.options[3].iconName = 'contacts';
    //       this.options[3].component = 'CustomersPage';
    //     } else {
    //       if (_DealerStat == 'OFF' && _CustStat == 'ON') {
    //         this.options[2].displayText = 'Dealers';
    //         this.options[2].iconName = 'person';
    //         this.options[2].component = 'DashboardPage';
    //       } else {
    //         if (_DealerStat == 'OFF' && _CustStat == 'OFF' && onlyDeal == null) {
    //           this.options[2].displayText = 'Dealers';
    //           this.options[2].iconName = 'person';
    //           this.options[2].component = 'DealerPage';
    //           this.options[3].displayText = 'Customers';
    //           this.options[3].iconName = 'contacts';
    //           this.options[3].component = 'CustomersPage';
    //         } else {
    //           if (onlyDeal == 'true') {
    //             this.options[2].displayText = 'Customers';
    //             this.options[2].iconName = 'contacts';
    //             this.options[2].component = 'CustomersPage';
    //           }
    //         }
    //       }
    //     }
    //   }
    //   // if (checkCon != null || supAdminValue != null) {
    //   //   if (checkCon == 'true' && supAdminValue == 'false') {
    //   //     // console.log("console=> ", localStorage.getItem("isDealervalue"))
    //   //     this.options[2].displayText = 'Admin';
    //   //     this.options[2].iconName = 'person';
    //   //     this.options[2].component = 'DashboardPage';
    //   //     this.options[3].displayText = 'Customers';
    //   //     this.options[3].iconName = 'contacts';
    //   //     this.options[3].component = 'CustomersPage';
    //   //   } else if (checkCon == 'false') {
    //   //     this.options[2].displayText = 'Customers';
    //   //     this.options[2].iconName = 'contacts';
    //   //     this.options[2].component = 'CustomersPage';
    //   //   }
    //   // }
    //   this.events.subscribe("sidemenu:event", data => {
    //     console.log("sidemenu:event=> ", data);
    //     if (data) {
    //       this.options[2].displayText = 'Dealers';
    //       this.options[2].iconName = 'person';
    //       this.options[2].component = 'DashboardPage';
    //     }
    //   });
    // }
    MyApp.prototype.onOptionSelected = function (option) {
        var _this = this;
        this.menuCtrl.close().then(function () {
            if (option.custom && option.custom.isLogin) {
                _this.presentAlert('You\'ve clicked the login option!');
            }
            else if (option.custom && option.custom.isLogout) {
                _this.presentAlert('You\'ve clicked the logout option!');
            }
            else if (option.custom && option.custom.isExternalLink) {
                var url = option.custom.externalUrl;
                window.open(url, '_blank');
            }
            else {
                // Get the params if any
                var params = option.custom && option.custom.param;
                console.log("side menu click event=> " + option.component);
                // Redirect to the selected page
                // if (localStorage.getItem("isDealervalue") && option.component == 'DashboardPage') {
                //   localStorage.setItem('details', localStorage.getItem('dealer'));
                //   localStorage.setItem('isDealervalue', 'false');
                // }
                // debugger;
                if (option.displayText == 'Admin' && option.component == 'DashboardPage') {
                    localStorage.setItem("dealer_status", 'OFF');
                    localStorage.setItem('details', localStorage.getItem("superAdminData"));
                    localStorage.removeItem('superAdminData');
                }
                if (option.displayText == 'Dealers' && option.component == 'DashboardPage') {
                    if (localStorage.getItem('custumer_status') == 'ON') {
                        var _dealdata = JSON.parse(localStorage.getItem("dealer"));
                        if (localStorage.getItem("superAdminData") != null || _this.islogin.isSuperAdmin == true) {
                            localStorage.setItem("dealer_status", 'ON');
                        }
                        else {
                            if (_dealdata.isSuperAdmin == true) {
                                localStorage.setItem("dealer_status", 'OFF');
                            }
                            else {
                                localStorage.setItem("OnlyDealer", "true");
                            }
                        }
                        localStorage.setItem("custumer_status", 'OFF');
                        localStorage.setItem('details', localStorage.getItem("dealer"));
                    }
                    else {
                        console.log("something wrong!!");
                    }
                }
                _this.nav.setRoot(option.component, params);
            }
        });
    };
    MyApp.prototype.collapseMenuOptions = function () {
        this.sideMenu.collapseAllOptions();
    };
    MyApp.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Information',
            message: message,
            buttons: ['Ok']
        });
        alert.present();
    };
    MyApp.prototype.openPage = function (page, index) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component) {
            this.nav.setRoot(page.component);
            this.menuCtrl.close();
        }
        else {
            if (this.selectedMenu) {
                this.selectedMenu = 0;
            }
            else {
                this.selectedMenu = index;
            }
        }
    };
    MyApp.prototype.chkCondition = function () {
        var _this = this;
        // debugger;
        this.events.subscribe("event_sidemenu", function (data) {
            // var sidemenuOption = JSON.parse(data);
            // console.log(data);
            // console.log(sidemenuOption);
            _this.islogin = JSON.parse(data);
            // console.log("islogin event publish=> " + this.islogin);
            _this.options[2].displayText = 'Dealers';
            _this.options[2].iconName = 'person';
            _this.options[2].component = 'DashboardPage';
            // this.events.publish("sidemenu:event", this.options)
            _this.initializeOptions();
            // console.log("options=> " + JSON.stringify(this.options[2]))
        });
        // this.events.subscribe('user:updated', (udata) => {
        //   this.islogin = udata;
        //   console.log("islogin=> " + JSON.stringify(this.islogin));
        //   console.log("isDealer=> ", udata.isDealer)
        //   if (udata.isDealer === false) {
        //     this.options[2].displayText = 'Dealers';
        //     this.options[2].iconName = 'person';
        //     this.options[2].component = 'DashboardPage';
        //   }
        //   this.initializeOptions();
        //   console.log(JSON.stringify(this.options))
        // });
        this.initializeOptions();
        // this.dealerChk = localStorage.getItem('condition_chk');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */])
    ], MyApp.prototype, "sideMenu", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/app/app.html"*/'<ion-menu [content]="content" [swipeEnabled]="false" (ionOpen)=\'chkCondition()\'>\n  <ion-header>\n    <!-- <ion-navbar style="background-image: linear-gradient(to right top, #d80622, #c0002d, #a60033, #8b0935, #6f1133);"> -->\n    <!-- <div class="headProf">\n      <img src="assets/imgs/dummy-user-img.png">\n      <div>\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n        <p style="font-size: 12px">\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}</p>\n        <p style="font-size: 12px">\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}</p>\n      </div>\n    </div> -->\n    <!-- </ion-navbar> -->\n  </ion-header>\n\n  <ion-content id=outerNew>\n    <div class="headProf">\n      <img src="assets/imgs/dummy-user-img.png">\n      <div>\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n        <p style="font-size: 12px">\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}</p>\n        <p style="font-size: 12px">\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}</p>\n      </div>\n    </div>\n    <!-- side menu content -->\n    <side-menu-content [settings]="sideMenuSettings" [options]="options" (change)="onOptionSelected($event)"></side-menu-content>\n    <!-- <ion-list no-lines>\n      <ion-item *ngFor="let p of pages;  let i=index" (click)="openPage(p, i);" style="background-color: transparent; border-bottom: 1px solid #717775; color: #f0f0f0">\n\n        <span ion-text>\n          <ion-icon name="{{p.icon}}"> </ion-icon>\n          &nbsp;&nbsp;&nbsp;{{p.title}}\n          <ion-icon [name]="selectedMenu == i? \'arrow-dropdown-circle\' : \'arrow-dropright-circle\'" *ngIf="p.subPages" float-right></ion-icon>\n        </span>\n\n        <ion-list no-lines [hidden]="selectedMenu != i" style="margin: 0px 0 0px; padding:0px !important;">\n          <ion-item no-border *ngFor="let subPage of p.subPages;let i2=index" text-wrap (click)="openPage(subPage)" style="background-color: transparent;border-bottom: 1px solid #b9c2bf; color: #f0f0f0; padding-left: 40px;font-size: 0.8em;">\n            <span ion-text>&nbsp;{{subPage.title}}</span>\n          </ion-item>\n        </ion-list>\n\n      </ion-item>\n    </ion-list> -->\n  </ion-content>\n  <!-- <ion-footer>\n    <ion-toolbar color="light" (tap)="logout()" class="toolbarmd">\n      <ion-title>\n        <ion-icon color="danger" name="log-out"></ion-icon>&nbsp;&nbsp;&nbsp;\n        <span ion-text color="danger">Logout</span>\n      </ion-title>\n    </ion-toolbar>\n  </ion-footer> -->\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_4__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__["a" /* MenuProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_9__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 492:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuOptionSelect; });
// SideMenuOptionSelect constant
var SideMenuOptionSelect = 'sideMenu:optionSelect';
//# sourceMappingURL=side-menu-option-select-event.js.map

/***/ }),

/***/ 493:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateGroup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateGroup = /** @class */ (function () {
    function UpdateGroup(navCtrl, navParams, apigroupupdatecall, alertCtrl, modalCtrl, formBuilder, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupupdatecall = apigroupupdatecall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        this.GroupStatus = [
            {
                name: "Active"
            },
            {
                name: "InActive"
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            grouptype: ['']
        });
    }
    UpdateGroup.prototype.ngOnInit = function () {
    };
    UpdateGroup.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateGroup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-model',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/groups/update-group/update-group.html"*/'<ion-header>\n\n        <ion-navbar>\n\n            <ion-title>Update Group</ion-title>\n\n            <ion-buttons end>\n\n                <button ion-button icon-only (click)="dismiss()">\n\n                    <ion-icon name="close-circle"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n        </ion-navbar>\n\n    </ion-header>\n\n    <ion-content>\n\n     \n\n        <form [formGroup]="groupForm">\n\n\n\n            <ion-item>\n\n                    <ion-label fixed fixed style="min-width: 50% !important;">Group Name</ion-label>\n\n                    <ion-input formControlName="group_name" type="text"></ion-input>\n\n           </ion-item>\n\n           \n\n\n\n           <ion-item>\n\n                <ion-label >Group Status*</ion-label>\n\n                <ion-select formControlName="status" style="min-width:50%;">\n\n                    <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.name" (ionSelect)="GroupStatusdata(statusname)">{{statusname.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n    \n\n      \n\n    </form>\n\n    <!-- <button ion-button block (click)="AddGroup()">ADD</button> -->\n\n        \n\n    </ion-content>\n\n    <ion-footer class="footSty">\n\n    \n\n            <ion-toolbar>\n\n                <ion-row no-padding>\n\n                    <ion-col width-50 style="text-align: center;">\n\n                        <button ion-button clear color="light" (click)="UpdateGroup()">UPDATE</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-toolbar>\n\n        </ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/groups/update-group/update-group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], UpdateGroup);
    return UpdateGroup;
}());

//# sourceMappingURL=update-group.js.map

/***/ })

},[412]);
//# sourceMappingURL=main.js.map