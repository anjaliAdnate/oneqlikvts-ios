webpackJsonp([26],{

/***/ 1045:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { DatePicker } from '@ionic-native/date-picker';


var HistoryDevicePage = /** @class */ (function () {
    function HistoryDevicePage(events, navCtrl, navParams, alertCtrl, toastCtrl, apiCall) {
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.drawerHidden = true;
        this.shouldBounce = true;
        this.dockedHeight = 150;
        this.bounceThreshold = 500;
        this.distanceTop = 56;
        this.showActionSheet = false;
        this.locations = [];
        this.SelectVehicle = 'Select Vehicle';
        this.allData = {};
        this.showZoom = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    HistoryDevicePage.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                // todo something
                // this.navController.pop();
                console.log("back button poped");
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
        // else {
        //   this.navCtrl.pop();
        // }
        localStorage.removeItem("markerTarget");
        localStorage.removeItem("speedMarker");
        localStorage.removeItem("updatetimedate");
        if (localStorage.getItem("MainHistory") != null) {
            console.log("coming soon");
            this.showDropDown = true;
            this.getdevices();
        }
        else {
            this.device = this.navParams.get('device');
            console.log("devices=> ", this.device);
            this.trackerId = this.device.Device_ID;
            this.trackerType = this.device.iconType;
            this.DeviceId = this.device._id;
            this.trackerName = this.device.Device_Name;
            this.btnClicked(this.datetimeStart, this.datetimeEnd);
        }
        this.hideplayback = false;
        this.target = 0;
    };
    HistoryDevicePage.prototype.ngOnDestroy = function () {
        localStorage.removeItem("markerTarget");
        localStorage.removeItem("speedMarker");
        localStorage.removeItem("updatetimedate");
        localStorage.removeItem("MainHistory");
    };
    HistoryDevicePage.prototype.changeformat = function (date) {
        console.log("date=> " + new Date(date).toISOString());
    };
    HistoryDevicePage.prototype.getdevices = function () {
        var _this = this;
        console.log("getdevices");
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        // var baseURLp = 'http://13.126.36.205:3000/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        this.apiCall.startLoading().present();
        // this.apiCall.getdevicesApi(this.islogin._id, this.islogin.email)
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
            _this.devices1243 = [];
            _this.devices = data;
            _this.devices1243.push(data);
            localStorage.setItem('devices', _this.devices);
            _this.isdevice = localStorage.getItem('devices');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
    };
    HistoryDevicePage.prototype.onChangedSelect = function (item) {
        debugger;
        var that = this;
        that.trackerId = item.Device_ID;
        that.trackerType = item.iconType;
        that.DeviceId = item._id;
        that.trackerName = item.Device_Name;
        if (that.allData.map) {
            that.allData.map.clear();
            that.allData.map.remove();
        }
    };
    HistoryDevicePage.prototype.Playback = function () {
        var that = this;
        that.showZoom = true;
        if (localStorage.getItem("markerTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("markerTarget"));
        }
        that.playing = !that.playing; // This would alternate the state each time
        var coord = that.dataArrayCoords[that.target];
        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        that.startPos = [lat, lng];
        that.speed = 200; // km/h
        if (that.playing) {
            that.allData.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.allData.mark == undefined) {
                that.allData.map.addMarker({
                    icon: 'www/assets/imgs/vehicles/running' + that.trackerType + '.png',
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLng */](that.startPos[0], that.startPos[1]),
                }).then(function (marker) {
                    that.allData.mark = marker;
                    that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
                });
            }
            else {
                that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLng */](that.startPos[0], that.startPos[1]));
                that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
            }
        }
        else {
            that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLng */](that.startPos[0], that.startPos[1]));
        }
    };
    HistoryDevicePage.prototype.liveTrack = function (map, mark, coords, target, startPos, speed, delay) {
        var that = this;
        that.events.subscribe("SpeedValue:Updated", function (sdata) {
            speed = sdata;
        });
        var target = target;
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);
        function _gotoPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLng */](coords[target][0], coords[target][1]);
            var distance = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["i" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["i" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLng */](lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLng */](lat, lng));
                    map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLng */](lat, lng));
                    setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["i" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    map.setCameraTarget(dest);
                    target++;
                    setTimeout(_gotoPoint, delay);
                }
            }
            a++;
            if (a > coords.length) {
            }
            else {
                that.speedMarker = coords[target][3].speed;
                that.updatetimedate = coords[target][2].time;
                if (that.playing) {
                    _moveMarker();
                    target = target;
                    localStorage.setItem("markerTarget", target);
                }
                else { }
                // km_h = km_h;
            }
        }
        var a = 0;
        _gotoPoint();
    };
    HistoryDevicePage.prototype.zoomin = function () {
        var that = this;
        that.allData.map.moveCameraZoomIn();
    };
    HistoryDevicePage.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    HistoryDevicePage.prototype.inter = function (fastforwad) {
        // debugger
        var that = this;
        console.log("fastforwad=> " + fastforwad);
        if (fastforwad == 'fast') {
            that.speed = 2 * that.speed;
            console.log("speed fast=> " + that.speed);
        }
        else if (fastforwad == 'slow') {
            if (that.speed > 50) {
                that.speed = that.speed / 2;
                console.log("speed slow=> " + that.speed);
            }
            else {
                console.log("speed normal=> " + that.speed);
            }
        }
        else {
            that.speed = 200;
        }
        that.events.publish("SpeedValue:Updated", that.speed);
    };
    HistoryDevicePage.prototype.btnClicked = function (timeStart, timeEnd) {
        if (localStorage.getItem("MainHistory") != null) {
            if (this.selectedVehicle == undefined) {
                var alert_1 = this.alertCtrl.create({
                    message: "Please select the vehicle first!!",
                    buttons: ['OK']
                });
                alert_1.present();
            }
            else {
                this.maphistory(timeStart, timeEnd);
            }
        }
        else {
            this.maphistory(timeStart, timeEnd);
        }
    };
    HistoryDevicePage.prototype.maphistory = function (timeStart, timeEnd) {
        var _this = this;
        var from1 = new Date(timeStart);
        this.fromtime = from1.toISOString();
        var to1 = new Date(timeEnd);
        this.totime = to1.toISOString();
        if (this.totime >= this.fromtime) {
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: 'Select Correct Time',
                message: 'To time always greater than From Time',
                buttons: ['ok']
            });
            alert_2.present();
            return false;
        }
        this.apiCall.startLoading().present();
        this.apiCall.getDistanceSpeedCall(this.trackerId, this.fromtime, this.totime)
            .subscribe(function (data3) {
            _this.data2 = data3;
            _this.latlongObjArr = data3;
            if (_this.data2["Average Speed"] == 'NaN') {
                _this.data2.AverageSpeed = 0;
            }
            else {
                _this.data2.AverageSpeed = _this.data2["Average Speed"];
            }
            _this.data2.IdleTime = _this.data2["Idle Time"];
            _this.hideplayback = true;
            // this.customTxt = "<html> <head><style> </style> </head><body>Total Distance - " + this.total_dis + " Km<br>Average Speed - " + this.avg_speed + " Km/hr</body> </html> "
            //////////////////////////////////
            _this.locations = [];
            _this.stoppages(timeStart, timeEnd);
            ////////////////////////////////
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log("error in getdistancespeed =>  ", error);
            var body = error._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.stoppages = function (timeStart, timeEnd) {
        var _this = this;
        var that = this;
        that.apiCall.stoppage_detail(this.islogin._id, new Date(timeStart).toISOString(), new Date(timeEnd).toISOString(), this.DeviceId)
            .subscribe(function (res) {
            console.log('stoppage data', res);
            var arr = [];
            for (var i = 0; i < res.length; i++) {
                _this.arrivalTime = new Date(res[i].arrival_time).toLocaleString();
                _this.departureTime = new Date(res[i].departure_time).toLocaleString();
                var fd = new Date(_this.arrivalTime).getTime();
                var td = new Date(_this.departureTime).getTime();
                var time_difference = td - fd;
                var total_min = time_difference / 60000;
                var hours = total_min / 60;
                var rhours = Math.floor(hours);
                var minutes = (hours - rhours) * 60;
                var rminutes = Math.round(minutes);
                var Durations = rhours + 'Hours' + ':' + rminutes + 'Min';
                arr.push({
                    lat: res[i].lat,
                    lng: res[i].long,
                    arrival_time: res[i].arrival_time,
                    departure_time: res[i].departure_time,
                    device: res[i].device,
                    address: res[i].address,
                    user: res[i].user,
                    duration: Durations
                });
                that.locations.push(arr);
            }
            console.log('stoppage data locations', that.locations);
            that.callgpsFunc(that.fromtime, that.totime);
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.callgpsFunc = function (fromtime, totime) {
        var that = this;
        that.apiCall.gpsCall(this.trackerId, fromtime, totime)
            .subscribe(function (data3) {
            that.apiCall.stopLoading();
            if (data3.length > 0) {
                if (data3.length > 1) { // to draw polyline at least need two points
                    that.gps(data3.reverse());
                }
                else {
                    var alert_3 = that.alertCtrl.create({
                        message: 'No Data found for selected vehicle..',
                        buttons: [{
                                text: 'OK',
                                handler: function () {
                                    // that.datetimeStart = moment({ hours: 0 }).format();
                                    // console.log('start date', this.datetimeStart)
                                    // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                                    // console.log('stop date', this.datetimeEnd);
                                    // that.selectedVehicle = undefined;
                                    that.hideplayback = false;
                                }
                            }]
                    });
                    alert_3.present();
                }
            }
            else {
                var alert_4 = that.alertCtrl.create({
                    message: 'No Data found for selected vehicle..',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                // that.datetimeStart = moment({ hours: 0 }).format();
                                // console.log('start date', this.datetimeStart)
                                // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                                // console.log('stop date', this.datetimeEnd);
                                // that.selectedVehicle = undefined;
                                that.hideplayback = false;
                            }
                        }]
                });
                alert_4.present();
            }
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = that.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.gps = function (data3) {
        var that = this;
        that.latlongObjArr = data3;
        that.dataArrayCoords = [];
        for (var i = 0; i < data3.length; i++) {
            if (data3[i].lat && data3[i].lng) {
                var arr = [];
                var startdatetime = new Date(data3[i].insertionTime);
                arr.push(data3[i].lat);
                arr.push(data3[i].lng);
                arr.push({ "time": startdatetime.toLocaleString() });
                arr.push({ "speed": data3[i].speed });
                that.dataArrayCoords.push(arr);
            }
        }
        that.mapData = [];
        that.mapData = data3.map(function (d) {
            return { lat: d.lat, lng: d.lng };
        });
        that.mapData.reverse();
        if (that.allData.map != undefined) {
            that.allData.map.remove();
        }
        var bounds = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["h" /* LatLngBounds */](that.mapData);
        var mapOptions = {
            gestures: {
                rotate: false,
                tilt: false
            }
        };
        that.allData.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
        that.allData.map.moveCamera({
            target: bounds
        });
        this.allData.map.on(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_CLICK).subscribe(function (data) {
            console.log('Click MAP');
            that.drawerHidden1 = true;
        });
        if (that.locations[0] != undefined) { // check if there is stoppages or not
            for (var k = 0; k < that.locations[0].length; k++) {
                that.setStoppages(that.locations[0][k]);
            }
        }
        that.allData.map.addMarker({
            title: 'S',
            position: that.mapData[0],
            icon: 'red',
            styles: {
                'text-align': 'center',
                'font-style': 'italic',
                'font-weight': 'bold',
                'color': 'red'
            },
        }).then(function (marker) {
            marker.showInfoWindow();
            that.allData.map.addMarker({
                title: 'D',
                position: that.mapData[that.mapData.length - 1],
                icon: 'green',
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'green'
                },
            }).then(function (marker) {
                marker.showInfoWindow();
            });
        });
        that.allData.map.addPolyline({
            points: that.mapData,
            color: '#635400',
            width: 3,
            geodesic: true
        });
    };
    HistoryDevicePage.prototype.setStoppages = function (pdata) {
        var that = this;
        ///////////////////////////////
        var htmlInfoWindow = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* HtmlInfoWindow */]();
        var frame = document.createElement('div');
        frame.innerHTML = [
            '<p style="font-size: 7px;">Address:- ' + pdata.address + '</p>',
            '<p style="font-size: 7px;">Arrival Time:- ' + __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(pdata.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>',
            '<p style="font-size: 7px;">Departure Time:- ' + __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(pdata.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>'
        ].join("");
        // frame.getElementsByTagName("img")[0].addEventListener("click", () => {
        //   htmlInfoWindow.setBackgroundColor('red');
        // });
        htmlInfoWindow.setContent(frame, { width: "220px", height: "100px" });
        ///////////////////////////////////////////////////
        if (pdata != undefined)
            (function (data) {
                console.log("inside for data=> ", data);
                var centerMarker = data;
                var location = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLng */](centerMarker.lat, centerMarker.lng);
                var markerOptions = {
                    position: location,
                    icon: 'www/assets/imgs/park.png'
                };
                that.allData.map.addMarker(markerOptions)
                    .then(function (marker) {
                    console.log('centerMarker.ID' + centerMarker.ID);
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        console.log(e);
                        console.log(e.lat);
                        that.showActionSheet = true;
                        that.drawerHidden1 = false;
                        console.log("latt1", e[0].lat);
                        console.log("long1", e[0].lng);
                        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                            "position": {
                                lat: e[0].lat,
                                lng: e[0].lng
                            }
                        }).then(function (results) {
                            if (results.length == 0) {
                                //not found
                                return null;
                            }
                            // console.log('address=>',results[0]);
                            // let address: any = [   
                            // results[0].subThoroughfare || "",
                            // results[0].thoroughfare || "",
                            // results[0].locality || "",
                            // results[0].adminArea || "",
                            // results[0].postalCode || "",
                            // results[0].country || ""
                            // ].join(", ");
                            that.addressof = results[0].extra.lines[0];
                            console.log("pickup location ", that.addressof);
                            // that.sendAddress(that.addressofstudent,studentid,that.markerlatlong.lat,that.markerlatlong.lng);
                        });
                        // htmlInfoWindow.open(marker);
                        setTimeout(function () {
                            console.log('time out call');
                            that.address = that.addressof;
                            console.log("pickup location new ", that.address);
                            that.durations = data.duration;
                            that.arrTime = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(data.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                            that.depTime = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(data.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                        }, 500);
                    });
                });
            })(pdata);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], HistoryDevicePage.prototype, "navBar", void 0);
    HistoryDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-history-device',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/history-device/history-device.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title *ngIf="device">{{device.Device_Name}}</ion-title>\n    <ion-title *ngIf="!device">View History</ion-title>\n    <ion-buttons end>\n      <div *ngIf="hideplayback">\n        <ion-icon color="light" name="rewind" style="font-size:19px;margin-top:11px;margin-right: 17px" (click)="inter(\'slow\')"></ion-icon>\n        <ion-icon color="light" name="arrow-dropright-circle" style="font-size:24px;margin-top:10px;margin-right: 15px" class="play"\n          *ngIf="!playing" (click)="Playback()"></ion-icon>\n        <ion-icon color="light" name="pause" style="font-size:24px;margin-top:10px;margin-right: 15px" class="pause" *ngIf="playing"\n          (click)="Playback()"></ion-icon>\n        <ion-icon color="light" name="fastforward" style="font-size:19px;margin-top:11px;margin-right: 17px" (click)="inter(\'fast\')"></ion-icon>\n\n      </div>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-item *ngIf="showDropDown">\n    <ion-label>{{SelectVehicle}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n      [canSearch]="true" (onChange)="onChangedSelect(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-row>\n    <ion-col width-50 padding-left class="col1">\n      <ion-avatar item-start class="avtar">\n        <img src="assets/imgs/clock.svg" align="left">\n      </ion-avatar>\n      <ion-label style="margin-top: 1px;">\n        <span style="font-size: 11px">From Date</span>\n        <ion-datetime displayFormat="DD/MM/YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="changeformat(datetimeStart)"\n          style="font-size: 10px;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col width-50 class="col1">\n      <ion-avatar item-start class="avtar">\n        <img src="assets/imgs/clock.svg" align="left">\n      </ion-avatar>\n      <ion-label style="margin-top: 1px;">\n        <span style="font-size: 11px">To Date</span>\n        <ion-datetime displayFormat="DD/MM/YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="changeformat(datetimeEnd)"\n          style="font-size: 10px;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col ion-text text-right padding-right>\n      <ion-icon ios="ios-search" md="md-search" style="font-size:30px;" (tap)="btnClicked(datetimeStart,datetimeEnd)"></ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content>\n  <div id="map_canvas">\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 68%" *ngIf="showZoom">\n      <button ion-fab mini (click)="zoomin()" color="gpsc">\n        <ion-icon name="add" color="black"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 80%" *ngIf="showZoom">\n      <button ion-fab mini (click)="zoomout()" color="gpsc">\n        <ion-icon name="remove" color="black"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n</ion-content>\n\n<div *ngIf="showActionSheet" class="divPlan">\n  <ion-bottom-drawer [(hidden)]="drawerHidden1" [dockedHeight]="dockedHeight" [bounceThreshold]="bounceThreshold" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop">\n    <div class="drawer-content">\n\n      <ion-row style="margin-bottom:-10%;">\n        <ion-col style="text-align:center;">\n          <p style="font-size: 20px;color:black;text-align: center;" *ngIf="!durations">N/A</p>\n          <p style="font-size: 20px;color:black;text-align: center;" *ngIf="durations">{{durations}}</p>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-bottom: -6%;">\n\n        <ion-col col-50>\n          <p style="font-size: 13px;color:green;margin-left: 4%;" *ngIf="!arrTime">\n            <ion-icon name="time" width="55" height="55" style="margin-top: 9%;color: green;"></ion-icon>&mdash;N/A</p>\n          <p style="font-size: 13px;color:green;margin-left: 4%;" *ngIf="arrTime">\n            <ion-icon name="time" width="55" height="55" style="margin-top: 9%;color:green;"></ion-icon> &mdash;{{arrTime}}</p>\n        </ion-col>\n        <ion-col col-50>\n          <p style="font-size: 13px;color:#ac0031;margin-left: 4%;" *ngIf="!depTime">\n            <ion-icon name="time" width="55" height="55" style="margin-top: 9%;color:#ac0031;"></ion-icon>&mdash;N/A</p>\n          <p style="font-size: 13px;margin-left: 4%;color:#ac0031;" *ngIf="depTime">\n            <ion-icon name="time" width="55" height="55" style="margin-top: 9%;color: #ac0031"></ion-icon> &mdash;{{depTime}}</p>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-bottom: -6%;">\n        <p style="font-size: 13px;color:cornflowerblue;margin-left: 4%;" *ngIf="!address">\n          <ion-icon name="pin" width="55" height="55" style="margin-top: 9%"></ion-icon> &mdash;N/A</p>\n        <p style="font-size: 13px;color:cornflowerblue;margin-left: 4%;" *ngIf="address">\n          <ion-icon name="pin" width="55" height="55" style="margin-top: 9%"></ion-icon>&mdash;{{address}}\n        </p>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>\n<ion-footer class="footSty">\n\n  <ion-row style="background-color: #dfdfdf; padding: 0px !important;">\n    <ion-col width-50 style="padding: 0px">\n      <p style="color:black;font-size:14px; text-align:center;">\n        <ion-icon name="time" style="color:#33cd5f;font-size:15px;"></ion-icon>&nbsp;\n        <span *ngIf="updatetimedate">{{updatetimedate}}&nbsp;</span>\n        <span *ngIf="!updatetimedate">0:0&nbsp;</span>\n      </p>\n    </ion-col>\n\n    <ion-col width-50 style="padding: 0px">\n      <p style="color:black;font-size:14px;text-align:center;">\n        <ion-icon name="speedometer" style="color:#cd4343"></ion-icon>&nbsp;\n        <span *ngIf="speedMarker">{{speedMarker}}&nbsp;Km/hr</span>\n        <span *ngIf="!speedMarker">0&nbsp;Km/hr</span>\n      </p>\n    </ion-col>\n  </ion-row>\n\n  <ion-toolbar>\n    <ion-row no-padding>\n      <ion-col width-50 style="text-align: center; border-right: 1px solid white; padding: 0px !important">\n        <p style="color: white; margin:0px; padding:0px" *ngIf="data2">{{data2.Distance}} km</p>\n        <p style="color: white; margin:0px; padding:0px" *ngIf="!data2">0 Km</p>\n        <p style="color: white; margin:0px; padding:0px">\n          Total Distance\n        </p>\n      </ion-col>\n      <ion-col width-50 style="text-align: center; padding: 0px !important">\n        <p style="color:#ffffff; margin:0px; padding:0px" *ngIf="data2">{{data2.AverageSpeed}}</p>\n        <p style="color:#ffffff; margin:0px; padding:0px" *ngIf="!data2">0 (Km/h)</p>\n        <p style="color:#ffffff; margin:0px; padding:0px">Average Speed</p>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/history-device/history-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], HistoryDevicePage);
    return HistoryDevicePage;
}());

//# sourceMappingURL=history-device.js.map

/***/ }),

/***/ 545:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryDevicePageModule", function() { return HistoryDevicePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history_device__ = __webpack_require__(1045);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ion_bottom_drawer__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var HistoryDevicePageModule = /** @class */ (function () {
    function HistoryDevicePageModule() {
    }
    HistoryDevicePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__history_device__["a" /* HistoryDevicePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__history_device__["a" /* HistoryDevicePage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ion_bottom_drawer__["a" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], HistoryDevicePageModule);
    return HistoryDevicePageModule;
}());

//# sourceMappingURL=history-device.module.js.map

/***/ })

});
//# sourceMappingURL=26.js.map