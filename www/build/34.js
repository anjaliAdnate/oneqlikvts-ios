webpackJsonp([34],{

/***/ 1005:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceSummaryRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DeviceSummaryRepoPage = /** @class */ (function () {
    function DeviceSummaryRepoPage(navCtrl, navParams, apicallsummary) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallsummary = apicallsummary;
        this.summaryReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    DeviceSummaryRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    DeviceSummaryRepoPage.prototype.change = function (datetimeStart) {
        console.log("datetimeStart=> " + datetimeStart);
    };
    DeviceSummaryRepoPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    DeviceSummaryRepoPage.prototype.getSummaarydevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.device_id = selectedVehicle._id;
        // this.getIgnitiondeviceReport(from, to);
    };
    DeviceSummaryRepoPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicallsummary.startLoading().present();
        // this.apicallsummary.livedatacall(this.islogin._id, this.islogin.email)
        this.apicallsummary.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicallsummary.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.devices = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicallsummary.stopLoading();
            console.log(err);
        });
    };
    DeviceSummaryRepoPage.prototype.getSummaryReport = function (starttime, endtime) {
        var _this = this;
        this.summaryReportData = [];
        var outerthis = this;
        if (this.device_id == undefined) {
            this.device_id = "";
        }
        console.log(starttime);
        console.log(endtime);
        this.apicallsummary.startLoading().present();
        this.apicallsummary.getSummaryReportApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.islogin._id, this.device_id)
            .subscribe(function (data) {
            _this.apicallsummary.stopLoading();
            _this.summaryReport = data;
            console.log(_this.summaryReport);
            var i = 0, howManyTimes = _this.summaryReport.length;
            function f() {
                outerthis.summaryReportData.push({ 'avgSpeed': outerthis.summaryReport[i].avgSpeed, 'Device_Name': outerthis.summaryReport[i].device.Device_Name, 'routeViolations': outerthis.summaryReport[i].routeViolations, 'overspeeds': outerthis.summaryReport[i].overspeeds, 'ignOn': outerthis.summaryReport[i].ignOn, 'ignOff': outerthis.summaryReport[i].ignOff, 'distance': outerthis.summaryReport[i].distance, 'tripCount': outerthis.summaryReport[i].tripCount });
                // outerthis.summaryReportData.push({ 'distance': outerthis.summaryReport[i].distance, 'Device_Name': outerthis.summaryReport[i].device.Device_Name });
                if (outerthis.summaryReport[i].endPoint != null && outerthis.summaryReport[i].startPoint != null) {
                    var latEnd = outerthis.summaryReport[i].endPoint[0];
                    var lngEnd = outerthis.summaryReport[i].endPoint[1];
                    var latlng = new google.maps.LatLng(latEnd, lngEnd);
                    var latStart = outerthis.summaryReport[i].startPoint[0];
                    var lngStart = outerthis.summaryReport[i].startPoint[1];
                    var lngStart1 = new google.maps.LatLng(latStart, lngStart);
                    var geocoder = new google.maps.Geocoder();
                    var request = {
                        latLng: latlng
                    };
                    var request1 = {
                        latLng: lngStart1
                    };
                    geocoder.geocode(request, function (data, status) {
                        console.log(data);
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                console.log("End address is: " + data[1].formatted_address);
                                outerthis.locationEndAddress = data[1].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            console.log(" address available");
                        }
                        outerthis.summaryReportData[outerthis.summaryReportData.length - 1].EndLocation = outerthis.locationEndAddress;
                    });
                    geocoder.geocode(request1, function (data, status) {
                        console.log(data);
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                console.log("Start address is: " + data[1].formatted_address);
                                outerthis.locationAddress = data[1].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            console.log(" address available");
                        }
                        outerthis.summaryReportData[outerthis.summaryReportData.length - 1].StartLocation = outerthis.locationAddress;
                    });
                }
                console.log(outerthis.summaryReportData);
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 5000);
                }
            }
            f();
        }, function (error) {
            _this.apicallsummary.stopLoading();
            console.log(error);
        });
    };
    DeviceSummaryRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-device-summary-repo',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/device-summary-repo/device-summary-repo.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Summary Report</ion-title>\n\n    </ion-navbar>\n\n\n\n    <ion-item style="background-color: #fafafa;">\n\n        <ion-label>Select Vehicle</ion-label>\n\n        <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n            [canSearch]="true" (onChange)="getSummaarydevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n        </select-searchable>\n\n    </ion-item>\n\n\n\n    <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n        <ion-col width-20>\n\n            <ion-label>\n\n                <span style="font-size: 13px">From Date</span>\n\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n                    style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n            </ion-label>\n\n        </ion-col>\n\n\n\n        <ion-col width-20>\n\n            <ion-label>\n\n                <span style="font-size: 13px">To Date</span>\n\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n                    style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n            </ion-label>\n\n        </ion-col>\n\n\n\n        <ion-col width-20>\n\n            <div style="margin-top: 9px; float: right">\n\n                <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getSummaryReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n            </div>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <ion-card *ngFor="let item of summaryReportData">\n\n\n\n        <ion-item style="border-bottom: 2px solid #dedede;">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/car_red_icon.png">\n\n            </ion-avatar>\n\n            <!-- <ion-thumbnail item-start>\n\n                       \n\n                        <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                                    \n\n                        <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n                    </ion-thumbnail>\n\n    \n\n                    <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                        <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n                    </ion-thumbnail>\n\n    \n\n                    <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                        <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n    \n\n                        <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                        <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                        <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n    \n\n                    </ion-thumbnail> -->\n\n            <p style="color:black;font-size:13px;">{{item.Device_Name }}</p>\n\n\n\n            <ion-row style="padding-top: 12px;">\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.ignOn">{{item.ignOn}}</span>\n\n                        <span *ngIf="!item.ignOn">00.00</span>&nbsp;</p>\n\n                    <p style="color:#53ab53;font-size:11px;font-weight: 350;"> Running </p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.ignOff">{{item.ignOff}}</span>\n\n                        <span *ngIf="!item.ignOff">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:red;font-weight:350;">Stop</p>\n\n                </ion-col>\n\n                <ion-col center text-center>\n\n                    <p class="para">\n\n                        <span *ngIf="item.distance">{{item.distance | number : \'1.0-2\'}}</span>\n\n                        <span *ngIf="!item.distance">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;">Total KM</p>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row style="padding-top: 5px;">\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.overspeeds">{{item.overspeeds}}</span>\n\n                        <span *ngIf="!item.overspeeds">00.00</span>&nbsp;</p>\n\n                    <p class="para1">Overspeeding</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.routeViolations">{{item.routeViolations}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.routeViolations">00.00</span>&nbsp;</p>\n\n                    <p class="para1">Route Voilation</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.avgSpeed">{{item.avgSpeed | number : \'1.0-2\'}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.avgSpeed">00.00</span>&nbsp;</p>\n\n                    <p class="para1">Avg Speed</p>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row style="padding-top: 5px;">\n\n                <p>\n\n                    <ion-icon name="pin" style="color:#33c45c;font-size:13px;"></ion-icon>&nbsp;\n\n                    <span *ngIf="!item.StartLocation" style="color:gray;font-size:11px;font-weight: 400;">N/A</span>\n\n                    <span *ngIf="item.StartLocation" style="color:gray;font-size:11px;font-weight: 400;" text-nowrap>{{item.StartLocation}}</span>\n\n                </p>\n\n            </ion-row>\n\n            <ion-row>\n\n                <p>\n\n                    <ion-icon name="pin" style="color:#e14444;font-size:13px;"></ion-icon>&nbsp;\n\n                    <span *ngIf="!item.EndLocation" style="color:gray;font-size:11px;font-weight: 400;">&nbsp;&nbsp;N/A</span>\n\n                    <span *ngIf="item.EndLocation" style="color:gray;font-size:11px;font-weight: 400;" text-nowrap>{{item.EndLocation}}</span>\n\n                </p>\n\n            </ion-row>\n\n        </ion-item>\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/device-summary-repo/device-summary-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DeviceSummaryRepoPage);
    return DeviceSummaryRepoPage;
}());

//# sourceMappingURL=device-summary-repo.js.map

/***/ }),

/***/ 507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceSummaryRepoPageModule", function() { return DeviceSummaryRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_summary_repo__ = __webpack_require__(1005);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DeviceSummaryRepoPageModule = /** @class */ (function () {
    function DeviceSummaryRepoPageModule() {
    }
    DeviceSummaryRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__device_summary_repo__["a" /* DeviceSummaryRepoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__device_summary_repo__["a" /* DeviceSummaryRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], DeviceSummaryRepoPageModule);
    return DeviceSummaryRepoPageModule;
}());

//# sourceMappingURL=device-summary-repo.module.js.map

/***/ })

});
//# sourceMappingURL=34.js.map