webpackJsonp([33],{

/***/ 1006:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DistanceReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DistanceReportPage = /** @class */ (function () {
    function DistanceReportPage(navCtrl, navParams, apicallDistance) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallDistance = apicallDistance;
        this.distanceReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        // var temp = new Date();
        // // this.datetimeStart = temp.toISOString();
        // var settime= temp.getTime();
        // // this.datetime=new Date(settime).setMinutes(0);
        // this.datetime=new Date(settime).setHours(5,30,0);
        //  this.datetimeStart =new Date(this.datetime).toISOString();
        //  var a= new Date()
        // a.setHours(a.getHours() + 5);
        // a.setMinutes(a.getMinutes()+30);
        // this.datetimeEnd = new Date(a).toISOString();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    DistanceReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    DistanceReportPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    DistanceReportPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    DistanceReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicallDistance.startLoading().present();
        // this.apicallDistance.livedatacall(this.islogin._id, this.islogin.email)
        this.apicallDistance.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicallDistance.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.devices = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicallDistance.stopLoading();
            console.log(err);
        });
    };
    DistanceReportPage.prototype.getdistancedevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle._id;
        // this.getIgnitiondeviceReport(from, to);
    };
    DistanceReportPage.prototype.getDistanceReport = function (starttime, endtime) {
        var _this = this;
        var outerthis = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        console.log(starttime);
        console.log(endtime);
        this.apicallDistance.startLoading().present();
        this.apicallDistance.getDistanceReportApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.islogin._id, this.Ignitiondevice_id)
            .subscribe(function (data) {
            _this.apicallDistance.stopLoading();
            _this.distanceReport = data;
            console.log(_this.distanceReport);
            var i = 0, howManyTimes = _this.distanceReport.length;
            function f() {
                outerthis.distanceReportData.push({ 'distance': outerthis.distanceReport[i].distance, 'Device_Name': outerthis.distanceReport[i].device.Device_Name });
                if (outerthis.distanceReport[i].endPoint != null && outerthis.distanceReport[i].startPoint != null) {
                    var latEnd = outerthis.distanceReport[i].endPoint[0];
                    var lngEnd = outerthis.distanceReport[i].endPoint[1];
                    var latlng = new google.maps.LatLng(latEnd, lngEnd);
                    var latStart = outerthis.distanceReport[i].startPoint[0];
                    var lngStart = outerthis.distanceReport[i].startPoint[1];
                    var lngStart1 = new google.maps.LatLng(latStart, lngStart);
                    var geocoder = new google.maps.Geocoder();
                    var request = {
                        latLng: latlng
                    };
                    var request1 = {
                        latLng: lngStart1
                    };
                    geocoder.geocode(request, function (data, status) {
                        console.log(data);
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                console.log("End address is: " + data[1].formatted_address);
                                outerthis.locationEndAddress = data[1].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            console.log(" address available");
                        }
                        outerthis.distanceReportData[outerthis.distanceReportData.length - 1].EndLocation = outerthis.locationEndAddress;
                    });
                    geocoder.geocode(request1, function (data, status) {
                        console.log(data);
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                console.log("Start address is: " + data[1].formatted_address);
                                outerthis.locationAddress = data[1].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            console.log(" address available");
                        }
                        outerthis.distanceReportData[outerthis.distanceReportData.length - 1].StartLocation = outerthis.locationAddress;
                    });
                }
                console.log(outerthis.distanceReportData);
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 100);
                }
            }
            f();
        }, function (error) {
            _this.apicallDistance.stopLoading();
            console.log(error);
        });
    };
    DistanceReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-distance-report',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/distance-report/distance-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Distance Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true"\n\n      (onChange)="getdistancedevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          (ionChange)="change(datetimeStart)" style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          (ionChange)="change1(datetimeEnd)" style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right;">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;"\n\n          (click)="getDistanceReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-card *ngFor="let item of distanceReportData">\n\n\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/car_red_icon.png">\n\n      </ion-avatar>\n\n\n\n      <ion-row style="padding-left:4px">\n\n        <ion-col col-8>\n\n          <p style="color:black;font-size:16px;">{{item.Device_Name }}</p>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <p style="font-size: 12px;color:#e14444;" ion-text text-right>&nbsp;&nbsp; {{item.distance}}&nbsp;KM\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <br />\n\n      <ion-row>\n\n        <p>\n\n          <span>\n\n            <ion-icon ios="ios-pin" md="md-pin" style="color:#33c45c;font-size:17px;margin-left:5px;"></ion-icon>&nbsp;\n\n          </span>\n\n          <span *ngIf="item.StartLocation" style="color:gray;font-size:11px;font-weight: 400;"></span>\n\n          <span *ngIf="!item.StartLocation" style="color:gray;font-size:11px;font-weight: 400;">&nbsp;&nbsp;N/A</span>\n\n          <span style="color:gray;font-size:11px;font-weight: 400;" text-nowrap>{{item.StartLocation}}</span>\n\n        </p>\n\n\n\n      </ion-row>\n\n      <ion-row>\n\n\n\n        <p style="font-size: 14px;">\n\n          <span>\n\n            <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:17px;margin-left:5px;\n\n                          "></ion-icon>&nbsp;\n\n          </span>\n\n          <span *ngIf="item.EndLocation" style="color:gray;font-size:11px;font-weight: 400;"></span>\n\n          <span *ngIf="!item.EndLocation" style="color:gray;font-size:11px;font-weight: 400;">&nbsp;&nbsp;N/A</span>\n\n          <span style="color:gray;font-size:11px;font-weight: 400;" text-nowrap>{{item.EndLocation}}</span>\n\n        </p>\n\n\n\n      </ion-row>\n\n\n\n    </ion-item>\n\n\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/distance-report/distance-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DistanceReportPage);
    return DistanceReportPage;
}());

//# sourceMappingURL=distance-report.js.map

/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistanceReportPageModule", function() { return DistanceReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__distance_report__ = __webpack_require__(1006);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DistanceReportPageModule = /** @class */ (function () {
    function DistanceReportPageModule() {
    }
    DistanceReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__distance_report__["a" /* DistanceReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__distance_report__["a" /* DistanceReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], DistanceReportPageModule);
    return DistanceReportPageModule;
}());

//# sourceMappingURL=distance-report.module.js.map

/***/ })

});
//# sourceMappingURL=33.js.map