webpackJsonp([5],{

/***/ 497:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentSecurePageModule", function() { return PaymentSecurePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_secure__ = __webpack_require__(721);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentSecurePageModule = /** @class */ (function () {
    function PaymentSecurePageModule() {
    }
    PaymentSecurePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__payment_secure__["a" /* PaymentSecurePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__payment_secure__["a" /* PaymentSecurePage */]),
            ],
        })
    ], PaymentSecurePageModule);
    return PaymentSecurePageModule;
}());

//# sourceMappingURL=payment-secure.module.js.map

/***/ }),

/***/ 721:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentSecurePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network_interface_ngx__ = __webpack_require__(722);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { LivePage } from './../live/live';
// import { WalletPage } from './../wallet/wallet';


// import { PaymentGreetingPage } from '../payment-greeting/payment-greeting';
// import { PaytmwalletloginPage } from '../paytmwalletlogin/paytmwalletlogin';
// import { ApiServiceProvider } from '../../providers/api-service/api-service';


var PaymentSecurePage = /** @class */ (function () {
    function PaymentSecurePage(navCtrl, navParams, apiCall, toastCtrl, networkInterface) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.networkInterface = networkInterface;
        this.rideFare = this.navParams.get('rideFare');
        console.log(this.rideFare);
    }
    PaymentSecurePage.prototype.ionViewDidLoad = function () {
        this.amounttobepaid = JSON.parse(localStorage.getItem('tripFare'));
        console.log(this.amounttobepaid);
        this.getNetworkDetal();
    };
    PaymentSecurePage.prototype.proceedSecurely = function () {
        // PaytmwalletloginPage
        var _this = this;
        this.paytmregNum = localStorage.getItem('paytmregNum');
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        console.log(userId);
        var toastCtrl = this.toastCtrl.create({
            message: "Payment done, Thanks for using OneQlik VTS !!",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var toastCtrlfailure = this.toastCtrl.create({
            message: "Payment failed , Try again",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var toastCtrlServerError = this.toastCtrl.create({
            message: "Internal Server Error, Try Again",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var updatePaymentStatus = this.toastCtrl.create({
            message: "Unable to update Payment Status ",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var paymentWithdraw = {
            CUST_ID: userId,
            app_ip: "192.168.1.102",
            deviceId: this.paytmregNum,
            "app_id": "OneQlikVTS",
            "TXN_AMOUNT": this.amounttobepaid
        };
        this.apiCall.startLoading();
        this.apiCall.proceedSecurely(paymentWithdraw)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            console.log(res);
            if (res.Status == "TXN_SUCCESS") {
                _this.apiCall.updateTripStatus(userId).subscribe(function (res) {
                    console.log("updateStatusResponse=>", res);
                    if (res.payment_status == true) {
                        toastCtrl.present();
                        toastCtrl.onDidDismiss(function () {
                            localStorage.removeItem('flag');
                            _this.navCtrl.setRoot("LivePage");
                        });
                    }
                    else {
                        updatePaymentStatus.present();
                    }
                });
            }
            else if (res.STATUS == "TXN_FAILURE") {
                console.log("Transaction failed ");
                toastCtrlfailure.present();
            }
            else {
                toastCtrlServerError.present();
            }
        });
    };
    PaymentSecurePage.prototype.getNetworkDetal = function () {
        this.networkInterface.getCarrierIPAddress()
            .then(function (address) { return console.info("IP: " + address.ip + ", Subnet: " + address.subnet); })
            .catch(function (error) { return console.error("Unable to get IP: " + error); });
    };
    PaymentSecurePage.prototype.toasterror = function (errstatus) {
        console.log(errstatus);
        var toastCtrl1 = this.toastCtrl.create({
            duration: 3000,
            position: 'bottom',
            cssClass: 'toastStyle'
        });
        toastCtrl1['message'] = errstatus;
        console.log(toastCtrl1);
        return toastCtrl1.present();
    };
    PaymentSecurePage.prototype.releaseAMT = function () {
        var _this = this;
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.apiCall.startLoading();
        var releaseObj = {
            CUST_ID: userId,
            app_id: 'OneQlikVTS'
        };
        this.apiCall.releaseAmount(releaseObj)
            .subscribe(function (res) {
            if (res.STATUS == "TXN_SUCCESS") {
                _this.apiCall.stopLoading();
                _this.proceedSecurely();
            }
            else {
                console.log("Please Try again");
                _this.toasterror("try aghain");
            }
        }, function (err) {
            console.log("Internal server Error");
            _this.apiCall.stopLoading();
            _this.toasterror("try aghain");
        });
    };
    PaymentSecurePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payment-secure',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/add-devices/payment-secure/payment-secure.html"*/'<ion-content no-padding>\n\n  <ion-grid style="height:45%;background:#8eda4a;">\n\n    <ion-card style="margin: 0px;width: 28%;border-radius: 15px;padding-top: 46ppx;position: fixed;margin-top: 60px;margin-left: 37%;margin-bottom: 20px;">\n\n      <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n    </ion-card>\n\n    <h5 style="margin: 10px 0px 0px 18px;;color: white;font-weight: 400;padding-top: 52%;text-align: center;font-size: 1.8rem;">Payment</h5>\n\n  </ion-grid>\n\n  <ion-grid no-padding style="text-align: center;">\n\n    <p style="color: #8eda4a;font-size: 15px;text-align: right;margin-right: 5%;">Amount to be paid :\n\n      <i class="fa fa-rupee"></i> {{amounttobepaid}}</p>\n\n    <p style="color: #6a6c69;margin: 8% 0% 0% 20%;font-size: 20px;text-align: left;">Pay via</p>\n\n    <img src="assets/icon/Paytm_logo.png" style="width: 60%;margin: 2% 0% 0% 17%;">\n\n    <p style="color: #8f8f8f;margin: 0%;margin-top: 5%;font-size: 14px;">Balance Available in Wallet\n\n      <i class="fa fa-rupee"></i> 1000</p>\n\n  </ion-grid>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 100px;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n  <ion-row style="margin-top: 22px;width: 90%;margin-left: 5%;">\n\n    <button ion-button full style="margin: 0%;height: 50px;background: #87c23f;font-size: 18px;border-radius: 6px;text-transform: none;"\n\n      (click)="releaseAMT()">Proceed Securely</button>\n\n  </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/add-devices/payment-secure/payment-secure.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network_interface_ngx__["a" /* NetworkInterface */]])
    ], PaymentSecurePage);
    return PaymentSecurePage;
}());

//# sourceMappingURL=payment-secure.js.map

/***/ }),

/***/ 722:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkInterface; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(13);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var NetworkInterface = /** @class */ (function (_super) {
    __extends(NetworkInterface, _super);
    function NetworkInterface() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NetworkInterface.prototype.getWiFiIPAddress = function () { return Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["cordova"])(this, "getWiFiIPAddress", {}, arguments); };
    NetworkInterface.prototype.getCarrierIPAddress = function () { return Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["cordova"])(this, "getCarrierIPAddress", {}, arguments); };
    NetworkInterface.prototype.getHttpProxyInformation = function (url) { return Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["cordova"])(this, "getHttpProxyInformation", {}, arguments); };
    NetworkInterface.pluginName = "NetworkInterface";
    NetworkInterface.plugin = "cordova-plugin-networkinterface";
    NetworkInterface.pluginRef = "networkinterface";
    NetworkInterface.repo = "https://github.com/salbahra/cordova-plugin-networkinterface";
    NetworkInterface.platforms = ["Android", "BlackBerry 10", "Browser", "iOS", "Windows", "Windows Phone"];
    NetworkInterface = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], NetworkInterface);
    return NetworkInterface;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["g" /* IonicNativePlugin */]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL25ldHdvcmstaW50ZXJmYWNlL25neC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLDhCQUFzQyxNQUFNLG9CQUFvQixDQUFDOztJQW9DbEMsb0NBQWlCOzs7O0lBT3JELDJDQUFnQjtJQVNoQiw4Q0FBbUI7SUFVbkIsa0RBQXVCLGFBQUMsR0FBVzs7Ozs7O0lBMUJ4QixnQkFBZ0I7UUFENUIsVUFBVSxFQUFFO09BQ0EsZ0JBQWdCOzJCQXJDN0I7RUFxQ3NDLGlCQUFpQjtTQUExQyxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb3Jkb3ZhLCBJb25pY05hdGl2ZVBsdWdpbiwgUGx1Z2luIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9jb3JlJztcblxuLyoqXG4gKiBAbmFtZSBOZXR3b3JrIEludGVyZmFjZVxuICogQGRlc2NyaXB0aW9uXG4gKiBOZXR3b3JrIGludGVyZmFjZSBpbmZvcm1hdGlvbiBwbHVnaW4gZm9yIENvcmRvdmEvUGhvbmVHYXAgdGhhdCBzdXBwb3J0cyBBbmRyb2lkLCBCbGFja2JlcnJ5IDEwLCBCcm93c2VyLCBpT1MsIGFuZCBXaW5kb3dzIFBob25lIDguXG4gKlxuICogQHVzYWdlXG4gKiBgYGB0eXBlc2NyaXB0XG4gKiBpbXBvcnQgeyBOZXR3b3JrSW50ZXJmYWNlIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9uZXR3b3JrLWludGVyZmFjZS9uZ3gnO1xuICpcbiAqIGNvbnN0cnVjdG9yKCBwcml2YXRlIG5ldHdvcmtJbnRlcmZhY2U6IE5ldHdvcmtJbnRlcmZhY2UgKSB7XG4gKlxuICogICB0aGlzLm5ldHdvcmtJbnRlcmZhY2UuZ2V0V2lGaUlQQWRkcmVzcygpXG4gKiAgICAgLnRoZW4oYWRkcmVzcyA9PiBjb25zb2xlLmluZm8oYElQOiAke2FkZHJlc3MuaXB9LCBTdWJuZXQ6ICR7YWRkcmVzcy5zdWJuZXR9YCkpXG4gKiAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoYFVuYWJsZSB0byBnZXQgSVA6ICR7ZXJyb3J9YCkpO1xuICpcbiAqICAgdGhpcy5uZXR3b3JrSW50ZXJmYWNlLmdldENhcnJpZXJJUEFkZHJlc3MoKVxuICogICAgIC50aGVuKGFkZHJlc3MgPT4gY29uc29sZS5pbmZvKGBJUDogJHthZGRyZXNzLmlwfSwgU3VibmV0OiAke2FkZHJlc3Muc3VibmV0fWApKVxuICogICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGBVbmFibGUgdG8gZ2V0IElQOiAke2Vycm9yfWApKTtcbiAqXG4gKiAgIGNvbnN0IHVybCA9ICd3d3cuZ2l0aHViLmNvbSc7XG4gKiAgIHRoaXMubmV0d29ya0ludGVyZmFjZS5nZXRIdHRwUHJveHlJbmZvcm1hdGlvbih1cmwpXG4gKiAgICAgLnRoZW4ocHJveHkgPT4gY29uc29sZS5pbmZvKGBUeXBlOiAke3Byb3h5LnR5cGV9LCBIb3N0OiAke3Byb3h5Lmhvc3R9LCBQb3J0OiAke3Byb3h5LnBvcnR9YCkpXG4gKiAgICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoYFVuYWJsZSB0byBnZXQgcHJveHkgaW5mbzogJHtlcnJvcn1gKSk7XG4gKiB9XG4gKiBgYGBcbiAqL1xuQFBsdWdpbih7XG4gIHBsdWdpbk5hbWU6ICdOZXR3b3JrSW50ZXJmYWNlJyxcbiAgcGx1Z2luOiAnY29yZG92YS1wbHVnaW4tbmV0d29ya2ludGVyZmFjZScsXG4gIHBsdWdpblJlZjogJ25ldHdvcmtpbnRlcmZhY2UnLFxuICByZXBvOiAnaHR0cHM6Ly9naXRodWIuY29tL3NhbGJhaHJhL2NvcmRvdmEtcGx1Z2luLW5ldHdvcmtpbnRlcmZhY2UnLFxuICBwbGF0Zm9ybXM6IFsnQW5kcm9pZCcsICdCbGFja0JlcnJ5IDEwJywgJ0Jyb3dzZXInLCAnaU9TJywgJ1dpbmRvd3MnLCAnV2luZG93cyBQaG9uZSddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBOZXR3b3JrSW50ZXJmYWNlIGV4dGVuZHMgSW9uaWNOYXRpdmVQbHVnaW4ge1xuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBXaUZpIElQIGFkZHJlc3NcbiAgICogQHJldHVybiB7UHJvbWlzZTxhbnk+fSBSZXR1cm5zIGEgUHJvbWlzZSB0aGF0IHJlc29sdmVzIHdpdGggdGhlIElQIGFkZHJlc3MgaW5mb3JtYXRpb24uXG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGdldFdpRmlJUEFkZHJlc3MoKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogR2V0cyB0aGUgd2lyZWxlc3MgY2FycmllciBJUCBhZGRyZXNzXG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gUmV0dXJucyBhIFByb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIHRoZSBJUCBhZGRyZXNzIGluZm9ybWF0aW9uLlxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBnZXRDYXJyaWVySVBBZGRyZXNzKCk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIHJlbGV2YW50IHByb3hpZXMgZm9yIHRoZSBwYXNzZWQgVVJMIGluIG9yZGVyIG9mIGFwcGxpY2F0aW9uXG4gICAqIEBwYXJhbSB7dXJsfSBtZXNzYWdlICBUaGUgbWVzc2FnZSB0byBkaXNwbGF5LlxuICAgKiBAcmV0dXJuIHtQcm9taXNlPGFueT59IFJldHVybnMgYSBQcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2l0aCB0aGUgcHJveHkgaW5mb3JtYXRpb24uXG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGdldEh0dHBQcm94eUluZm9ybWF0aW9uKHVybDogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxufVxuIl19

/***/ })

});
//# sourceMappingURL=5.js.map