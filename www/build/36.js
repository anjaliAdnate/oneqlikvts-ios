webpackJsonp([36],{

/***/ 1039:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DealerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DealerPage = /** @class */ (function () {
    function DealerPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, alerCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.events = events;
        this.islogin = {};
        this.page = 1;
        this.limit = 5;
        this.DealerArraySearch = [];
        this.DealerArray = [];
        this.ndata = [];
        this.DealerData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("_id=> " + this.islogin._id);
        // console.log("islogin devices => " + JSON.stringify(this.islogin));
        // this.setsmsforotp = localStorage.getItem('setsms');
        // this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        // console.log("isSuperAdminStatus=> " + this.isSuperAdminStatus);
        // this.isDealer = this.islogin.isDealer
        // console.log("isDealer=> " + this.isDealer);
    }
    DealerPage.prototype.ionViewDidLoad = function () {
        this.getDealersListCall();
    };
    DealerPage.prototype.getDealersListCall = function () {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.DealerArray = data;
            _this.DealerArraySearch = data;
            console.log("Dealer data=> ", data);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("getting error from server=> ", err);
            var s = JSON.parse(err._body);
            var p = s.message;
            var toast = _this.toastCtrl.create({
                message: "No Dealer(s) found",
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            toast.onDidDismiss(function () {
                _this.navCtrl.setRoot("DashboardPage");
            });
        });
    };
    DealerPage.prototype.addDealersModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create('AddDealerPage');
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getDealersListCall();
        });
        modal.present();
    };
    DealerPage.prototype._editDealer = function (item) {
        var _this = this;
        var modal = this.modalCtrl.create('EditDealerPage', {
            param: item
        });
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getDealersListCall();
        });
        modal.present();
    };
    DealerPage.prototype.DeleteDealer = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: 'Do you want to delete this Customer?',
            buttons: [{
                    text: 'No'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteDeal(_id);
                    }
                }]
        });
        alert.present();
    };
    DealerPage.prototype.deleteDeal = function (_id) {
        var _this = this;
        console.log('user id bole to dealer', _id);
        var deletePayload = {
            'userId': _id,
            'deleteuser': true
        };
        this.apiCall.startLoading().present();
        this.apiCall.deleteDealerCall(deletePayload).
            subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("deleted dealer data=> " + data);
            var toast = _this.toastCtrl.create({
                message: 'Deleted dealer successfully.',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getDealersListCall();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    DealerPage.prototype.callSearch = function (ev) {
        var _this = this;
        console.log(ev.target.value);
        var searchKey = ev.target.value;
        this.apiCall.DealerSearchService(this.islogin._id, this.page, this.limit, searchKey)
            .subscribe(function (data) {
            _this.DealerArraySearch = data;
            _this.DealerArray = data;
        }, function (err) {
            console.log("error dealer=> ", JSON.stringify(err._body));
            var a = JSON.parse(err._body);
            var b = a.message;
            _this.DealerArraySearch = [];
            _this.DealerArray = [];
            var toast = _this.toastCtrl.create({
                message: "No Dealer found for search key '" + ev.target.value + "' ..",
                duration: 3000,
                position: "bottom"
            });
            toast.present();
            toast.onDidDismiss(function () {
                // ev.target.value = '';
                // this.getDealersListCall();
            });
        });
    };
    DealerPage.prototype.onClear = function (ev) {
        // debugger;
        ev.target.value = '';
        this.getDealersListCall();
    };
    DealerPage.prototype.switchDealer = function (_id) {
        var _this = this;
        //debugger;
        console.log(_id);
        // localStorage.setItem('isDealervalue', 'true');
        // localStorage.setItem('isSuperAdminValue', 'false');
        // $rootScope.dealer = $rootScope.islogin;
        localStorage.setItem('superAdminData', JSON.stringify(this.islogin));
        localStorage.setItem('custumer_status', 'OFF');
        localStorage.setItem('dealer_status', 'ON');
        this.apiCall.getcustToken(_id)
            .subscribe(function (res) {
            console.log('UserChangeObj=>', res);
            var custToken = res;
            var logindata = JSON.stringify(custToken);
            var logindetails = JSON.parse(logindata);
            var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
            // console.log('token=>', logindata);
            var details = JSON.parse(userDetails);
            // console.log(details.isDealer);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(details));
            var dealerSwitchObj = {
                "logindata": logindata,
                "details": userDetails,
                'condition_chk': details.isDealer
            };
            var temp = localStorage.getItem('isDealervalue');
            console.log("temp=> ", temp);
            _this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
            _this.events.publish("sidemenu:event", temp);
            _this.navCtrl.setRoot('DashboardPage');
        }, function (err) {
            console.log(err);
        });
    };
    DealerPage.prototype.dealerStatus = function (data) {
        var _this = this;
        // console.log("status=> " + Customersdeta.status)
        // console.log(Customersdeta._id)
        // console.log(this.islogin._id);
        var msg;
        if (data.status) {
            msg = 'Do you want to Deactivate this Dealer?';
        }
        else {
            msg = 'Do you want to Activate this Dealer?';
        }
        var alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                    text: 'YES',
                    handler: function () {
                        _this.user_status(data);
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        _this.getDealersListCall();
                    }
                }]
        });
        alert.present();
    };
    DealerPage.prototype.user_status = function (data) {
        var _this = this;
        var stat;
        if (data.status) {
            stat = false;
        }
        else {
            stat = true;
        }
        var ddata = {
            "uId": data._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(ddata)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            // this.DeletedDevice = data;
            console.log("Dealer data=> ", data);
            // console.log("DeletedDevice=> " + this.DeletedDevice)
            var toast = _this.toastCtrl.create({
                message: 'Dealer status updated successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getDealersListCall();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error => ", err);
            // var body = err._body;
            // var msg = JSON.parse(body);
            // let alert = this.alerCtrl.create({
            //   title: 'Oops!',
            //   message: msg.message,
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    DealerPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            //   var baseURLp;
            //   baseURLp = 'https://www.oneqlik.in/users/getCust?uid=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit;
            that.ndata = [];
            _this.apiCall.getDealersCall(that.islogin._id, that.page, that.limit)
                .subscribe(function (data) {
                that.ndata = data;
                for (var i_1 = 0; i_1 < that.ndata.length; i_1++) {
                    that.DealerData.push(that.ndata[i_1]);
                }
                _this.DealerArray = [];
                for (var i = 0; i < _this.DealerData.length; i++) {
                    _this.CratedeOn = JSON.stringify(_this.DealerData[i].created_on).split('"')[1].split('T')[0];
                    var gmtDateTime = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(_this.DealerData[i].created_on).split('T')[1].split('.')[0], "HH:mm:ss");
                    var gmtDate = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(_this.DealerData[i].created_on).slice(0, -1).split('T'), "YYYY-MM-DD");
                    if (_this.DealerData[i].expiration_date != null) {
                        // var expirationDate = JSON.stringify(this.DealerData[i].expiration_date).split('"')[1].split('T')[0];
                        // var gmtDateTime1 = moment.utc(JSON.stringify(this.DealerData[i].expiration_date).split('T')[1].split('.')[0], "HH:mm:ss");
                        var gmtDate2 = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(_this.DealerData[i].expiration_date).slice(0, -1).split('T'), "YYYY-MM-DD");
                        _this.expirydate = gmtDate2.format('DD/MM/YYYY');
                    }
                    else {
                        _this.expirydate = null;
                    }
                    _this.time = gmtDateTime.local().format(' h:mm:ss a');
                    _this.date = gmtDate.format('DD/MM/YYYY');
                    _this.DealerArray.push({
                        'dealer_id': _this.DealerData[i].dealer_id,
                        'dealer_firstname': _this.DealerData[i].dealer_firstname,
                        'dealer_lastname': _this.DealerData[i].dealer_lastname,
                        'email': _this.DealerData[i].email,
                        'phone': _this.DealerData[i].phone,
                        'created_on': _this.DealerData[i].created_on,
                        'status': _this.DealerData[i].status,
                        // 'pass': this.DealerData[i].pass,
                        'vehicleCount': _this.DealerData[i].vehicleCount,
                    });
                }
                _this.DealerArraySearch = _this.DealerArray;
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log("error found=> " + err);
            });
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 500);
    };
    DealerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-customers',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/dealers/dealers.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Dealers</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="addDealersModal()">\n\n                <ion-icon name="add"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n    <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar>\n\n    <!-- <ion-searchbar placeholder="Search..." (ionInput)="getItems($event)"></ion-searchbar> -->\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n\n        </ion-refresher-content>\n\n    </ion-refresher>\n\n    <ion-list>\n\n        <div *ngFor="let item of DealerArraySearch">\n\n\n\n            <ion-item>\n\n                <ion-thumbnail item-start>\n\n                    <img src="assets/imgs/user.png" alt="item.Device_Name">\n\n                    <!-- <ion-icon name="trash" style="margin-left: 41%;margin-top: 10%;font-size: 30px;color: #b9002f;" (tap)="DeleteDealer(item.dealer_id)"></ion-icon> -->\n\n                </ion-thumbnail>\n\n                <div (tap)="switchDealer(item.dealer_id)">\n\n                    <p>\n\n                        <span ion-text color="dark">Name: </span>\n\n                        {{item.dealer_firstname}}&nbsp;{{item.dealer_lastname}}\n\n                    </p>\n\n                    <p>\n\n                        <span ion-text color="dark">Email: </span>\n\n                        {{item.email}}\n\n                    </p>\n\n                    <p>\n\n                        <span ion-text color="dark">Phone: </span>\n\n                        <a href="tel:{{item.phone}}">{{item.phone}}</a>\n\n                    </p>\n\n                    <p>\n\n                        <span ion-text color="dark">Created On: </span>\n\n                        {{item.created_on | date:\'shortDate\'}}\n\n                    </p>\n\n                    <p>\n\n                        <span ion-text color="dark">Total Vehicles: </span>\n\n                        <span ion-text color="danger">{{item.vehicleCount}}</span>\n\n                    </p>\n\n                </div>\n\n\n\n                <p>\n\n                    <button ion-button small (click)="_editDealer(item)">Edit</button>\n\n                    <button ion-button small (click)="dealerStatus(item)" *ngIf="item.status == true">Active</button>\n\n                    <button ion-button small color="danger" (click)="dealerStatus(item)" *ngIf="item.status != true">InActive</button>\n\n                    <button ion-button small color="danger" (click)="DeleteDealer(item.dealer_id)">Delete</button>\n\n                </p>\n\n            </ion-item>\n\n\n\n        </div>\n\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n\n        </ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/dealers/dealers.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], DealerPage);
    return DealerPage;
}());

//# sourceMappingURL=dealers.js.map

/***/ }),

/***/ 538:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealerPageModule", function() { return DealerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dealers__ = __webpack_require__(1039);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DealerPageModule = /** @class */ (function () {
    function DealerPageModule() {
    }
    DealerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dealers__["a" /* DealerPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__dealers__["a" /* DealerPage */])
            ]
        })
    ], DealerPageModule);
    return DealerPageModule;
}());

//# sourceMappingURL=dealers.module.js.map

/***/ })

});
//# sourceMappingURL=36.js.map