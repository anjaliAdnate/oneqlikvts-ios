webpackJsonp([35],{

/***/ 1004:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditDealerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditDealerPage = /** @class */ (function () {
    function EditDealerPage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this._dealerData = navPar.get("param");
        console.log("dealer details=> " + JSON.stringify(this._dealerData));
        this.editDealerForm = formBuilder.group({
            userid: [this._dealerData.userid, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            first_name: [this._dealerData.dealer_firstname, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            last_name: [this._dealerData.dealer_lastname, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            email: [this._dealerData.email, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            phone: [this._dealerData.phone, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            address: [this._dealerData.address, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
        });
    }
    EditDealerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditDealerPage');
    };
    EditDealerPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    EditDealerPage.prototype._submit = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.editDealerForm.valid) {
            var payload = {
                address: this.editDealerForm.value.address,
                contactid: this._dealerData.dealer_id,
                expire_date: null,
                first_name: this.editDealerForm.value.first_name,
                last_name: this.editDealerForm.value.last_name,
                status: this._dealerData.status,
                user_id: this.editDealerForm.value.userid
            };
            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(payload)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("dealer updated data=> ", data);
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log("error occured=> ", err);
            });
        }
    };
    EditDealerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-cust',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/dealers/edit-dealer/edit-dealer.html"*/'<ion-header>\n  <ion-navbar>\n      <ion-title>Edit Dealer Details</ion-title>\n      <ion-buttons end>\n          <button ion-button ion-only (click)="dismiss()">\n              <ion-icon name="close-circle"></ion-icon>\n          </button>\n      </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <form [formGroup]="editDealerForm">\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">User ID*</ion-label>\n          <ion-input formControlName="userid" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.userid.valid && (editDealerForm.controls.userid.dirty || submitAttempt)">\n          <p>Enter User ID</p>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">First Name*</ion-label>\n          <ion-input formControlName="first_name" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.first_name.valid && (editDealerForm.controls.first_name.dirty || submitAttempt)">\n          <p>Enter First Name</p>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">Last Name*</ion-label>\n          <ion-input formControlName="last_name" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.last_name.valid && (editDealerForm.controls.last_name.dirty || submitAttempt)">\n          <p>Enter Last Name</p>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">Email ID</ion-label>\n          <ion-input formControlName="email" type="email"></ion-input>\n      </ion-item>\n      <!-- <ion-item class="logitem1" *ngIf="!editDealerForm.controls.email.valid && (editDealerForm.controls.email.dirty || submitAttempt)">\n          <p>email id is required!</p>\n      </ion-item> -->\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">Mobile Number</ion-label>\n          <ion-input formControlName="phone" type="number" maxlength="10" minlength="10"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.phone.valid && (editDealerForm.controls.phone.dirty || submitAttempt)">\n          <p>mobile number is required and should be 10 digits!</p>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">Address</ion-label>\n          <ion-input formControlName="address" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.address.valid && (editDealerForm.controls.address.dirty || submitAttempt)">\n          <p>Enter Address</p>\n      </ion-item>\n\n      <!-- <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">Creation On</ion-label>\n          <ion-input type="date" formControlName="creationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">Expire On</ion-label>\n          <ion-input type="date" formControlName="expirationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n      </ion-item>\n\n      <ion-item *ngIf="isSuperAdminStatus">\n          <ion-label>Dealers</ion-label>\n          <ion-select formControlName="dealer_firstname">\n              <ion-option *ngFor="let dealer of getAllDealersData" [value]="dealer.dealer_firstname" (ionSelect)="DealerselectData(dealer)">{{dealer.dealer_firstname}}</ion-option>\n          </ion-select>\n      </ion-item> -->\n  </form>\n  <!-- <button ion-button block (click)="updateCustomer()">UPDATE DETAILS</button> -->\n</ion-content>\n<ion-footer class="footSty">\n  <ion-toolbar>\n      <ion-row>\n          <ion-col style="text-align: center;">\n              <button ion-button clear color="light" (click)="_submit()">UPDATE DEALER DETAILS</button>\n          </ion-col>\n      </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/dealers/edit-dealer/edit-dealer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], EditDealerPage);
    return EditDealerPage;
}());

//# sourceMappingURL=edit-dealer.js.map

/***/ }),

/***/ 506:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDealerPageModule", function() { return EditDealerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_dealer__ = __webpack_require__(1004);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EditDealerPageModule = /** @class */ (function () {
    function EditDealerPageModule() {
    }
    EditDealerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__edit_dealer__["a" /* EditDealerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__edit_dealer__["a" /* EditDealerPage */]),
            ],
        })
    ], EditDealerPageModule);
    return EditDealerPageModule;
}());

//# sourceMappingURL=edit-dealer.module.js.map

/***/ })

});
//# sourceMappingURL=35.js.map