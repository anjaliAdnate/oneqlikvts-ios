webpackJsonp([39],{

/***/ 1002:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateCustModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UpdateCustModalPage = /** @class */ (function () {
    function UpdateCustModalPage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.devicedetail = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("_id=> " + this.islogin._id);
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        this.customer = navPar.get("param");
        console.log("customer details=> " + JSON.stringify(this.customer));
        // console.log("cust stat=> ", moment(this.customer.date, 'dd/mm/yyyy').toDate().toISOString());
        // console.log("one year later date=> " + this.customer.date)
        // if (this.customer.date == null) {
        //     var tempdate = new Date();
        //     tempdate.setDate(tempdate.getDate() + 365);
        //     // console.log("current year=> ", new Date(tempdate).toISOString())
        //     this.yearLater = moment(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
        //     // console.log("current year if=> ", this.yearLater)
        // } else {
        //     this.yearLater = moment(this.customer.date, 'DD/MM/YYYY').format('YYYY-MM-DD');
        //     // console.log("current year else=> ", this.yearLater)
        // }
        // debugger
        if (this.customer.expiration_date == null) {
            // console.log("creation date=> ", this.customer.date)
            // debugger;
            // var tru = moment(this.customer.created_on, 'DD/MM/YYYY').format('YYYY-MM-DD');
            // var tempdate = new Date(tru);
            // // console.log("type date=> ", tempdate)
            // tempdate.setDate(tempdate.getDate() + 365);
            // // console.log("current year=> ", new Date(tempdate).toISOString())
            // this.yearLater = moment(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
            var tru = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD');
            var tempdate = new Date(tru);
            // console.log("type date=> ", tempdate)
            tempdate.setDate(tempdate.getDate() + 365);
            // console.log("current year=> ", new Date(tempdate).toISOString())
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
            // console.log("current year if=> ", this.yearLater)
        }
        else {
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this.customer.expiration_date), 'DD/MM/YYYY').format('YYYY-MM-DD');
            // this.yearLater = this.customer.expiration_date;
            // console.log("current year else=> ", this.yearLater)
        }
        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one month later date=> " + __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"));
        console.log("edited date=> ", __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD'));
        console.log("expiration date=> ", this.yearLater);
        // =============== end
        this.updatecustForm = formBuilder.group({
            userid: [this.customer.userid, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            first_name: [this.customer.first_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            last_name: [this.customer.last_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            email: [this.customer.email, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            phone: [this.customer.phone, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            address: [this.customer.address, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            creationdate: [__WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD')],
            expirationdate: [this.yearLater],
            dealer_firstname: [this.customer.dealer_firstname],
        });
    }
    UpdateCustModalPage.prototype.ngOnInit = function () {
        this.getAllDealers();
    };
    UpdateCustModalPage.prototype.getAllDealers = function () {
        var _this = this;
        console.log("get user");
        var baseURLp = 'https://www.oneqlik.in/users/getAllDealerVehicles';
        this.apiCall.getAllDealerVehiclesCall(baseURLp)
            .subscribe(function (data) {
            _this.getAllDealersData = data;
        }, function (err) {
            console.log(err);
        });
    };
    UpdateCustModalPage.prototype.DealerselectData = function (dealerselect) {
        console.log(dealerselect);
        this.dealerdata = dealerselect;
        console.log(this.dealerdata.dealer_id);
    };
    UpdateCustModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateCustModalPage.prototype.updateCustomer = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.updatecustForm.valid) {
            console.log(this.updatecustForm.value);
            this.devicedetail = {
                "contactid": this.customer._id,
                "address": this.updatecustForm.value.address,
                "expire_date": new Date(this.updatecustForm.value.expirationdate).toISOString(),
                "first_name": this.updatecustForm.value.first_name,
                "last_name": this.updatecustForm.value.last_name,
                "status": this.customer.status,
                "user_id": this.updatecustForm.value.userid
            };
            if (this.vehType == undefined) {
                this.devicedetail;
            }
            else {
                this.devicedetail.vehicleType = this.vehType._id;
                // this.vehType._id
            }
            console.log(this.devicedetail);
            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(this.devicedetail)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                _this.editdata = data;
                // console.log("editdata=>"+ this.editdata);
                var toast = _this.toastCtrl.create({
                    message: "data updated successfully!",
                    position: 'bottom',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.viewCtrl.dismiss(_this.editdata);
                });
                toast.present();
            }, function (err) {
                _this.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var alert = _this.alerCtrl.create({
                    title: 'Oops!',
                    message: msg.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    UpdateCustModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-cust',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/customers/modals/update-cust/update-cust.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Edit Customer Details</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button ion-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="updatecustForm">\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">User ID*</ion-label>\n\n            <ion-input formControlName="userid" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.userid.valid && (updatecustForm.controls.userid.dirty || submitAttempt)">\n\n            <p>user id is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">First Name*</ion-label>\n\n            <ion-input formControlName="first_name" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.first_name.valid && (updatecustForm.controls.first_name.dirty || submitAttempt)">\n\n            <p>first name is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Last Name*</ion-label>\n\n            <ion-input formControlName="last_name" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.last_name.valid && (updatecustForm.controls.last_name.dirty || submitAttempt)">\n\n            <p>last name is required!</p>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Email ID*</ion-label>\n\n            <ion-input formControlName="email" type="email"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.email.valid && (updatecustForm.controls.email.dirty || submitAttempt)">\n\n            <p>email id is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Mobile Number</ion-label>\n\n            <ion-input formControlName="phone" type="number" maxlength="10" minlength="10"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.phone.valid && (updatecustForm.controls.phone.dirty || submitAttempt)">\n\n            <p>mobile number is required and should be 10 digits!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Address</ion-label>\n\n            <ion-input formControlName="address" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatecustForm.controls.address.valid && (updatecustForm.controls.address.dirty || submitAttempt)">\n\n            <p>address is required!</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Creation On</ion-label>\n\n            <ion-input type="date" formControlName="creationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">Expire On</ion-label>\n\n            <ion-input type="date" formControlName="expirationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item *ngIf="isSuperAdminStatus">\n\n            <ion-label>Dealers</ion-label>\n\n            <ion-select formControlName="dealer_firstname">\n\n                <ion-option *ngFor="let dealer of getAllDealersData" [value]="dealer.dealer_firstname" (ionSelect)="DealerselectData(dealer)">{{dealer.dealer_firstname}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n    </form>\n\n    <!-- <button ion-button block (click)="updateCustomer()">UPDATE DETAILS</button> -->\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="updateCustomer()">UPDATE CUSTOMER DETAILS</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/customers/modals/update-cust/update-cust.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["AlertController"]])
    ], UpdateCustModalPage);
    return UpdateCustModalPage;
}());

//# sourceMappingURL=update-cust.js.map

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCustModalPageModule", function() { return UpdateCustModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__update_cust__ = __webpack_require__(1002);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UpdateCustModalPageModule = /** @class */ (function () {
    function UpdateCustModalPageModule() {
    }
    UpdateCustModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__update_cust__["a" /* UpdateCustModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__update_cust__["a" /* UpdateCustModalPage */])
            ]
        })
    ], UpdateCustModalPageModule);
    return UpdateCustModalPageModule;
}());

//# sourceMappingURL=update-cust.module.js.map

/***/ })

});
//# sourceMappingURL=39.js.map