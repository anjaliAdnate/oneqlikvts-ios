webpackJsonp([32],{

/***/ 1010:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FuelReportPage = /** @class */ (function () {
    function FuelReportPage(navCtrl, navParams, apicallFuel, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallFuel = apicallFuel;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        // var temp = new Date();
        // this.datetimeStart = temp.toISOString();
        // // $rootScope.datetimeStart.setHours(0,0,0,0);
        // this.datetimeEnd = temp.toISOString();
        // var temp = new Date();
        // // this.datetimeStart = temp.toISOString();
        // var settime= temp.getTime();
        // // this.datetime=new Date(settime).setMinutes(0);
        // this.datetime=new Date(settime).setHours(5,30,0);
        //  this.datetimeStart =new Date(this.datetime).toISOString();
        //  var a= new Date()
        // a.setHours(a.getHours() + 5);
        // a.setMinutes(a.getMinutes()+30);
        // this.datetimeEnd = new Date(a).toISOString();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    FuelReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicallFuel.startLoading().present();
        // this.apicallFuel.livedatacall(this.islogin._id, this.islogin.email)
        this.apicallFuel.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicallFuel.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.devices1243.push(data);
            console.log(_this.devices1243);
            localStorage.setItem('devices1243', JSON.stringify(_this.devices1243));
            _this.devices = localStorage.getItem('devices1243');
            for (var i = 0; i < _this.devices1243[i]; i++) {
                _this.devices1243[i] = {
                    'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
                };
            }
        }, function (err) {
            _this.apicallFuel.stopLoading();
            console.log(err);
        });
    };
    FuelReportPage.prototype.getFueldevice = function (from, to, selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle._id;
        // this.getIgnitiondeviceReport(from, to);
    };
    FuelReportPage.prototype.getFuelReport = function (starttime, endtime) {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        // console.log(starttime);
        // console.log(endtime);
        this.apicallFuel.startLoading().present();
        this.apicallFuel.getFuelApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallFuel.stopLoading();
            _this.fuelReport = data;
            console.log(_this.fuelReport);
            if (_this.fuelReport.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicallFuel.stopLoading();
            console.log(error);
        });
    };
    FuelReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-report',template:/*ion-inline-start:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/fuel-report/fuel-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Fuel Report</ion-title>\n\n  </ion-navbar>\n\n\n\n\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getFueldevice(datetimeStart, datetimeEnd, selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getFuelReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-list>\n\n    <ion-card *ngFor="let fuelfilldata of   fuelReport">\n\n\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-avatar item-start>\n\n          <img src="assets/imgs/car_red_icon.png">\n\n          <!-- <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                            \n\n                <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))"> -->\n\n\n\n        </ion-avatar>\n\n        <ion-row>\n\n          <ion-col col-8>\n\n            <p style="margin-left:3px;color:gray;font-size:16px;">{{fuelfilldata.vehicleName}}</p>\n\n          </ion-col>\n\n\n\n          <ion-col col-4>\n\n\n\n            <div class="row" style="margin-top:-11px;text-align:center;">\n\n              <img src="assets/imgs/fuel.png" style="height: 19px;width: 17px;margin-top:9px;">&nbsp;\n\n              <span *ngIf="fuelfilldata.litres"></span>\n\n              <span *ngIf="!fuelfilldata.litres" style="font-size:11px;">&nbsp;N/A</span>\n\n              <span style="font-size:11px;color:#d40e0e;margin-top:12px;white-space:normal;">{{fuelfilldata.litres}}&nbsp;L </span>\n\n            </div>\n\n          </ion-col>\n\n\n\n        </ion-row>\n\n\n\n\n\n        <ion-row>\n\n\n\n          <p style="margin-top:0%;">\n\n            <span>\n\n              <ion-icon ios="ios-time" md="md-time" style="margin-left: 6px;font-size:15px;"></ion-icon>&nbsp;</span>\n\n            <span style="font-size:11px;">{{fuelfilldata.timestamp | date: \'medium\'}}</span>\n\n          </p>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n\n\n          <p style="text-align:left;font-size: 14px;">\n\n            <span>\n\n              <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;margin-left: 8px;\n\n                    "></ion-icon>&nbsp;</span>\n\n            <span *ngIf="fuelfilldata.address"></span>\n\n            <span *ngIf="!fuelfilldata.address">&nbsp;&nbsp;N/A</span>\n\n            <span style="color:gray;font-size:11px;font-weight: 400;" ion-text text-wrap>{{fuelfilldata.address}}</span>\n\n          </p>\n\n\n\n        </ion-row>\n\n\n\n      </ion-item>\n\n\n\n    </ion-card>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/ONGOING_PROJECTS/iOS_APPS/oneqlikvts-ios/src/pages/fuel-report/fuel-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], FuelReportPage);
    return FuelReportPage;
}());

//# sourceMappingURL=fuel-report.js.map

/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelReportPageModule", function() { return FuelReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fuel_report__ = __webpack_require__(1010);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FuelReportPageModule = /** @class */ (function () {
    function FuelReportPageModule() {
    }
    FuelReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fuel_report__["a" /* FuelReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fuel_report__["a" /* FuelReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], FuelReportPageModule);
    return FuelReportPageModule;
}());

//# sourceMappingURL=fuel-report.module.js.map

/***/ })

});
//# sourceMappingURL=32.js.map